﻿using local = ProCIF.DataPribadi.API.Models;
using ProCIF.DataPribadi.API.Services;
using ProCIF.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace ProCIF.DataPribadi.API.Controllers
{
    public class clsAMLScreeningList : IDisposable
    {
        IService _service;
        public clsAMLScreeningList(IService service)
        {
            _service = service;
        }

        public local.APIMessage<List<ScreeningListModel>> CheckScreeningList(ScreeningListModel screeningListModel)
        {
            local.APIMessage<List<ScreeningListModel>> msgFinalResponse = new local.APIMessage<List<ScreeningListModel>>();

            msgFinalResponse = _service.SubCheckScreeningList(screeningListModel);

            if (msgFinalResponse.IsSuccess == true && msgFinalResponse.Data.Count > 0)
            {
                msgFinalResponse.Description = "";
            }
            else if (msgFinalResponse.IsSuccess)
            {
                msgFinalResponse.Data = null;
            }

            return msgFinalResponse;
        }

        public local.APIMessage<DataTable> SubCheckScreeningNegara(string strKodeNegara)
        {
            local.APIMessage<DataSet> msgResponse = new local.APIMessage<DataSet>();
            local.APIMessage<DataTable> msgResponseFinal = new local.APIMessage<DataTable>();
            string strError = "";

            msgResponse = _service.SubCheckScreeningNegara(strKodeNegara, out strError);

            if (msgResponse.IsSuccess == true && msgResponse.Data != null)
            {
                msgResponseFinal.Data = new DataTable();
                msgResponseFinal.Data.Columns.Add("KodeNegara");
                msgResponseFinal.Data.Columns.Add("NamaNegara");
                msgResponseFinal.Data.Columns.Add("StatusHighrisk");

                foreach (DataRow item in msgResponse.Data.Tables[0].Rows)
                {
                    if (item["Warning"].Equals("Y"))
                    {
                        msgResponseFinal.Data.Rows.Add(item["KodeNegara"].ToString().Trim(), item["NamaNegara"].ToString().Trim(), "W");
                    }

                    if (item["Reject"].Equals("Y"))
                    {
                        msgResponseFinal.Data.Rows.Add(item["KodeNegara"].ToString().Trim(), item["NamaNegara"].ToString().Trim(), "R");
                    }
                }
                msgResponseFinal.IsSuccess = true;
            }

            if (msgResponse.IsSuccess == true && msgResponse.Data == null)
            {
                msgResponseFinal.IsSuccess = true;
                msgResponseFinal.Data = null;
            }

            if (msgResponseFinal.IsSuccess && msgResponseFinal.Data == null)
            {
                msgResponseFinal.Description = "Negara tidak termasuk HIGHRISK!";
                msgResponseFinal.Data = null;
            }

            if (msgResponseFinal.IsSuccess && msgResponseFinal.Data != null && msgResponseFinal.Data.Rows.Count == 0)
            {
                msgResponseFinal.Description = "Negara tidak termasuk HIGHRISK!";
                msgResponseFinal.Data = null;
            }

            if (msgResponseFinal.IsSuccess && msgResponseFinal.Data != null && msgResponseFinal.Data.Rows.Count > 0)
            {
                msgResponseFinal.Description = "Negara tidak termasuk HIGHRISK!";
                if (msgResponseFinal.Data.Rows[0]["StatusHighrisk"].ToString().ToUpper().Trim().Equals("W"))
                {
                    msgResponseFinal.Description = "Negara termasuk WARNING dan HIGHRISK!";
                }

                if (msgResponseFinal.Data.Rows[0]["StatusHighrisk"].ToString().ToUpper().Trim().Equals("R"))
                {
                    msgResponseFinal.Description = "Negara termasuk BLACKLIST!";
                }
            }

            return msgResponseFinal;
        }
        
        #region DisposeObject
        // Flag: Has Dispose already been called?
        bool disposed = false;

        // Public implementation of Dispose pattern callable by consumers.
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        // Protected implementation of Dispose pattern.
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;

            if (disposing)
            {
                // Free any other managed objects here.
                //
            }
            disposed = true;
        }
        #endregion
    }
}
