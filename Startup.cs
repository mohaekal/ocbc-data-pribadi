﻿using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using ProCIF.DataPribadi.API.Models;
using ProCIF.DataPribadi.API.Services;

namespace ProCIF.DataPribadi.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }
        
        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            string strConnStringCFMAST;
            string strConnStringCIF;
            string strConnStringLog;
            int intServerId, intServerId2, intMaxRetry;
            int intServerId3;

            intMaxRetry = int.Parse(Configuration["MaxRetryEPV"]);

            strConnStringCFMAST = Configuration["DBNAME_CFMAST"];
            intServerId = int.Parse(Configuration["DBRMDatabaseServerID"]);

            strConnStringCIF = Configuration["DBNAME_CIF"];
            intServerId2 = int.Parse(Configuration["DBNISPDatabaseServerID"]);

            #region Buid Connection LOG
            strConnStringLog = Configuration["DBNAME_Log"];
            intServerId3 = int.Parse(Configuration["DBLogDatabaseServerID"]);
            intMaxRetry = int.Parse(Configuration["MaxRetryEPV"]);
            #endregion

            string strWSPasswordURL = Configuration["wsPasswordURL"];
            strConnStringCFMAST = OCBCNISP.NetCoreLibrary.clsEPV.BuildConnString(strWSPasswordURL, intServerId.ToString(), strConnStringCFMAST, intMaxRetry);
            strConnStringCIF = OCBCNISP.NetCoreLibrary.clsEPV.BuildConnString(strWSPasswordURL, intServerId2.ToString(), strConnStringCIF, intMaxRetry);            
            strConnStringLog = OCBCNISP.NetCoreLibrary.clsEPV.BuildConnString(strWSPasswordURL, intServerId3.ToString(), strConnStringLog, intMaxRetry);


            Action<ProCIF.DataPribadi.API.Models.GlobalVariabelList> globalVariabel = (opt =>
            {
                opt.ConnectionStringCIF = strConnStringCIF;
                opt.ConnectionStringCFMAST = strConnStringCFMAST;
                opt.ConnectionStringLOG = strConnStringLog;
            });

            services.Configure(globalVariabel);
            services.AddScoped(resolver => resolver.GetRequiredService<IOptions<GlobalVariabelList>>().Value);
            services.AddScoped<IService, clsAPIService>();
            services.AddCors(options =>
            {
                string strTimespan = "1728000";
                options.AddPolicy("AllowOrigin",
                    builder => builder.WithOrigins("*")
                                    .WithMethods("GET", "POST", "OPTIONS", "PUT", "DELETE", "HEAD")
                                    .WithHeaders("Origin", "X-Requested-With", "Content-Type", "Accept", "Secret")
                                    .AllowCredentials()
                                    .SetPreflightMaxAge(TimeSpan.Parse(strTimespan)));
            });
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseCors(
                           options => options.WithOrigins("*").AllowAnyMethod().AllowAnyHeader()
                       );

            app.UseHttpsRedirection();
            app.UseMvc();
        }

    }
}
