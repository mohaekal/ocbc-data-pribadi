﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProCIF.DataPribadi.API.Models
{
    public class APIMessage<ModelResult>
    {
        private bool _IsSuccess;
        private string _Description;
        private ModelResult _Data;

        public APIMessage()
        {
            _IsSuccess = false;
            _Description = "";
        }

        public bool IsSuccess { get => _IsSuccess; set => _IsSuccess = value; }
        public string Description { get => _Description; set => _Description = value; }
        public ModelResult Data { get => _Data; set => _Data = value; }
    }
}
