﻿using APILogging.NetCoreLibrary.Models;
using APILogging.NetCoreLibrary.Services;
using Confluent.Kafka;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using local = ProCIF.DataPribadi.API.Models;
using ProCIF.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Reflection;
using System.Text;
using System.Xml.Linq;

namespace ProCIF.DataPribadi.API.Services
{
    public class clsAPIService : IService
    {
        private string _strCIFParameterConnectionString_CFMAST = "";
        private string _strCIFParameterConnectionString_SQL_CIF = "";
        private string _strParameterConnectionStringLog = "";
        private string _strKAFKA_Server = "";
        private string _strKAFKA_Request_Topic = "";
        private string _strKAFKA_Process_Topic = "";
        private int _nTimeOut = 0;
        private IConfiguration _iconfiguration;

        public clsAPIService(IConfiguration iconfiguration, local.GlobalVariabelList globalVariabelList)
        {
            _iconfiguration = iconfiguration;

            _nTimeOut = Convert.ToInt32(_iconfiguration["TimeOut"]);

            _strCIFParameterConnectionString_SQL_CIF = globalVariabelList.ConnectionStringCIF;
            _strCIFParameterConnectionString_CFMAST = globalVariabelList.ConnectionStringCFMAST;
            _strParameterConnectionStringLog = globalVariabelList.ConnectionStringLOG;

            _strKAFKA_Server = _iconfiguration["Kafka_Server"];
            _strKAFKA_Request_Topic = _iconfiguration["Kafka_Request_Topic"];
            _strKAFKA_Process_Topic = _iconfiguration["Kafka_Process_Topic"];
        }

        #region "CORE BANKING"

        public local.APIMessage<GeneralCIFModel> QuickCreation(GeneralCIFModel createCIFModel)
        {
            doLogging(API_LOGENVELOPModel.INFO, "Executing : " + GetMethodName(), "", null, null, createCIFModel.GUID, createCIFModel.NIK, createCIFModel.Module, createCIFModel.Branch);

            local.APIMessage<GeneralCIFModel> msgResponse = new local.APIMessage<GeneralCIFModel>();
            StringBuilder strRqDetail = new StringBuilder();//pembentukan RqDetail + nanti assign value dari client

            bool blnResult = false;
            string strResult = "", strErr = "";

            try
            {

                #region QuickCreationXML

                strRqDetail.AppendLine("<ns1:Customername>" + XmlValidasi(createCIFModel.CustomerName1) + "</ns1:Customername>");
                strRqDetail.AppendLine("<ns1:Customername2>" + XmlValidasi(createCIFModel.CustomerName2) + "</ns1:Customername2>");
                strRqDetail.AppendLine("<ns1:TitleBeforeName>" + XmlValidasi(createCIFModel.TitleBeforeName) + "</ns1:TitleBeforeName>");
                strRqDetail.AppendLine("<ns1:TitleAfterName>" + XmlValidasi(createCIFModel.TitleAfterName) + "</ns1:TitleAfterName>");
                strRqDetail.AppendLine("<ns1:EUR>" + XmlValidasi(createCIFModel.BirthDate.ToString("ddMMyyyy")) + "</ns1:EUR>");
                strRqDetail.AppendLine("<ns1:Banknumber>" + XmlValidasi(createCIFModel.BankNumber) + "</ns1:Banknumber>");
                strRqDetail.AppendLine("<ns1:Branchnumber>" + XmlValidasi(createCIFModel.BranchNumber) + "</ns1:Branchnumber>");
                strRqDetail.AppendLine("<ns1:Shortname>" + XmlValidasi(createCIFModel.Shortname) + "</ns1:Shortname>");
                strRqDetail.AppendLine("<ns1:Residentcode>" + XmlValidasi(createCIFModel.ResidentCode) + "</ns1:Residentcode>");
                strRqDetail.AppendLine("<ns1:State>" + XmlValidasi(createCIFModel.State) + "</ns1:State>");
                strRqDetail.AppendLine("<ns1:County>" + XmlValidasi(createCIFModel.County) + "</ns1:County>");
                strRqDetail.AppendLine("<ns1:Country>" + XmlValidasi(createCIFModel.CountryOfResidence) + "</ns1:Country>");
                strRqDetail.AppendLine("<ns1:Countryofcitizenship>" + XmlValidasi(createCIFModel.CountryOfCitizenship) + "</ns1:Countryofcitizenship>");
                strRqDetail.AppendLine("<ns1:Countryofheritage>" + XmlValidasi(createCIFModel.Religion) + "</ns1:Countryofheritage>");
                strRqDetail.AppendLine("<ns1:DMY1>" + XmlValidasi(createCIFModel.TransactionDateDMY) + "</ns1:DMY1>");
                strRqDetail.AppendLine("<ns1:IDnumber>" + XmlValidasi(createCIFModel.IDNumber) + "</ns1:IDnumber>");
                strRqDetail.AppendLine("<ns1:IDtypecode>" + XmlValidasi(createCIFModel.IDTypeCode) + "</ns1:IDtypecode>");
                if (String.IsNullOrEmpty(createCIFModel.DateIDExpiry) == true)
                {
                    strRqDetail.AppendLine("<ns1:DMY2/>");
                }
                else
                {
                    strRqDetail.AppendLine("<ns1:DMY2>" + XmlValidasi(Convert.ToDateTime(createCIFModel.DateIDExpiry).ToString("ddMMyy")) + "</ns1:DMY2>");
                }
                strRqDetail.AppendLine("<ns1:Customertypecode>" + XmlValidasi(createCIFModel.CustomerTypeCode) + "</ns1:Customertypecode>");
                strRqDetail.AppendLine("<ns1:Subclass>" + XmlValidasi(createCIFModel.Subclass) + "</ns1:Subclass>");
                strRqDetail.AppendLine("<ns1:FedWHcode>" + XmlValidasi(createCIFModel.TaxCode) + "</ns1:FedWHcode>");
                strRqDetail.AppendLine("<ns1:MotherMaidenName>" + XmlValidasi(createCIFModel.MotherMaidenName) + "</ns1:MotherMaidenName>");
                strRqDetail.AppendLine("<ns1:Addressline1>" + XmlValidasi(createCIFModel.Addressline1) + "</ns1:Addressline1>");
                strRqDetail.AppendLine("<ns1:Addressline2>" + XmlValidasi(createCIFModel.Addressline2) + "</ns1:Addressline2>");
                strRqDetail.AppendLine("<ns1:Addressline3>" + XmlValidasi(createCIFModel.Addressline3) + "</ns1:Addressline3>");
                strRqDetail.AppendLine("<ns1:Addressline4>" + XmlValidasi(createCIFModel.Addressline4) + "</ns1:Addressline4>");
                strRqDetail.AppendLine("<ns1:Postalcode>" + XmlValidasi(createCIFModel.Postalcode) + "</ns1:Postalcode>");
                strRqDetail.AppendLine("<ns1:Electionicaddressdescription>" + XmlValidasi(createCIFModel.ElectionicAddressDescription) + "</ns1:Electionicaddressdescription>");
                strRqDetail.AppendLine("<ns1:Electionicaddressdescription2>" + XmlValidasi(createCIFModel.ElectionicAddressDescription2) + "</ns1:Electionicaddressdescription2>");
                strRqDetail.AppendLine("<ns1:Customernumber>" + XmlValidasi(createCIFModel.CustomerNumber) + "</ns1:Customernumber>");
                strRqDetail.AppendLine("<ns1:Phoneticsearchkey>" + XmlValidasi(createCIFModel.PhoneticSearchKey) + "</ns1:Phoneticsearchkey>");
                strRqDetail.AppendLine("<ns1:AddressSeqNo>" + XmlValidasi(createCIFModel.AddressSeqNo) + "</ns1:AddressSeqNo>");
                strRqDetail.AppendLine("<ns1:Addresssequenceno>" + XmlValidasi(createCIFModel.AddressSequenceNo) + "</ns1:Addresssequenceno>");
                strRqDetail.AppendLine("<ns1:Addresssequenceno2>" + XmlValidasi(createCIFModel.AddressSequenceNo2) + "</ns1:Addresssequenceno2>");
                strRqDetail.AppendLine("<ns1:UserID>" + XmlValidasi(createCIFModel.UserID) + "</ns1:UserID>");
                strRqDetail.AppendLine("<ns1:Jobname>" + XmlValidasi(createCIFModel.JobName) + "</ns1:Jobname>");
                strRqDetail.AppendLine("<ns1:Foreignaddress>" + XmlValidasi(createCIFModel.ForeignAddress) + "</ns1:Foreignaddress>");
                strRqDetail.AppendLine("<ns1:Sexcode>" + XmlValidasi(createCIFModel.Sexcode) + "</ns1:Sexcode>");
                strRqDetail.AppendLine("<ns1:BLACKLISTEDFLAG>" + XmlValidasi(createCIFModel.IsBlackList) + "</ns1:BLACKLISTEDFLAG>");
                strRqDetail.AppendLine("<ns1:Identifier>" + XmlValidasi(createCIFModel.Identifier) + "</ns1:Identifier>");
                strRqDetail.AppendLine("<ns1:Retention>" + XmlValidasi(createCIFModel.Retention) + "</ns1:Retention>");
                strRqDetail.AppendLine("<ns1:Aliasnameflag>" + XmlValidasi(createCIFModel.AliasNameFlag) + "</ns1:Aliasnameflag>");
                strRqDetail.AppendLine("<ns1:Shortnamesequence>" + XmlValidasi(createCIFModel.ShortNameSequence) + "</ns1:Shortnamesequence>");
                strRqDetail.AppendLine("<ns1:Inquirycode>" + XmlValidasi(createCIFModel.InquiryCode) + "</ns1:Inquirycode>");
                strRqDetail.AppendLine("<ns1:Officercode>" + XmlValidasi(createCIFModel.OfficerCode) + "</ns1:Officercode>");
                strRqDetail.AppendLine("<ns1:Insidercode>" + XmlValidasi(createCIFModel.InsiderCode) + "</ns1:Insidercode>");
                strRqDetail.AppendLine("<ns1:VIPcustomercode>" + XmlValidasi(createCIFModel.VIPCustomerCode) + "</ns1:VIPcustomercode>");
                strRqDetail.AppendLine("<ns1:Deceasedcustomerflag>" + XmlValidasi(createCIFModel.DeceasedCustomerFlag) + "</ns1:Deceasedcustomerflag>");
                strRqDetail.AppendLine("<ns1:Holdmailcode>" + XmlValidasi(createCIFModel.HoldmailCode) + "</ns1:Holdmailcode>");
                strRqDetail.AppendLine("<ns1:Profitanalysis>" + XmlValidasi(createCIFModel.ProfitAnalysis) + "</ns1:Profitanalysis>");
                strRqDetail.AppendLine("<ns1:SIC1userdefined>" + XmlValidasi(createCIFModel.SIC1Userdefined) + "</ns1:SIC1userdefined>");
                strRqDetail.AppendLine("<ns1:SIC2userdefined>" + XmlValidasi(createCIFModel.SIC2Userdefined) + "</ns1:SIC2userdefined>");
                strRqDetail.AppendLine("<ns1:SIC3userdefined>" + XmlValidasi(createCIFModel.SIC3Userdefined) + "</ns1:SIC3userdefined>");
                strRqDetail.AppendLine("<ns1:SIC4userdefined>" + XmlValidasi(createCIFModel.SIC4Userdefined) + "</ns1:SIC4userdefined>");
                strRqDetail.AppendLine("<ns1:SIC5userdefined>" + XmlValidasi(createCIFModel.SIC5Userdefined) + "</ns1:SIC5userdefined>");
                strRqDetail.AppendLine("<ns1:SIC6userdefined>" + XmlValidasi(createCIFModel.SIC6Userdefined) + "</ns1:SIC6userdefined>");
                strRqDetail.AppendLine("<ns1:SIC7userdefined>" + XmlValidasi(createCIFModel.SIC7Userdefined) + "</ns1:SIC7userdefined>");
                strRqDetail.AppendLine("<ns1:SIC8userdefined>" + XmlValidasi(createCIFModel.SIC8Userdefined) + "</ns1:SIC8userdefined>");
                strRqDetail.AppendLine("<ns1:Customernamecontrol>" + XmlValidasi(createCIFModel.CustomerNameControl) + "</ns1:Customernamecontrol>");
                strRqDetail.AppendLine("<ns1:Dateoflastmaintenance>" + XmlValidasi(createCIFModel.TransactionDateDMY) + "</ns1:Dateoflastmaintenance>");
                strRqDetail.AppendLine("<ns1:CustomerReviewDateDDMMYY>" + XmlValidasi(createCIFModel.ReviewDateDMY) + "</ns1:CustomerReviewDateDDMMYY>");
                //BirthincorporationdateYYYYDDD = format tanggal yang diinginkan ddMMyy
                strRqDetail.AppendLine("<ns1:BirthincorporationdateYYYYDDD>" + XmlValidasi(createCIFModel.BirthIncorporationDateDMY) + "</ns1:BirthincorporationdateYYYYDDD>");
                strRqDetail.AppendLine("<ns1:Individualindicator>" + XmlValidasi(createCIFModel.IndividualIndicator) + "</ns1:Individualindicator>");
                strRqDetail.AppendLine("<ns1:SMSAcode>" + XmlValidasi(createCIFModel.SMSACode) + "</ns1:SMSAcode>");
                strRqDetail.AppendLine("<ns1:Businesstype>" + XmlValidasi(createCIFModel.BusinessType) + "</ns1:Businesstype>");
                strRqDetail.AppendLine("<ns1:Creditrating>" + XmlValidasi(createCIFModel.CreditRating) + "</ns1:Creditrating>");
                strRqDetail.AppendLine("<ns1:CIFgroupcode>" + XmlValidasi(createCIFModel.CIFGroupCode) + "</ns1:CIFgroupcode>");
                strRqDetail.AppendLine("<ns1:CIFcombinedcycle>" + XmlValidasi(createCIFModel.CIFCombinedCycle) + "</ns1:CIFcombinedcycle>");
                strRqDetail.AppendLine("<ns1:TINstatus>" + XmlValidasi(createCIFModel.TINStatus) + "</ns1:TINstatus>");
                strRqDetail.AppendLine("<ns1:StateWHcode>" + XmlValidasi(createCIFModel.TaxCode) + "</ns1:StateWHcode>");
                strRqDetail.AppendLine("<ns1:FedWHdateMMDDYY>" + XmlValidasi("000000") + " </ns1:FedWHdateMMDDYY>");
                strRqDetail.AppendLine("<ns1:UIC1userdefined>" + XmlValidasi(createCIFModel.UIC1Userdefined) + "</ns1:UIC1userdefined>");
                strRqDetail.AppendLine("<ns1:UIC2userdefined>" + XmlValidasi(createCIFModel.StatusPernikahan) + "</ns1:UIC2userdefined>");
                strRqDetail.AppendLine("<ns1:UIC3userdefined>" + XmlValidasi(createCIFModel.LevelPendidikan) + "</ns1:UIC3userdefined>");
                strRqDetail.AppendLine("<ns1:UIC4userdefined>" + XmlValidasi(createCIFModel.UIC4Userdefined) + "</ns1:UIC4userdefined>");
                strRqDetail.AppendLine("<ns1:UIC5userdefined>" + XmlValidasi(createCIFModel.UIC5Userdefined) + "</ns1:UIC5userdefined>");
                strRqDetail.AppendLine("<ns1:UIC6userdefined>" + XmlValidasi(createCIFModel.UIC6Userdefined) + "</ns1:UIC6userdefined>");
                strRqDetail.AppendLine("<ns1:UIC7userdefined>" + XmlValidasi(createCIFModel.RatarataTransaksi) + "</ns1:UIC7userdefined>");
                strRqDetail.AppendLine("<ns1:UIC8userdefined>" + XmlValidasi(createCIFModel.RiskProfile) + "</ns1:UIC8userdefined>");
                strRqDetail.AppendLine("<ns1:Customerstatus>" + XmlValidasi(createCIFModel.CustomerStatus) + "</ns1:Customerstatus>");
                strRqDetail.AppendLine("<ns1:IDIssuePlace>" + XmlValidasi(createCIFModel.IDIssuePlace) + "</ns1:IDIssuePlace>");
                strRqDetail.AppendLine("<ns1:IDStatusCode>" + XmlValidasi(createCIFModel.IDStatusCode) + "</ns1:IDStatusCode>");
                strRqDetail.AppendLine("<ns1:BirthIncorporationdate>" + XmlValidasi(createCIFModel.BirthPlace) + "</ns1:BirthIncorporationdate>");
                strRqDetail.AppendLine("<ns1:InternalIndustrycode>" + XmlValidasi(createCIFModel.InternalIndustryCode) + "</ns1:InternalIndustrycode>");
                strRqDetail.AppendLine("<ns1:GeographicalLocationcode>" + XmlValidasi(createCIFModel.GeographicalLocationCode) + "</ns1:GeographicalLocationcode>");
                strRqDetail.AppendLine("<ns1:DebitACNumber>" + XmlValidasi(createCIFModel.DebitACNumber) + "</ns1:DebitACNumber>");
                strRqDetail.AppendLine("<ns1:DebitACType>" + XmlValidasi(createCIFModel.DebitACType) + "</ns1:DebitACType>");
                strRqDetail.AppendLine("<ns1:ECRDraweeCode>" + XmlValidasi(createCIFModel.ECRDraweeCode) + "</ns1:ECRDraweeCode>");
                strRqDetail.AppendLine("<ns1:DrawerNumber>" + XmlValidasi(createCIFModel.DrawerNumber) + "</ns1:DrawerNumber>");
                strRqDetail.AppendLine("<ns1:Lastchangedbyuser>" + XmlValidasi(createCIFModel.LastChangedByuser.ToString()) + "</ns1:Lastchangedbyuser>");
                strRqDetail.AppendLine("<ns1:LastchangedDDMMYY>" + XmlValidasi(createCIFModel.TransactionDateDMY) + "</ns1:LastchangedDDMMYY>");
                strRqDetail.AppendLine("<ns1:Lastchangedtime>" + XmlValidasi(createCIFModel.TransactionDate.ToString("hhmmss")) + "</ns1:Lastchangedtime>");
                strRqDetail.AppendLine("<ns1:Noofstatementcopies>" + XmlValidasi(createCIFModel.NoofStatementCopies) + "</ns1:Noofstatementcopies>");
                strRqDetail.AppendLine("<ns1:UseinMBVALCF4>" + XmlValidasi(createCIFModel.UseinMBVALCF4) + "</ns1:UseinMBVALCF4>");
                strRqDetail.AppendLine("<ns1:InformationDateDMY>" + XmlValidasi(createCIFModel.InformationDateDMY) + "</ns1:InformationDateDMY>");
                strRqDetail.AppendLine("<ns1:Dummy>" + XmlValidasi("") + "</ns1:Dummy>");
                strRqDetail.AppendLine("<ns1:ReviewDateDMY>" + XmlValidasi(createCIFModel.ReviewDateDMY) + "</ns1:ReviewDateDMY>");
                strRqDetail.AppendLine("<ns1:DUPIDFLAG>" + XmlValidasi(createCIFModel.IsDuplicateID) + "</ns1:DUPIDFLAG>");
                strRqDetail.AppendLine("<ns1:BusinessUnitCode>" + XmlValidasi(createCIFModel.BusinessUnitCode) + "</ns1:BusinessUnitCode>");
                strRqDetail.AppendLine("<ns1:CIFGroupMemberCode>" + XmlValidasi(createCIFModel.CIFGroupMemberCode) + "</ns1:CIFGroupMemberCode>");
                strRqDetail.AppendLine("<ns1:HIGHRISKCUSTOMTERFLAG>" + XmlValidasi(createCIFModel.IsHighriskCustomer) + "</ns1:HIGHRISKCUSTOMTERFLAG>");
                if (createCIFModel.IdIssuedDateDMY == "0")
                {
                    strRqDetail.AppendLine("<ns1:IdIssuedDateDDMMYY/>");
                    strRqDetail.AppendLine("<ns1:IdIssuedDateYYYYDDD/>");
                }
                else
                {
                    strRqDetail.AppendLine("<ns1:IdIssuedDateDDMMYY>" + XmlValidasi(createCIFModel.IdIssuedDateDMY) + "</ns1:IdIssuedDateDDMMYY>");
                    strRqDetail.AppendLine("<ns1:IdIssuedDateYYYYDDD>" + XmlValidasi(DateTimeLib.GetJulian(DateTimeLib.GetDateTime(createCIFModel.IdIssuedDate, "yyyy-MM-dd")).ToString()) + "</ns1:IdIssuedDateYYYYDDD>");
                }
                strRqDetail.AppendLine("<ns1:Electionicaddressdescription3>" + XmlValidasi(createCIFModel.ElectionicAddressDescription3) + "</ns1:Electionicaddressdescription3>");
                strRqDetail.AppendLine("<ns1:Electionicaddressdescription4>" + XmlValidasi(createCIFModel.ElectionicAddressDescription4) + "</ns1:Electionicaddressdescription4>");
                strRqDetail.AppendLine("<ns1:Contactname>" + XmlValidasi(createCIFModel.Contactname) + "</ns1:Contactname>");
                strRqDetail.AppendLine("<ns1:KELURAHAN>" + XmlValidasi(createCIFModel.Kelurahan) + "</ns1:KELURAHAN>");
                strRqDetail.AppendLine("<ns1:KECAMATAN>" + XmlValidasi(createCIFModel.Kecamatan) + "</ns1:KECAMATAN>");
                strRqDetail.AppendLine("<ns1:DATIII>" + XmlValidasi(createCIFModel.DatiII) + "</ns1:DATIII>");
                strRqDetail.AppendLine("<ns1:PROVINSI>" + XmlValidasi(createCIFModel.Provinsi) + "</ns1:PROVINSI>");
                strRqDetail.AppendLine("<ns1:CODELAPTXTUNAI>" + XmlValidasi(createCIFModel.KodeLaporanTransaksiTunai) + "</ns1:CODELAPTXTUNAI>");
                strRqDetail.AppendLine("<ns1:BIrthincorporationdateYYYYDDD2>" + XmlValidasi(DateTimeLib.GetJulian(createCIFModel.BirthDate).ToString()) + "</ns1:BIrthincorporationdateYYYYDDD2>");
                strRqDetail.AppendLine("<ns1:USERENTRY>" + XmlValidasi(createCIFModel.UserID) + "</ns1:USERENTRY>");
                strRqDetail.AppendLine("<ns1:EUR2>" + XmlValidasi(createCIFModel.EUR2) + "</ns1:EUR2>");

                if (String.IsNullOrEmpty(createCIFModel.DateIDExpiry) == true || createCIFModel.DateIDExpiry == "0")
                {
                    strRqDetail.AppendLine("<ns1:IDExpirydate/>");
                }
                else
                {
                    strRqDetail.AppendLine("<ns1:IDExpirydate>" + XmlValidasi(DateTimeLib.GetJulian(Convert.ToDateTime(createCIFModel.DateIDExpiry)).ToString()) + "</ns1:IDExpirydate>");
                }

                strRqDetail.AppendLine("<ns1:EUR3>" + XmlValidasi(createCIFModel.EUR3) + "</ns1:EUR3>");
                strRqDetail.AppendLine("<ns1:KODEDATACENTER>" + XmlValidasi(createCIFModel.DataCenterCode) + "</ns1:KODEDATACENTER>");
                strRqDetail.AppendLine("<ns1:OriginalcustomerdateYYYYDDD>" + XmlValidasi(DateTimeLib.GetJulian(createCIFModel.TransactionDate).ToString()) + "</ns1:OriginalcustomerdateYYYYDDD>");

                strRqDetail.AppendLine("<ns1:FatcaFlag>" + XmlValidasi(createCIFModel.FatcaFlag) + "</ns1:FatcaFlag>");
                if (String.IsNullOrEmpty(createCIFModel.FatcaDate) || createCIFModel.FatcaDate == "0")
                    strRqDetail.AppendLine("<ns1:FatcaJulianDate>" + XmlValidasi("0") + "</ns1:FatcaJulianDate>");
                else
                    strRqDetail.AppendLine("<ns1:FatcaJulianDate>" + XmlValidasi(DateTimeLib.GetJulian(Convert.ToDateTime(createCIFModel.FatcaDate)).ToString()) + "</ns1:FatcaJulianDate>");

                #endregion

                using (clsWebServiceCIF cWS = new clsWebServiceCIF(createCIFModel.NIK, createCIFModel.GUID, createCIFModel.Module, _iconfiguration["wsProCIFURL"]))
                {
                    blnResult = cWS.CallCIFAddQuickCIFCreation01700(strRqDetail.ToString(), out strResult, out strErr);
                    doLogging(API_LOGENVELOPModel.INFO, "Data Checking strResult: " + GetMethodName(), "", strResult, null, createCIFModel.GUID, createCIFModel.NIK, createCIFModel.Module, createCIFModel.Branch);
                    if (strErr.Length > 0)
                        doLogging(API_LOGENVELOPModel.INFO, "Data Checking strErr: " + GetMethodName(), "", strErr, null, createCIFModel.GUID, createCIFModel.NIK, createCIFModel.Module, createCIFModel.Branch);

                }

                if (blnResult)
                {
                    DataSet dsOut = null;
                    try
                    {
                        if (ConvertRsSubDetailToXML(strResult, out dsOut))
                        {
                            #region QuickCreationResult                            
                            msgResponse.Data = new GeneralCIFModel();
                            msgResponse.Data.CustomerName1 = dsOut.Tables[0].Rows[0]["CustomerName"].ToString().Trim();
                            msgResponse.Data.CustomerName2 = dsOut.Tables[0].Rows[0]["Customername2"].ToString().Trim();
                            msgResponse.Data.TitleBeforeName = dsOut.Tables[0].Rows[0]["TitleBeforeName"].ToString().Trim();
                            msgResponse.Data.TitleAfterName = dsOut.Tables[0].Rows[0]["TitleAfterName"].ToString().Trim();
                            msgResponse.Data.BankNumber = dsOut.Tables[0].Rows[0]["Banknumber"].ToString().Trim();
                            msgResponse.Data.BranchNumber = dsOut.Tables[0].Rows[0]["Branchnumber"].ToString().Trim();
                            msgResponse.Data.Shortname = dsOut.Tables[0].Rows[0]["Shortname"].ToString().Trim();
                            msgResponse.Data.ResidentCode = dsOut.Tables[0].Rows[0]["Residentcode"].ToString().Trim();
                            msgResponse.Data.State = dsOut.Tables[0].Rows[0]["State"].ToString().Trim();
                            msgResponse.Data.County = dsOut.Tables[0].Rows[0]["County"].ToString().Trim();
                            msgResponse.Data.CountryOfCitizenship = dsOut.Tables[0].Rows[0]["Countryofcitizenship"].ToString().Trim();
                            msgResponse.Data.Religion = dsOut.Tables[0].Rows[0]["Countryofheritage"].ToString().Trim();
                            msgResponse.Data.IDNumber = dsOut.Tables[0].Rows[0]["IDnumber"].ToString().Trim();
                            msgResponse.Data.IDTypeCode = dsOut.Tables[0].Rows[0]["IDtypecode"].ToString().Trim();
                            msgResponse.Data.CustomerTypeCode = dsOut.Tables[0].Rows[0]["Customertypecode"].ToString().Trim();
                            msgResponse.Data.Subclass = dsOut.Tables[0].Rows[0]["Subclass"].ToString().Trim();
                            msgResponse.Data.TaxCode = dsOut.Tables[0].Rows[0]["FedWHcode"].ToString().Trim();
                            msgResponse.Data.MotherMaidenName = dsOut.Tables[0].Rows[0]["MotherMaidenName"].ToString().Trim();
                            msgResponse.Data.Addressline1 = dsOut.Tables[0].Rows[0]["Addressline1"].ToString().Trim();
                            msgResponse.Data.Addressline2 = dsOut.Tables[0].Rows[0]["Addressline2"].ToString().Trim();
                            msgResponse.Data.Addressline3 = dsOut.Tables[0].Rows[0]["Addressline3"].ToString().Trim();
                            msgResponse.Data.Addressline4 = dsOut.Tables[0].Rows[0]["Addressline4"].ToString().Trim();
                            msgResponse.Data.Postalcode = dsOut.Tables[0].Rows[0]["Postalcode"].ToString().Trim();
                            msgResponse.Data.ElectionicAddressDescription = dsOut.Tables[0].Rows[0]["Electionicaddressdescription"].ToString().Trim();
                            msgResponse.Data.ElectionicAddressDescription2 = dsOut.Tables[0].Rows[0]["Electionicaddressdescription2"].ToString().Trim();

                            msgResponse.Data.CustomerNumber = long.Parse(dsOut.Tables[0].Rows[0]["Customernumber"].ToString().Trim()).ToString();                            
                            doLogging(API_LOGENVELOPModel.INFO, "SUCCES CREATE CIF","", msgResponse.Data.CustomerNumber + "|" + msgResponse.Data.CustomerName1 + msgResponse.Data.CustomerName2, null, createCIFModel.GUID, createCIFModel.NIK, createCIFModel.Module, createCIFModel.Branch);

                            msgResponse.Data.PhoneticSearchKey = dsOut.Tables[0].Rows[0]["Phoneticsearchkey"].ToString().Trim();
                            msgResponse.Data.AddressSeqNo = dsOut.Tables[0].Rows[0]["AddressSeqNo"].ToString().Trim();
                            msgResponse.Data.AddressSequenceNo = dsOut.Tables[0].Rows[0]["Addresssequenceno"].ToString().Trim();
                            msgResponse.Data.AddressSequenceNo2 = dsOut.Tables[0].Rows[0]["Addresssequenceno2"].ToString().Trim();
                            msgResponse.Data.UserID = dsOut.Tables[0].Rows[0]["UserID"].ToString().Trim();
                            msgResponse.Data.JobName = dsOut.Tables[0].Rows[0]["Jobname"].ToString().Trim();
                            msgResponse.Data.ForeignAddress = dsOut.Tables[0].Rows[0]["Foreignaddress"].ToString().Trim();
                            msgResponse.Data.Sexcode = dsOut.Tables[0].Rows[0]["Sexcode"].ToString().Trim();
                            msgResponse.Data.IsBlackList = dsOut.Tables[0].Rows[0]["BLACKLISTEDFLAG"].ToString().Trim();
                            msgResponse.Data.Identifier = dsOut.Tables[0].Rows[0]["Identifier"].ToString().Trim();
                            msgResponse.Data.Retention = dsOut.Tables[0].Rows[0]["Retention"].ToString().Trim();
                            msgResponse.Data.AliasNameFlag = dsOut.Tables[0].Rows[0]["Aliasnameflag"].ToString().Trim();
                            msgResponse.Data.ShortNameSequence = dsOut.Tables[0].Rows[0]["Shortnamesequence"].ToString().Trim();
                            msgResponse.Data.InquiryCode = dsOut.Tables[0].Rows[0]["Inquirycode"].ToString().Trim();
                            msgResponse.Data.OfficerCode = dsOut.Tables[0].Rows[0]["Officercode"].ToString().Trim();
                            msgResponse.Data.InsiderCode = dsOut.Tables[0].Rows[0]["Insidercode"].ToString().Trim();
                            msgResponse.Data.VIPCustomerCode = dsOut.Tables[0].Rows[0]["VIPcustomercode"].ToString().Trim();
                            msgResponse.Data.HoldmailCode = dsOut.Tables[0].Rows[0]["Holdmailcode"].ToString().Trim();
                            msgResponse.Data.ProfitAnalysis = dsOut.Tables[0].Rows[0]["Profitanalysis"].ToString().Trim();
                            msgResponse.Data.SIC1Userdefined = dsOut.Tables[0].Rows[0]["SIC1userdefined"].ToString().Trim();
                            msgResponse.Data.SIC2Userdefined = dsOut.Tables[0].Rows[0]["SIC2userdefined"].ToString().Trim();
                            msgResponse.Data.SIC3Userdefined = dsOut.Tables[0].Rows[0]["SIC3userdefined"].ToString().Trim();
                            msgResponse.Data.SIC4Userdefined = dsOut.Tables[0].Rows[0]["SIC4userdefined"].ToString().Trim();
                            msgResponse.Data.SIC5Userdefined = dsOut.Tables[0].Rows[0]["SIC5userdefined"].ToString().Trim();
                            msgResponse.Data.SIC6Userdefined = dsOut.Tables[0].Rows[0]["SIC6userdefined"].ToString().Trim();
                            msgResponse.Data.SIC7Userdefined = dsOut.Tables[0].Rows[0]["SIC7userdefined"].ToString().Trim();
                            msgResponse.Data.SIC8Userdefined = dsOut.Tables[0].Rows[0]["SIC8userdefined"].ToString().Trim();
                            msgResponse.Data.CustomerNameControl = dsOut.Tables[0].Rows[0]["Customernamecontrol"].ToString().Trim();
                            msgResponse.Data.DateOfLastMaintenance = dsOut.Tables[0].Rows[0]["Dateoflastmaintenance"].ToString().Trim();
                            msgResponse.Data.IndividualIndicator = dsOut.Tables[0].Rows[0]["Individualindicator"].ToString().Trim();
                            msgResponse.Data.SMSACode = dsOut.Tables[0].Rows[0]["SMSAcode"].ToString().Trim();
                            msgResponse.Data.BusinessType = dsOut.Tables[0].Rows[0]["Businesstype"].ToString().Trim();
                            msgResponse.Data.CreditRating = dsOut.Tables[0].Rows[0]["Creditrating"].ToString().Trim();
                            msgResponse.Data.CIFGroupCode = dsOut.Tables[0].Rows[0]["CIFgroupcode"].ToString().Trim();
                            msgResponse.Data.CIFCombinedCycle = dsOut.Tables[0].Rows[0]["CIFcombinedcycle"].ToString().Trim();
                            msgResponse.Data.TINStatus = dsOut.Tables[0].Rows[0]["TINstatus"].ToString().Trim();
                            msgResponse.Data.StateWHcode = dsOut.Tables[0].Rows[0]["StateWHcode"].ToString().Trim();
                            msgResponse.Data.UIC1Userdefined = dsOut.Tables[0].Rows[0]["UIC1userdefined"].ToString().Trim();
                            msgResponse.Data.StatusPernikahan = dsOut.Tables[0].Rows[0]["UIC2userdefined"].ToString().Trim();
                            msgResponse.Data.LevelPendidikan = dsOut.Tables[0].Rows[0]["UIC3userdefined"].ToString().Trim();
                            msgResponse.Data.UIC4Userdefined = dsOut.Tables[0].Rows[0]["UIC4userdefined"].ToString().Trim();
                            msgResponse.Data.UIC5Userdefined = dsOut.Tables[0].Rows[0]["UIC5userdefined"].ToString().Trim();
                            msgResponse.Data.UIC6Userdefined = dsOut.Tables[0].Rows[0]["UIC6userdefined"].ToString().Trim();
                            msgResponse.Data.RatarataTransaksi = dsOut.Tables[0].Rows[0]["UIC7userdefined"].ToString().Trim();
                            msgResponse.Data.RiskProfile = dsOut.Tables[0].Rows[0]["UIC8userdefined"].ToString().Trim();
                            msgResponse.Data.CustomerStatus = dsOut.Tables[0].Rows[0]["Customerstatus"].ToString().Trim();
                            msgResponse.Data.IDIssuePlace = dsOut.Tables[0].Rows[0]["IDIssuePlace"].ToString().Trim();
                            msgResponse.Data.IDStatusCode = dsOut.Tables[0].Rows[0]["IDStatusCode"].ToString().Trim();
                            msgResponse.Data.InternalIndustryCode = dsOut.Tables[0].Rows[0]["InternalIndustrycode"].ToString().Trim();
                            msgResponse.Data.GeographicalLocationCode = dsOut.Tables[0].Rows[0]["GeographicalLocationcode"].ToString().Trim();
                            msgResponse.Data.DebitACNumber = dsOut.Tables[0].Rows[0]["DebitACNumber"].ToString().Trim();
                            msgResponse.Data.DebitACType = dsOut.Tables[0].Rows[0]["DebitACType"].ToString().Trim();
                            msgResponse.Data.ECRDraweeCode = dsOut.Tables[0].Rows[0]["ECRDraweeCode"].ToString().Trim();
                            msgResponse.Data.DrawerNumber = dsOut.Tables[0].Rows[0]["DrawerNumber"].ToString().Trim();
                            msgResponse.Data.LastChangedByuser = dsOut.Tables[0].Rows[0]["Lastchangedbyuser"].ToString().Trim();
                            msgResponse.Data.NoofStatementCopies = dsOut.Tables[0].Rows[0]["Noofstatementcopies"].ToString().Trim();
                            msgResponse.Data.UseinMBVALCF4 = dsOut.Tables[0].Rows[0]["UseinMBVALCF4"].ToString().Trim();
                            msgResponse.Data.IsDuplicateID = dsOut.Tables[0].Rows[0]["DUPIDFLAG"].ToString().Trim();
                            msgResponse.Data.BusinessUnitCode = dsOut.Tables[0].Rows[0]["BusinessUnitCode"].ToString().Trim();
                            msgResponse.Data.CIFGroupMemberCode = dsOut.Tables[0].Rows[0]["CIFGroupMemberCode"].ToString().Trim();
                            msgResponse.Data.ElectionicAddressDescription3 = dsOut.Tables[0].Rows[0]["Electionicaddressdescription3"].ToString().Trim();
                            msgResponse.Data.ElectionicAddressDescription4 = dsOut.Tables[0].Rows[0]["Electionicaddressdescription4"].ToString().Trim();
                            msgResponse.Data.Contactname = dsOut.Tables[0].Rows[0]["Contactname"].ToString().Trim();
                            msgResponse.Data.Kelurahan = dsOut.Tables[0].Rows[0]["KELURAHAN"].ToString().Trim();
                            msgResponse.Data.Kecamatan = dsOut.Tables[0].Rows[0]["KECAMATAN"].ToString().Trim();
                            msgResponse.Data.DatiII = dsOut.Tables[0].Rows[0]["DATIII"].ToString().Trim();
                            msgResponse.Data.Provinsi = dsOut.Tables[0].Rows[0]["PROVINSI"].ToString().Trim();
                            msgResponse.Data.KodeLaporanTransaksiTunai = dsOut.Tables[0].Rows[0]["CODELAPTXTUNAI"].ToString().Trim();
                            msgResponse.Data.DataCenterCode = dsOut.Tables[0].Rows[0]["KODEDATACENTER"].ToString().Trim();
                            msgResponse.Data.FatcaFlag = dsOut.Tables[0].Rows[0]["FatcaFlag"].ToString().Trim();

                            msgResponse.Data.CountryOfResidence = dsOut.Tables[0].Rows[0]["Country"].ToString().Trim();

                            if (Convert.ToInt32(dsOut.Tables[0].Rows[0]["FatcaJulianDate"].ToString().Trim()) == 0)
                            {
                                msgResponse.Data.FatcaDate = "0";
                            }
                            else
                            {
                                msgResponse.Data.FatcaDate = createCIFModel.FatcaDate;
                            }

                            msgResponse.Data.BirthDate = createCIFModel.BirthDate;

                            msgResponse.Data.IdIssuedDateDMY = dsOut.Tables[0].Rows[0]["IdIssuedDateDDMMYY"].ToString().Trim();
                            msgResponse.Data.IdIssuedDate = createCIFModel.IdIssuedDate;

                            msgResponse.Data.OriginalCustomerDate = createCIFModel.OriginalCustomerDate;
                            msgResponse.Data.InformationDate = createCIFModel.InformationDate;
                            msgResponse.Data.LastChangeDate = createCIFModel.LastChangeDate;
                            msgResponse.Data.FedWHDate = createCIFModel.FedWHDate;
                            msgResponse.Data.TransactionDate = createCIFModel.TransactionDate;

                            msgResponse.Data.NIK = createCIFModel.NIK;
                            msgResponse.Data.Branch = createCIFModel.Branch;
                            msgResponse.Data.Module = createCIFModel.Module;
                            msgResponse.Data.GUID = createCIFModel.GUID;
                            msgResponse.Data.MoreIndicator = createCIFModel.MoreIndicator;


                            #endregion

                            msgResponse.IsSuccess = true;
                            msgResponse.Description = "";

                            this.SubSaveCIFMaster_TM(msgResponse.Data);
                            this.SubSaveProCIFMaster_TM(msgResponse.Data);
                        }
                        else
                        {
                            msgResponse.Data = null;
                            msgResponse.IsSuccess = false;
                            msgResponse.Description = "ConvertRsSubDetailToXML Failed!";
                        }
                    }
                    catch (Exception ex)
                    {
                        msgResponse.Data = null;
                        msgResponse.IsSuccess = false;
                        //msgResponse.Description = ex.Message + Environment.NewLine + ex.StackTrace.ToString();
                        msgResponse.Description = API_LOGENVELOPModel.CATCH_DESC_EXCEPTION;
                        doLogging(API_LOGENVELOPModel.ERROR, "Catch Exception : " + GetMethodName(), "", msgResponse, ex, createCIFModel.GUID, createCIFModel.NIK, createCIFModel.Module, createCIFModel.Branch);
                    }
                }
                else
                {
                    msgResponse.Data = null;
                    msgResponse.IsSuccess = false;
                    msgResponse.Description = strErr;
                }
            }
            catch (Exception ex)
            {
                msgResponse.Data = null;
                msgResponse.IsSuccess = false;
                //msgResponse.Description = ex.Message + Environment.NewLine + ex.StackTrace.ToString();
                msgResponse.Description = API_LOGENVELOPModel.CATCH_DESC_EXCEPTION;
                doLogging(API_LOGENVELOPModel.ERROR, "Catch Exception : " + GetMethodName(), "", msgResponse, ex, createCIFModel.GUID, createCIFModel.NIK, createCIFModel.Module, createCIFModel.Branch);
            }
            return msgResponse;
        }

        public local.APIMessage<GeneralCIFModel> BasicMaintenance(GeneralCIFModel paramModel, bool isAutoApprove = false)
        {
            doLogging(API_LOGENVELOPModel.INFO, "Executing : " + GetMethodName(), "", null, null, paramModel.GUID, paramModel.NIK, paramModel.Module, paramModel.Branch);

            local.APIMessage<GeneralCIFModel> msgResponse = new local.APIMessage<GeneralCIFModel>();
            StringBuilder strRqDetail = new StringBuilder();//pembentukan RqDetail + nanti assign value dari client

            bool blnResult = false;
            string strResult = "", strErr = "";
            
            GeneralCIFModel modelOld = new GeneralCIFModel();
            modelOld = this.InquiryDetailV2(paramModel).Data;

            try
            {
                #region MaintenanceCIFXML
                strRqDetail.AppendLine("<ns1:Customernumber>" + XmlValidasi(paramModel.CustomerNumber.ToString()) + "</ns1:Customernumber>");
                strRqDetail.AppendLine("<ns1:Branchnumber>" + XmlValidasi(paramModel.BranchNumber.ToString()) + "</ns1:Branchnumber>");
                strRqDetail.AppendLine("<ns1:ControlingBranch>" + XmlValidasi("") + "</ns1:ControlingBranch>");
                strRqDetail.AppendLine("<ns1:Customertypecode>" + XmlValidasi(paramModel.CustomerTypeCode) + "</ns1:Customertypecode>");
                strRqDetail.AppendLine("<ns1:Subclass>" + XmlValidasi(paramModel.Subclass) + "</ns1:Subclass>");
                strRqDetail.AppendLine("<ns1:Residentcode>" + XmlValidasi(paramModel.ResidentCode) + "</ns1:Residentcode>");
                strRqDetail.AppendLine("<ns1:IDtypecode>" + XmlValidasi(paramModel.IDTypeCode) + "</ns1:IDtypecode>");
                strRqDetail.AppendLine("<ns1:IDnumber>" + XmlValidasi(paramModel.IDNumber) + "</ns1:IDnumber>");

                if (String.IsNullOrEmpty(paramModel.DateIDExpiry) == true)
                {
                    strRqDetail.AppendLine("<ns1:IDExpirydate/>");
                }
                else
                {
                    strRqDetail.AppendLine("<ns1:IDExpirydate>" + XmlValidasi(Convert.ToDateTime(paramModel.DateIDExpiry).ToString("ddMMyy")) + "</ns1:IDExpirydate>");
                }
                strRqDetail.AppendLine("<ns1:IDIssuePlace>" + XmlValidasi(paramModel.IDIssuePlace) + "</ns1:IDIssuePlace>");
                strRqDetail.AppendLine("<ns1:IDStatusCode>" + XmlValidasi(paramModel.IDStatusCode) + "</ns1:IDStatusCode>");

                strRqDetail.AppendLine("<ns1:DOBMMDDYYYY>" + XmlValidasi(Convert.ToDateTime(paramModel.BirthDate).ToString("ddMMyyyy")) + "</ns1:DOBMMDDYYYY>");

                strRqDetail.AppendLine("<ns1:BirthIncorporationdate>" + XmlValidasi(paramModel.BirthPlace) + "</ns1:BirthIncorporationdate>");
                strRqDetail.AppendLine("<ns1:OriginalcustomerdateMMDDYY>" + XmlValidasi(paramModel.OriginalCustomerDateDMY.ToString()) + "</ns1:OriginalcustomerdateMMDDYY>");
                strRqDetail.AppendLine("<ns1:Insidercode>" + XmlValidasi(paramModel.InsiderCode) + "</ns1:Insidercode>");
                strRqDetail.AppendLine("<ns1:Inquirycode>" + XmlValidasi(paramModel.InquiryCode) + "</ns1:Inquirycode>");
                strRqDetail.AppendLine("<ns1:Businesstype>" + XmlValidasi(paramModel.BusinessType) + "</ns1:Businesstype>");
                strRqDetail.AppendLine("<ns1:InternalIndustrycode>" + XmlValidasi(paramModel.InternalIndustryCode) + "</ns1:InternalIndustrycode>");
                strRqDetail.AppendLine("<ns1:FedWHcode>" + XmlValidasi(paramModel.TaxCode.ToString()) + "</ns1:FedWHcode>");
                strRqDetail.AppendLine("<ns1:Sexcode>" + XmlValidasi(paramModel.Sexcode) + "</ns1:Sexcode>");
                strRqDetail.AppendLine("<ns1:Countryofheritage>" + XmlValidasi(paramModel.Religion) + "</ns1:Countryofheritage>");
                strRqDetail.AppendLine("<ns1:Countryofcitizenship>" + XmlValidasi(paramModel.CountryOfCitizenship) + "</ns1:Countryofcitizenship>");
                strRqDetail.AppendLine("<ns1:Country>" + XmlValidasi(paramModel.CountryOfResidence) + "</ns1:Country>");
                strRqDetail.AppendLine("<ns1:VIPcustomercode>" + XmlValidasi(paramModel.VIPCustomerCode) + "</ns1:VIPcustomercode>");
                strRqDetail.AppendLine("<ns1:Profitanalysis>" + XmlValidasi(paramModel.ProfitAnalysis) + "</ns1:Profitanalysis>");
                strRqDetail.AppendLine("<ns1:CIFcombinedcycle>" + XmlValidasi(paramModel.CIFCombinedCycle.ToString()) + "</ns1:CIFcombinedcycle>");
                strRqDetail.AppendLine("<ns1:Holdmailcode>" + XmlValidasi(paramModel.HoldmailCode) + "</ns1:Holdmailcode>");
                strRqDetail.AppendLine("<ns1:Retention>" + XmlValidasi(paramModel.Retention.ToString()) + "</ns1:Retention>");
                strRqDetail.AppendLine("<ns1:SIC1userdefined>" + XmlValidasi(paramModel.SIC1Userdefined) + "</ns1:SIC1userdefined>");
                strRqDetail.AppendLine("<ns1:SIC2userdefined>" + XmlValidasi(paramModel.SIC2Userdefined) + "</ns1:SIC2userdefined>");
                strRqDetail.AppendLine("<ns1:SIC3userdefined>" + XmlValidasi(paramModel.SIC3Userdefined) + "</ns1:SIC3userdefined>");
                strRqDetail.AppendLine("<ns1:SIC4userdefined>" + XmlValidasi(paramModel.SIC4Userdefined) + "</ns1:SIC4userdefined>");
                strRqDetail.AppendLine("<ns1:SIC5userdefined>" + XmlValidasi(paramModel.SIC5Userdefined) + "</ns1:SIC5userdefined>");
                strRqDetail.AppendLine("<ns1:SIC6userdefined>" + XmlValidasi(paramModel.SIC6Userdefined) + "</ns1:SIC6userdefined>");
                strRqDetail.AppendLine("<ns1:SIC7userdefined>" + XmlValidasi(paramModel.SIC7Userdefined) + "</ns1:SIC7userdefined>");
                strRqDetail.AppendLine("<ns1:SIC8userdefined>" + XmlValidasi(paramModel.SIC8Userdefined) + "</ns1:SIC8userdefined>");
                strRqDetail.AppendLine("<ns1:UIC1userdefined>" + XmlValidasi(paramModel.UIC1Userdefined) + "</ns1:UIC1userdefined>");
                strRqDetail.AppendLine("<ns1:UIC2userdefined>" + XmlValidasi(paramModel.StatusPernikahan) + "</ns1:UIC2userdefined>");
                strRqDetail.AppendLine("<ns1:UIC3userdefined>" + XmlValidasi(paramModel.LevelPendidikan) + "</ns1:UIC3userdefined>");
                strRqDetail.AppendLine("<ns1:UIC4userdefined>" + XmlValidasi(paramModel.UIC4Userdefined) + "</ns1:UIC4userdefined>");
                strRqDetail.AppendLine("<ns1:UIC5userdefined>" + XmlValidasi(paramModel.UIC5Userdefined) + "</ns1:UIC5userdefined>");
                strRqDetail.AppendLine("<ns1:UIC6userdefined>" + XmlValidasi(paramModel.UIC6Userdefined) + "</ns1:UIC6userdefined>");
                strRqDetail.AppendLine("<ns1:UIC7userdefined>" + XmlValidasi(paramModel.RatarataTransaksi) + "</ns1:UIC7userdefined>");
                strRqDetail.AppendLine("<ns1:UIC8userdefined>" + XmlValidasi(paramModel.RiskProfile) + "</ns1:UIC8userdefined>");
                strRqDetail.AppendLine("<ns1:Lastchangedbyuser>" + XmlValidasi(paramModel.LastChangedByuser) + "</ns1:Lastchangedbyuser>");
                strRqDetail.AppendLine("<ns1:MotherMaidenName>" + XmlValidasi(paramModel.MotherMaidenName) + "</ns1:MotherMaidenName>");
                strRqDetail.AppendLine("<ns1:BlacklistProceed>" + XmlValidasi("Y") + "</ns1:BlacklistProceed>");
                strRqDetail.AppendLine("<ns1:CheckIDTupYN>" + XmlValidasi("") + "</ns1:CheckIDTupYN>");
                strRqDetail.AppendLine("<ns1:BusinessUnitCode>" + XmlValidasi(paramModel.BusinessUnitCode) + "</ns1:BusinessUnitCode>");
                strRqDetail.AppendLine("<ns1:CIFGroupMemberCode>" + XmlValidasi(paramModel.CIFGroupMemberCode.ToString()) + "</ns1:CIFGroupMemberCode>");
                strRqDetail.AppendLine("<ns1:DuplicateID>" + XmlValidasi("Y") + "</ns1:DuplicateID>");
                strRqDetail.AppendLine("<ns1:MemberTypeShareholderYN>" + XmlValidasi("") + "</ns1:MemberTypeShareholderYN>");
                strRqDetail.AppendLine("<ns1:MemberTypeManagementYN>" + XmlValidasi("") + "</ns1:MemberTypeManagementYN>");
                strRqDetail.AppendLine("<ns1:MemberTypeFinancialYN>" + XmlValidasi("") + "</ns1:MemberTypeFinancialYN>");
                strRqDetail.AppendLine("<ns1:GroupchangeFlag>" + XmlValidasi("") + "</ns1:GroupchangeFlag>");
                strRqDetail.AppendLine("<ns1:Highriskcustomerflag>" + XmlValidasi("Y") + "</ns1:Highriskcustomerflag>");
                strRqDetail.AppendLine("<ns1:USERENTRY>" + XmlValidasi(paramModel.NIK) + "</ns1:USERENTRY>");
                strRqDetail.AppendLine("<ns1:CODELAPTXTUNAI>" + XmlValidasi(paramModel.KodeLaporanTransaksiTunai) + "</ns1:CODELAPTXTUNAI>");

                //ini julian issued date
                if (paramModel.IdIssuedDateDMY=="0")
                {
                    strRqDetail.AppendLine("<ns1:IdIssuedDateDDMMYY/>");
                    strRqDetail.AppendLine("<ns1:IDExpirydate2/>");
                    strRqDetail.AppendLine("<ns1:DDMMYYYY/>");
                }
                else
                {
                    strRqDetail.AppendLine("<ns1:IdIssuedDateDDMMYY>" + XmlValidasi(paramModel.IdIssuedDateDMY.ToString()) + "</ns1:IdIssuedDateDDMMYY>");
                    strRqDetail.AppendLine("<ns1:IDExpirydate2>" + XmlValidasi(DateTimeLib.GetJulian(DateTimeLib.GetDateTime(paramModel.IdIssuedDate, "yyyy-MM-dd")).ToString()) + "</ns1:IDExpirydate2>");
                    string tmp = "0";

                    if (paramModel.IdIssuedDateDMY != "0")
                        tmp = DateTimeLib.GetDateTime(paramModel.IdIssuedDate, "yyyy-MM-dd").ToString("ddMMyyyy");

                    strRqDetail.AppendLine("<ns1:DDMMYYYY>" + XmlValidasi(tmp) + "</ns1:DDMMYYYY>");
                }


                if (String.IsNullOrEmpty(paramModel.DateIDExpiry) == true)
                {
                    strRqDetail.AppendLine("<ns1:IDExpirydate3>" + XmlValidasi("0") + "</ns1:IDExpirydate3>");
                    strRqDetail.AppendLine("<ns1:DDMMYYYY2>" + XmlValidasi("") + "</ns1:DDMMYYYY2>");
                }
                else
                {
                    strRqDetail.AppendLine("<ns1:IDExpirydate3>" + XmlValidasi(DateTimeLib.GetJulian(Convert.ToDateTime(paramModel.DateIDExpiry)).ToString()) + "</ns1:IDExpirydate3>");
                    strRqDetail.AppendLine("<ns1:DDMMYYYY2>" + XmlValidasi(Convert.ToDateTime(paramModel.DateIDExpiry).ToString("ddMMyyyy")) + "</ns1:DDMMYYYY2>");
                }

                strRqDetail.AppendLine("<ns1:Employeecode>" + XmlValidasi(paramModel.EmployeeNIK.ToString()) + "</ns1:Employeecode>");
                strRqDetail.AppendLine("<ns1:Employeename>" + XmlValidasi(paramModel.EmployeeName) + "</ns1:Employeename>");
                strRqDetail.AppendLine("<ns1:Customernumber2>" + XmlValidasi(paramModel.CustomerNumber.ToString()) + "</ns1:Customernumber2>");
                strRqDetail.AppendLine("<ns1:EmployeeIndicator>" + XmlValidasi(paramModel.EmployeeIndicator) + "</ns1:EmployeeIndicator>");

                strRqDetail.AppendLine("<ns1:OriginalcustomerdateYYYYDDD>" + XmlValidasi(DateTimeLib.GetJulian(paramModel.OriginalCustomerDate).ToString()) + "</ns1:OriginalcustomerdateYYYYDDD>");
                strRqDetail.AppendLine("<ns1:DDMMYYYY3>" + XmlValidasi(paramModel.OriginalCustomerDate.ToString("ddMMyyyy")) + "</ns1:DDMMYYYY3>");
                strRqDetail.AppendLine("<ns1:FatcaFlag>" + XmlValidasi(paramModel.FatcaFlag) + "</ns1:FatcaFlag>");

                if (String.IsNullOrEmpty(paramModel.FatcaDate) || paramModel.FatcaDate == "0")
                    strRqDetail.AppendLine("<ns1:FatcaJulianDate>" + XmlValidasi("0") + "</ns1:FatcaJulianDate>");
                else
                    strRqDetail.AppendLine("<ns1:FatcaJulianDate>" + XmlValidasi(DateTimeLib.GetJulian(Convert.ToDateTime(paramModel.FatcaDate)).ToString()) + "</ns1:FatcaJulianDate>");

                strRqDetail.AppendLine("<ns1:KodeUsaha>" + XmlValidasi(paramModel.RiskCode) + "</ns1:KodeUsaha>");
                strRqDetail.AppendLine("<ns1:Keterangan>" + XmlValidasi(paramModel.RiskDescription) + "</ns1:Keterangan>");
                strRqDetail.AppendLine("<ns1:HighRiskUser>" + XmlValidasi(paramModel.HighriskUser) + "</ns1:HighRiskUser>");

                #endregion

                using (clsWebServiceCIF cWS = new clsWebServiceCIF(paramModel.NIK, paramModel.GUID, paramModel.Module, _iconfiguration["wsProCIFURL"]))
                {
                    blnResult = cWS.CallCIFChangeCIFBasicInformationV2MaintPROCIF01822(strRqDetail.ToString(), out strResult, out strErr);
                    doLogging(API_LOGENVELOPModel.INFO, "Data Checking strResult: " + GetMethodName(), "", strResult, null, paramModel.GUID, paramModel.NIK, paramModel.Module, paramModel.Branch);
                    if (strErr.Length > 0)
                        doLogging(API_LOGENVELOPModel.INFO, "Data Checking strErr: " + GetMethodName(), "", strErr, null, paramModel.GUID, paramModel.NIK, paramModel.Module, paramModel.Branch);

                }

                if (blnResult)
                {
                    DataSet dsOut = null;
                    try
                    {
                        if (ConvertRsSubDetailToXML(strResult, out dsOut))
                        {
                            #region MaintenanceCIFResult
                            //msgResponse.Data = new GeneralCIFModel();
                            //msgResponse.Data.CustomerNumber = dsOut.Tables[0].Rows[0]["Customernumber"].ToString().Trim();
                            //msgResponse.Data.BranchNumber = dsOut.Tables[0].Rows[0]["Branchnumber"].ToString().Trim();
                            ////msgResponse.Data.CustomerNameControl = dsOut.Tables[0].Rows[0]["ControlingBranch"].ToString().Trim();
                            //msgResponse.Data.CustomerTypeCode = dsOut.Tables[0].Rows[0]["Customertypecode"].ToString().Trim();
                            //msgResponse.Data.Subclass = dsOut.Tables[0].Rows[0]["Subclass"].ToString().Trim();
                            //msgResponse.Data.ResidentCode = dsOut.Tables[0].Rows[0]["Residentcode"].ToString().Trim();
                            //msgResponse.Data.IDTypeCode = dsOut.Tables[0].Rows[0]["IDtypecode"].ToString().Trim();
                            //msgResponse.Data.IDNumber = dsOut.Tables[0].Rows[0]["IDnumber"].ToString().Trim();

                            //msgResponse.Data.IDIssuePlace = dsOut.Tables[0].Rows[0]["IDIssuePlace"].ToString().Trim();
                            //msgResponse.Data.IDStatusCode = dsOut.Tables[0].Rows[0]["IDStatusCode"].ToString().Trim();

                            //msgResponse.Data.InsiderCode = dsOut.Tables[0].Rows[0]["Insidercode"].ToString().Trim();
                            //msgResponse.Data.InquiryCode = dsOut.Tables[0].Rows[0]["Inquirycode"].ToString().Trim();
                            //msgResponse.Data.BusinessType = dsOut.Tables[0].Rows[0]["Businesstype"].ToString().Trim();
                            //msgResponse.Data.InternalIndustryCode = dsOut.Tables[0].Rows[0]["InternalIndustrycode"].ToString().Trim();
                            //msgResponse.Data.TaxCode = dsOut.Tables[0].Rows[0]["FedWHcode"].ToString().Trim();
                            //msgResponse.Data.Sexcode = dsOut.Tables[0].Rows[0]["Sexcode"].ToString().Trim();
                            //msgResponse.Data.Religion = dsOut.Tables[0].Rows[0]["Countryofheritage"].ToString().Trim();
                            //msgResponse.Data.CountryOfCitizenship = dsOut.Tables[0].Rows[0]["Countryofcitizenship"].ToString().Trim();
                            //msgResponse.Data.CountryOfResidence = dsOut.Tables[0].Rows[0]["Country"].ToString().Trim();
                            //msgResponse.Data.VIPCustomerCode = dsOut.Tables[0].Rows[0]["VIPcustomercode"].ToString().Trim();
                            //msgResponse.Data.ProfitAnalysis = dsOut.Tables[0].Rows[0]["Profitanalysis"].ToString().Trim();
                            //msgResponse.Data.CIFCombinedCycle = dsOut.Tables[0].Rows[0]["CIFcombinedcycle"].ToString().Trim();
                            //msgResponse.Data.HoldmailCode = dsOut.Tables[0].Rows[0]["Holdmailcode"].ToString().Trim();
                            //msgResponse.Data.Retention = dsOut.Tables[0].Rows[0]["Retention"].ToString().Trim();

                            //msgResponse.Data.SIC1Userdefined = dsOut.Tables[0].Rows[0]["SIC1userdefined"].ToString().Trim();
                            //msgResponse.Data.SIC2Userdefined = dsOut.Tables[0].Rows[0]["SIC2userdefined"].ToString().Trim();
                            //msgResponse.Data.SIC3Userdefined = dsOut.Tables[0].Rows[0]["SIC3userdefined"].ToString().Trim();
                            //msgResponse.Data.SIC4Userdefined = dsOut.Tables[0].Rows[0]["SIC4userdefined"].ToString().Trim();
                            //msgResponse.Data.SIC5Userdefined = dsOut.Tables[0].Rows[0]["SIC5userdefined"].ToString().Trim();
                            //msgResponse.Data.SIC6Userdefined = dsOut.Tables[0].Rows[0]["SIC6userdefined"].ToString().Trim();
                            //msgResponse.Data.SIC7Userdefined = dsOut.Tables[0].Rows[0]["SIC7userdefined"].ToString().Trim();
                            //msgResponse.Data.SIC8Userdefined = dsOut.Tables[0].Rows[0]["SIC8userdefined"].ToString().Trim();

                            //msgResponse.Data.UIC1Userdefined = dsOut.Tables[0].Rows[0]["UIC1userdefined"].ToString().Trim();
                            //msgResponse.Data.UIC2Userdefined = dsOut.Tables[0].Rows[0]["UIC2userdefined"].ToString().Trim();
                            //msgResponse.Data.UIC3Userdefined = dsOut.Tables[0].Rows[0]["UIC3userdefined"].ToString().Trim();
                            //msgResponse.Data.UIC4Userdefined = dsOut.Tables[0].Rows[0]["UIC4userdefined"].ToString().Trim();
                            //msgResponse.Data.UIC5Userdefined = dsOut.Tables[0].Rows[0]["UIC5userdefined"].ToString().Trim();
                            //msgResponse.Data.UIC6Userdefined = dsOut.Tables[0].Rows[0]["UIC6userdefined"].ToString().Trim();
                            //msgResponse.Data.UIC7Userdefined = dsOut.Tables[0].Rows[0]["UIC7userdefined"].ToString().Trim();
                            //msgResponse.Data.UIC8Userdefined = dsOut.Tables[0].Rows[0]["UIC8userdefined"].ToString().Trim();

                            //msgResponse.Data.LastChangedByuser = dsOut.Tables[0].Rows[0]["Lastchangedbyuser"].ToString().Trim();
                            //msgResponse.Data.MotherMaidenName = dsOut.Tables[0].Rows[0]["MotherMaidenName"].ToString().Trim();
                            //msgResponse.Data.IsBlackList = dsOut.Tables[0].Rows[0]["BlacklistProceed"].ToString().Trim();
                            //msgResponse.Data.IsDuplicateID = dsOut.Tables[0].Rows[0]["CheckIDTupYN"].ToString().Trim();
                            //msgResponse.Data.BusinessUnitCode = dsOut.Tables[0].Rows[0]["BusinessUnitCode"].ToString().Trim();
                            //msgResponse.Data.CIFGroupMemberCode = dsOut.Tables[0].Rows[0]["CIFGroupMemberCode"].ToString().Trim();
                            //msgResponse.Data.IsDuplicateID = dsOut.Tables[0].Rows[0]["DuplicateID"].ToString().Trim();
                            ////msgResponse.Data. = dsOut.Tables[0].Rows[0]["MemberTypeShareholderYN"].ToString().Trim();
                            ////msgResponse.Data. = dsOut.Tables[0].Rows[0]["MemberTypeManagementYN"].ToString().Trim();
                            ////msgResponse.Data. = dsOut.Tables[0].Rows[0]["MemberTypeFinancialYN"].ToString().Trim();
                            ////msgResponse.Data. = dsOut.Tables[0].Rows[0]["GroupchangeFlag"].ToString().Trim();

                            //msgResponse.Data.IsHighriskCustomer = dsOut.Tables[0].Rows[0]["Highriskcustomerflag"].ToString().Trim();
                            //msgResponse.Data.UserID = dsOut.Tables[0].Rows[0]["USERENTRY"].ToString().Trim();
                            //msgResponse.Data.KodeLaporanTransaksiTunai = dsOut.Tables[0].Rows[0]["CODELAPTXTUNAI"].ToString().Trim();

                            //msgResponse.Data.EmployeeNIK = dsOut.Tables[0].Rows[0]["Employeecode"].ToString().Trim();
                            //msgResponse.Data.EmployeeName = dsOut.Tables[0].Rows[0]["Employeename"].ToString().Trim();
                            //msgResponse.Data.EmployeeIndicator = dsOut.Tables[0].Rows[0]["EmployeeIndicator"].ToString().Trim();
                            //msgResponse.Data.FatcaFlag = dsOut.Tables[0].Rows[0]["FatcaFlag"].ToString().Trim();
                            //if (Convert.ToInt32(dsOut.Tables[0].Rows[0]["FatcaJulianDate"].ToString().Trim()) == 0)
                            //{
                            //    msgResponse.Data.FatcaDate = "0";
                            //}
                            //else
                            //{
                            //    msgResponse.Data.FatcaDate = paramModel.FatcaDate;
                            //}

                            //msgResponse.Data.RiskCode = dsOut.Tables[0].Rows[0]["KodeUsaha"].ToString().Trim();
                            //msgResponse.Data.RiskDescription = dsOut.Tables[0].Rows[0]["Keterangan"].ToString().Trim();
                            //msgResponse.Data.HighriskUser = dsOut.Tables[0].Rows[0]["HighRiskUser"].ToString().Trim();

                            //msgResponse.Data.BirthDate = paramModel.BirthDate;
                            //msgResponse.Data.IdIssuedDate = paramModel.IdIssuedDate;
                            //msgResponse.Data.DateIDExpiry = paramModel.DateIDExpiry;
                            //msgResponse.Data.OriginalCustomerDate = paramModel.OriginalCustomerDate;
                            //msgResponse.Data.LastChangeDate = paramModel.LastChangeDate;
                            //msgResponse.Data.FedWHDate = paramModel.FedWHDate;
                            //msgResponse.Data.TransactionDate = paramModel.TransactionDate;
                            //msgResponse.Data.InformationDate = paramModel.InformationDate;
                            #endregion

                            //insert crs
                            if (paramModel.CRSFlag == "Y")
                            {
                                InsertCRSFlag(paramModel.CustomerNumber, paramModel.CRSFlag, DateTimeLib.GetDateTimeInt(paramModel.CRSDate,"yyyy-MM-dd","yyyyMMdd").ToString());
                            }        
                            
                            if (paramModel.RiskCode != "")
                            {
                                InsertProCIFRiskProfile_TM(paramModel.CustomerNumber.ToString().PadLeft(19,'0'));
                            }

                            msgResponse.Data = new GeneralCIFModel();
                            msgResponse.Data = paramModel;
                            msgResponse.IsSuccess = true;
                            msgResponse.Description = "";

                            if (modelOld != null)
                            {
                                this.ProcessToProCIFChangeMaster(paramModel, modelOld, isAutoApprove);                                
                            }
                        }
                        else
                        {
                            msgResponse.Data = null;
                            msgResponse.IsSuccess = false;
                            msgResponse.Description = "ConvertRsSubDetailToXML Failed!";
                        }
                    }
                    catch (Exception ex)
                    {
                        msgResponse.Data = null;
                        msgResponse.IsSuccess = false;
                        //msgResponse.Description = ex.Message + Environment.NewLine + ex.StackTrace.ToString();
                        msgResponse.Description = API_LOGENVELOPModel.CATCH_DESC_EXCEPTION;
                        doLogging(API_LOGENVELOPModel.ERROR, "Catch Exception : " + GetMethodName(), "", msgResponse, ex, paramModel.GUID, paramModel.NIK, paramModel.Module, paramModel.Branch);
                    }
                }
                else
                {
                    msgResponse.Data = null;
                    msgResponse.IsSuccess = false;
                    msgResponse.Description = strErr;
                }
            }
            catch (Exception ex)
            {
                msgResponse.Data = null;
                msgResponse.IsSuccess = false;
                //msgResponse.Description = ex.Message + Environment.NewLine + ex.StackTrace.ToString();
                msgResponse.Description = API_LOGENVELOPModel.CATCH_DESC_EXCEPTION;
                doLogging(API_LOGENVELOPModel.ERROR, "Catch Exception : " + GetMethodName(), "", msgResponse, ex, paramModel.GUID, paramModel.NIK, paramModel.Module, paramModel.Branch);
            }
            return msgResponse;
        }

        public local.APIMessage<GeneralCIFModel> InquiryDetail(GeneralCIFModel paramModel)
        {
            doLogging(API_LOGENVELOPModel.INFO, "Executing : " + GetMethodName(), "", null, null, paramModel.GUID, paramModel.NIK, paramModel.Module, paramModel.Branch);

            local.APIMessage<GeneralCIFModel> msgResponse = new local.APIMessage<GeneralCIFModel>();
            StringBuilder strRqDetail = new StringBuilder();//pembentukan RqDetail + nanti assign value dari client

            bool blnResult = false;
            string strResult = "", strErr = "";

            try
            {
                #region MaintenanceCIFXML
                strRqDetail.AppendLine("<ns1:Customernumber>" + XmlValidasi(paramModel.CustomerNumber.ToString()) + "</ns1:Customernumber>");
                #endregion

                using (clsWebServiceCIF cWS = new clsWebServiceCIF(paramModel.NIK, paramModel.GUID, paramModel.Module, _iconfiguration["wsProCIFURL"]))
                {
                    blnResult = cWS.CallCIFInquiryBasicInfoDetails01600(strRqDetail.ToString(), out strResult, out strErr);
                    doLogging(API_LOGENVELOPModel.INFO, "Data Checking strResult: " + GetMethodName(), "", strResult, null, paramModel.GUID, paramModel.NIK, paramModel.Module, paramModel.Branch);
                    if (strErr.Length > 0)
                        doLogging(API_LOGENVELOPModel.INFO, "Data Checking strErr: " + GetMethodName(), "", strErr, null, paramModel.GUID, paramModel.NIK, paramModel.Module, paramModel.Branch);

                }

                if (blnResult)
                {
                    DataSet dsOut = null;
                    try
                    {

                        if (ConvertRsSubDetailToXML(strResult, out dsOut))
                        {
                            #region MaintenanceCIFResult
                            msgResponse.Data = new GeneralCIFModel();
                            msgResponse.Data.BranchNumber = dsOut.Tables[0].Rows[0]["Branchnumber"].ToString().Trim();

                            msgResponse.Data.CustomerTypeCode = dsOut.Tables[0].Rows[0]["Customertypecode"].ToString().Trim();
                            msgResponse.Data.Subclass = dsOut.Tables[0].Rows[0]["Subclass"].ToString().Trim();
                            msgResponse.Data.ResidentCode = dsOut.Tables[0].Rows[0]["Residentcode"].ToString().Trim();
                            msgResponse.Data.IDTypeCode = dsOut.Tables[0].Rows[0]["IDtypecode"].ToString().Trim();
                            msgResponse.Data.IDNumber = dsOut.Tables[0].Rows[0]["IDnumber"].ToString().Trim();

                            msgResponse.Data.IDIssuePlace = dsOut.Tables[0].Rows[0]["IDIssuePlace"].ToString().Trim();
                            msgResponse.Data.IDStatusCode = dsOut.Tables[0].Rows[0]["IDStatusCode"].ToString().Trim();

                            msgResponse.Data.BirthPlace = dsOut.Tables[0].Rows[0]["BirthIncorporationdate"].ToString().Trim();

                            msgResponse.Data.InsiderCode = dsOut.Tables[0].Rows[0]["Insidercode"].ToString().Trim();
                            msgResponse.Data.InquiryCode = dsOut.Tables[0].Rows[0]["Inquirycode"].ToString().Trim();
                            msgResponse.Data.BusinessType = dsOut.Tables[0].Rows[0]["Businesstype"].ToString().Trim();
                            msgResponse.Data.InternalIndustryCode = dsOut.Tables[0].Rows[0]["InternalIndustrycode"].ToString().Trim();
                            msgResponse.Data.TaxCode = dsOut.Tables[0].Rows[0]["FedWHcode"].ToString().Trim();
                            msgResponse.Data.Sexcode = dsOut.Tables[0].Rows[0]["Sexcode"].ToString().Trim();
                            msgResponse.Data.Religion = dsOut.Tables[0].Rows[0]["Countryofheritage"].ToString().Trim();
                            msgResponse.Data.CountryOfCitizenship = dsOut.Tables[0].Rows[0]["Countryofcitizenship"].ToString().Trim();
                            msgResponse.Data.CountryOfResidence = dsOut.Tables[0].Rows[0]["Country"].ToString().Trim();
                            msgResponse.Data.VIPCustomerCode = dsOut.Tables[0].Rows[0]["VIPcustomercode"].ToString().Trim();
                            msgResponse.Data.ProfitAnalysis = dsOut.Tables[0].Rows[0]["Profitanalysis"].ToString().Trim();
                            msgResponse.Data.CIFCombinedCycle = dsOut.Tables[0].Rows[0]["CIFcombinedcycle"].ToString().Trim();
                            msgResponse.Data.HoldmailCode = dsOut.Tables[0].Rows[0]["Holdmailcode"].ToString().Trim();
                            msgResponse.Data.Retention = dsOut.Tables[0].Rows[0]["Retention"].ToString().Trim();

                            msgResponse.Data.SIC1Userdefined = dsOut.Tables[0].Rows[0]["SIC1userdefined"].ToString().Trim();
                            msgResponse.Data.SIC2Userdefined = dsOut.Tables[0].Rows[0]["SIC2userdefined"].ToString().Trim();
                            msgResponse.Data.SIC3Userdefined = dsOut.Tables[0].Rows[0]["SIC3userdefined"].ToString().Trim();
                            msgResponse.Data.SIC4Userdefined = dsOut.Tables[0].Rows[0]["SIC4userdefined"].ToString().Trim();
                            msgResponse.Data.SIC5Userdefined = dsOut.Tables[0].Rows[0]["SIC5userdefined"].ToString().Trim();
                            msgResponse.Data.SIC6Userdefined = dsOut.Tables[0].Rows[0]["SIC6userdefined"].ToString().Trim();
                            msgResponse.Data.SIC7Userdefined = dsOut.Tables[0].Rows[0]["SIC7userdefined"].ToString().Trim();
                            msgResponse.Data.SIC8Userdefined = dsOut.Tables[0].Rows[0]["SIC8userdefined"].ToString().Trim();

                            msgResponse.Data.UIC1Userdefined = dsOut.Tables[0].Rows[0]["UIC1userdefined"].ToString().Trim();
                            msgResponse.Data.StatusPernikahan = dsOut.Tables[0].Rows[0]["UIC2userdefined"].ToString().Trim();
                            msgResponse.Data.LevelPendidikan = dsOut.Tables[0].Rows[0]["UIC3userdefined"].ToString().Trim();
                            msgResponse.Data.UIC4Userdefined = dsOut.Tables[0].Rows[0]["UIC4userdefined"].ToString().Trim();
                            msgResponse.Data.UIC5Userdefined = dsOut.Tables[0].Rows[0]["UIC5userdefined"].ToString().Trim();
                            msgResponse.Data.UIC6Userdefined = dsOut.Tables[0].Rows[0]["UIC6userdefined"].ToString().Trim();
                            msgResponse.Data.RatarataTransaksi = dsOut.Tables[0].Rows[0]["UIC7userdefined"].ToString().Trim();
                            msgResponse.Data.RiskProfile = dsOut.Tables[0].Rows[0]["UIC8userdefined"].ToString().Trim();
                            msgResponse.Data.MotherMaidenName = dsOut.Tables[0].Rows[0]["MotherMaidenName"].ToString().Trim();
                            msgResponse.Data.BusinessUnitCode = dsOut.Tables[0].Rows[0]["BusinessUnitCode"].ToString().Trim();
                            msgResponse.Data.CIFGroupMemberCode = dsOut.Tables[0].Rows[0]["CIFGroupMemberCode"].ToString().Trim();


                            msgResponse.Data.TitleBeforeName = dsOut.Tables[0].Rows[0]["TitleBeforeName"].ToString().Trim();
                            msgResponse.Data.TitleAfterName = dsOut.Tables[0].Rows[0]["TitleAfterName"].ToString().Trim();
                            msgResponse.Data.CustomerName1 = dsOut.Tables[0].Rows[0]["Customername"].ToString().Trim();
                            msgResponse.Data.CustomerName2 = dsOut.Tables[0].Rows[0]["Customername2"].ToString().Trim();
                            msgResponse.Data.CustomerNumber = paramModel.CustomerNumber;

                            if (Convert.ToInt32(dsOut.Tables[0].Rows[0]["IDExpirydateDMY"].ToString().Trim()) > 0)
                            {
                                try
                                {
                                    msgResponse.Data.DateIDExpiry = DateTimeLib.GetDateTime(Convert.ToInt32(dsOut.Tables[0].Rows[0]["IDExpirydateDMY"].ToString().Trim()), "ddMMyy").ToString("yyyy-MM-dd");
                                }
                                catch { }
                            }

                            if (Convert.ToInt32(dsOut.Tables[0].Rows[0]["OrgcustdteDMY"].ToString().Trim()) > 0)
                            {
                                msgResponse.Data.OriginalCustomerDate = DateTimeLib.GetDateTime(Convert.ToInt32(dsOut.Tables[0].Rows[0]["OrgcustdteDMY"].ToString().Trim()), "ddMMyy");
                            }

                            if (Convert.ToInt32(dsOut.Tables[0].Rows[0]["BirthincorporationdateYYYYDDD"].ToString().Trim()) > 0)
                            {
                                msgResponse.Data.BirthDate = DateTimeLib.GetDateTime(Convert.ToInt32(dsOut.Tables[0].Rows[0]["BirthincorporationdateYYYYDDD"].ToString().Trim()), "ddMMyy");
                            }

                            msgResponse.Data.HaveMessage = dsOut.Tables[0].Rows[0]["HaveMessage"].ToString().Trim();

                            if (msgResponse.Data.HaveMessage == "Y")
                            {
                                msgResponse.Data.ListOfMessage = new List<string>();
                                //karena ada 10 message
                                for (int i = 1; i < -10; i++)
                                {
                                    msgResponse.Data.ListOfMessage.Add(dsOut.Tables[0].Rows[0]["Message" + i.ToString()].ToString().Trim());
                                }
                            }

                            msgResponse.Data.NIK = paramModel.NIK;
                            msgResponse.Data.Branch = paramModel.Branch;
                            msgResponse.Data.Module = paramModel.Module;
                            msgResponse.Data.GUID = paramModel.GUID;
                            msgResponse.Data.MoreIndicator = paramModel.MoreIndicator;
                            #endregion

                            msgResponse.IsSuccess = true;
                            msgResponse.Description = "";
                        }
                        else
                        {
                            msgResponse.Data = null;
                            msgResponse.IsSuccess = false;
                            msgResponse.Description = "ConvertRsSubDetailToXML Failed!";
                        }
                    }
                    catch (Exception ex)
                    {
                        msgResponse.Data = null;
                        msgResponse.IsSuccess = false;
                        //msgResponse.Description = ex.Message + Environment.NewLine + ex.StackTrace.ToString();
                        msgResponse.Description = API_LOGENVELOPModel.CATCH_DESC_EXCEPTION;
                        doLogging(API_LOGENVELOPModel.ERROR, "Catch Exception : " + GetMethodName(), "", msgResponse, ex, paramModel.GUID, paramModel.NIK, paramModel.Module, paramModel.Branch);

                    }
                }
                else
                {
                    msgResponse.Data = null;
                    msgResponse.IsSuccess = false;
                    msgResponse.Description = strErr;
                }
            }
            catch (Exception ex)
            {
                msgResponse.Data = null;
                msgResponse.IsSuccess = false;
                //msgResponse.Description = ex.Message + Environment.NewLine + ex.StackTrace.ToString();
                msgResponse.Description = API_LOGENVELOPModel.CATCH_DESC_EXCEPTION;
                doLogging(API_LOGENVELOPModel.ERROR, "Catch Exception : " + GetMethodName(), "", msgResponse, ex, paramModel.GUID, paramModel.NIK, paramModel.Module, paramModel.Branch);

            }
            return msgResponse;
        }

        public local.APIMessage<GeneralCIFModel> InquiryDetailV2(GeneralCIFModel paramModel)
        {
            doLogging(API_LOGENVELOPModel.INFO, "Executing : " + GetMethodName(), "", null, null, paramModel.GUID, paramModel.NIK, paramModel.Module, paramModel.Branch);

            local.APIMessage<GeneralCIFModel> msgResponse = new local.APIMessage<GeneralCIFModel>();
            StringBuilder strRqDetail = new StringBuilder();//pembentukan RqDetail + nanti assign value dari client

            bool blnResult = false;
            string strResult = "", strErr = "";

            try
            {
                #region MaintenanceCIFXML
                strRqDetail.AppendLine("<ns1:Customernumber>" + XmlValidasi(paramModel.CustomerNumber.ToString()) + "</ns1:Customernumber>");
                #endregion

                using (clsWebServiceCIF cWS = new clsWebServiceCIF(paramModel.NIK, paramModel.GUID, paramModel.Module, _iconfiguration["wsProCIFURL"]))
                {
                    blnResult = cWS.CallCIFInquiryCIFDetail01610(strRqDetail.ToString(), out strResult, out strErr);
                    doLogging(API_LOGENVELOPModel.INFO, "Data Checking strResult: " + GetMethodName(), "", strResult, null, paramModel.GUID, paramModel.NIK, paramModel.Module, paramModel.Branch);
                    if (strErr.Length > 0)
                        doLogging(API_LOGENVELOPModel.INFO, "Data Checking strErr: " + GetMethodName(), "", strErr, null, paramModel.GUID, paramModel.NIK, paramModel.Module, paramModel.Branch);
                }

                if (blnResult)
                {
                    DataSet dsOut = null;
                    try
                    {

                        if (ConvertRsSubDetailToXML(strResult, out dsOut))
                        {
                            #region MaintenanceCIFResult
                            msgResponse.Data = new GeneralCIFModel();
                            msgResponse.Data.CustomerNumber = paramModel.CustomerNumber;
                            msgResponse.Data.BranchNumber = dsOut.Tables[0].Rows[0]["Branchnumber"].ToString().Trim();
                            //msgResponse.Data.BranchNumber = dsOut.Tables[0].Rows[0]["ControlingBranch"].ToString().Trim();
                            msgResponse.Data.CustomerTypeCode = dsOut.Tables[0].Rows[0]["Customertypecode"].ToString().Trim();
                            msgResponse.Data.Subclass = dsOut.Tables[0].Rows[0]["Subclass"].ToString().Trim();
                            msgResponse.Data.ResidentCode = dsOut.Tables[0].Rows[0]["Residentcode"].ToString().Trim();
                            msgResponse.Data.IDTypeCode = dsOut.Tables[0].Rows[0]["IDtypecode"].ToString().Trim();
                            msgResponse.Data.IDNumber = dsOut.Tables[0].Rows[0]["IDnumber"].ToString().Trim();
                            msgResponse.Data.IDIssuePlace = dsOut.Tables[0].Rows[0]["IDIssuePlace"].ToString().Trim();
                            msgResponse.Data.IDStatusCode = dsOut.Tables[0].Rows[0]["IDStatusCode"].ToString().Trim();
                            msgResponse.Data.BirthPlace = dsOut.Tables[0].Rows[0]["BirthIncorporationdate"].ToString().Trim();
                            msgResponse.Data.InsiderCode = dsOut.Tables[0].Rows[0]["Insidercode"].ToString().Trim();
                            msgResponse.Data.InquiryCode = dsOut.Tables[0].Rows[0]["Inquirycode"].ToString().Trim();
                            msgResponse.Data.BusinessType = dsOut.Tables[0].Rows[0]["Businesstype"].ToString().Trim();
                            msgResponse.Data.InternalIndustryCode = dsOut.Tables[0].Rows[0]["InternalIndustrycode"].ToString().Trim();
                            msgResponse.Data.TaxCode = dsOut.Tables[0].Rows[0]["FedWHcode"].ToString().Trim();
                            msgResponse.Data.Sexcode = dsOut.Tables[0].Rows[0]["Sexcode"].ToString().Trim();
                            msgResponse.Data.Religion = dsOut.Tables[0].Rows[0]["Countryofheritage"].ToString().Trim();
                            msgResponse.Data.CountryOfCitizenship = dsOut.Tables[0].Rows[0]["Countryofcitizenship"].ToString().Trim();
                            msgResponse.Data.CountryOfResidence = dsOut.Tables[0].Rows[0]["Country"].ToString().Trim();
                            msgResponse.Data.VIPCustomerCode = dsOut.Tables[0].Rows[0]["VIPcustomercode"].ToString().Trim();
                            msgResponse.Data.ProfitAnalysis = dsOut.Tables[0].Rows[0]["Profitanalysis"].ToString().Trim();
                            msgResponse.Data.CIFCombinedCycle = dsOut.Tables[0].Rows[0]["CIFcombinedcycle"].ToString().Trim();
                            msgResponse.Data.HoldmailCode = dsOut.Tables[0].Rows[0]["Holdmailcode"].ToString().Trim();
                            msgResponse.Data.Retention = dsOut.Tables[0].Rows[0]["Retention"].ToString().Trim();

                            msgResponse.Data.SIC1Userdefined = dsOut.Tables[0].Rows[0]["SIC1userdefined"].ToString().Trim();
                            msgResponse.Data.SIC2Userdefined = dsOut.Tables[0].Rows[0]["SIC2userdefined"].ToString().Trim();
                            msgResponse.Data.SIC3Userdefined = dsOut.Tables[0].Rows[0]["SIC3userdefined"].ToString().Trim();
                            msgResponse.Data.SIC4Userdefined = dsOut.Tables[0].Rows[0]["SIC4userdefined"].ToString().Trim();
                            msgResponse.Data.SIC5Userdefined = dsOut.Tables[0].Rows[0]["SIC5userdefined"].ToString().Trim();
                            msgResponse.Data.SIC6Userdefined = dsOut.Tables[0].Rows[0]["SIC6userdefined"].ToString().Trim();
                            msgResponse.Data.SIC7Userdefined = dsOut.Tables[0].Rows[0]["SIC7userdefined"].ToString().Trim();
                            msgResponse.Data.SIC8Userdefined = dsOut.Tables[0].Rows[0]["SIC8userdefined"].ToString().Trim();

                            msgResponse.Data.UIC1Userdefined = dsOut.Tables[0].Rows[0]["UIC1userdefined"].ToString().Trim();
                            msgResponse.Data.StatusPernikahan = dsOut.Tables[0].Rows[0]["UIC2userdefined"].ToString().Trim();
                            msgResponse.Data.LevelPendidikan = dsOut.Tables[0].Rows[0]["UIC3userdefined"].ToString().Trim();
                            msgResponse.Data.UIC4Userdefined = dsOut.Tables[0].Rows[0]["UIC4userdefined"].ToString().Trim();
                            msgResponse.Data.UIC5Userdefined = dsOut.Tables[0].Rows[0]["UIC5userdefined"].ToString().Trim();
                            msgResponse.Data.UIC6Userdefined = dsOut.Tables[0].Rows[0]["UIC6userdefined"].ToString().Trim();
                            msgResponse.Data.RatarataTransaksi = dsOut.Tables[0].Rows[0]["UIC7userdefined"].ToString().Trim();
                            msgResponse.Data.RiskProfile = dsOut.Tables[0].Rows[0]["UIC8userdefined"].ToString().Trim();

                            msgResponse.Data.LastChangedByuser = dsOut.Tables[0].Rows[0]["Lastchangedbyuser"].ToString().Trim();
                            msgResponse.Data.MotherMaidenName = dsOut.Tables[0].Rows[0]["MotherMaidenName"].ToString().Trim();


                            msgResponse.Data.BusinessUnitCode = dsOut.Tables[0].Rows[0]["BusinessUnitCode"].ToString().Trim();
                            msgResponse.Data.CIFGroupMemberCode = dsOut.Tables[0].Rows[0]["CIFGroupMemberCode"].ToString().Trim();

                            msgResponse.Data.UserID = dsOut.Tables[0].Rows[0]["USERENTRY"].ToString().Trim();
                            msgResponse.Data.HighriskUser = dsOut.Tables[0].Rows[0]["USERENTRY"].ToString().Trim();
                            msgResponse.Data.KodeLaporanTransaksiTunai = dsOut.Tables[0].Rows[0]["CODELAPTXTUNAI"].ToString().Trim();

                            msgResponse.Data.Contactname = dsOut.Tables[0].Rows[0]["ContactPerson"].ToString().Trim();
                            msgResponse.Data.TitleBeforeName = dsOut.Tables[0].Rows[0]["TitleBeforeName"].ToString().Trim();
                            msgResponse.Data.TitleAfterName = dsOut.Tables[0].Rows[0]["TitleAfterName"].ToString().Trim();
                            msgResponse.Data.CustomerName1 = dsOut.Tables[0].Rows[0]["Customername"].ToString().Trim();
                            msgResponse.Data.CustomerName2 = dsOut.Tables[0].Rows[0]["Customername2"].ToString().Trim();
                            msgResponse.Data.CustomerNumber = paramModel.CustomerNumber;

                            msgResponse.Data.EmployeeNIK = dsOut.Tables[0].Rows[0]["Employeecode"].ToString().Trim();

                            if (msgResponse.Data.EmployeeNIK.Trim().Contains("00000"))
                            {
                                msgResponse.Data.EmployeeNIK = "";
                            }

                            msgResponse.Data.EmployeeName = dsOut.Tables[0].Rows[0]["Employeename"].ToString().Trim();
                            msgResponse.Data.EmployeeIndicator = dsOut.Tables[0].Rows[0]["EmployeeIndicator"].ToString().Trim();
                            msgResponse.Data.FatcaFlag = dsOut.Tables[0].Rows[0]["FatcaFlag"].ToString().Trim();
                            if (Convert.ToInt32(dsOut.Tables[0].Rows[0]["FatcaJulianDate"].ToString().Trim()) == 0)
                            {
                                msgResponse.Data.FatcaDate = "0";
                            }
                            else
                            {
                                msgResponse.Data.FatcaDate = dsOut.Tables[0].Rows[0]["FatcaJulianDate"].ToString().Trim();
                                msgResponse.Data.FatcaDate = DateTimeLib.GetDatetimeFromJulian(msgResponse.Data.FatcaDate).ToString("yyyy-MM-dd");
                            }

                            if (Convert.ToInt32(dsOut.Tables[0].Rows[0]["IDExpirydate"].ToString().Trim()) > 0)
                            {
                                try
                                {
                                    msgResponse.Data.DateIDExpiry = DateTimeLib.GetDateTime(Convert.ToInt32(dsOut.Tables[0].Rows[0]["IDExpirydate"].ToString().Trim()), "ddMMyy").ToString("yyyy-MM-dd");
                                }
                                catch { }
                            }

                            if (Convert.ToInt32(dsOut.Tables[0].Rows[0]["OriginalcustomerdateMMDDYY"].ToString().Trim()) > 0)
                            {
                                msgResponse.Data.OriginalCustomerDate = DateTimeLib.GetDateTime(Convert.ToInt32(dsOut.Tables[0].Rows[0]["OriginalcustomerdateMMDDYY"].ToString().Trim()), "ddMMyy");
                            }

                            if (Convert.ToInt32(dsOut.Tables[0].Rows[0]["BirthincorporationdateDDMMYY"].ToString().Trim()) > 0)
                            {
                                msgResponse.Data.BirthDate = DateTimeLib.GetDateTime(Convert.ToInt32(dsOut.Tables[0].Rows[0]["BirthincorporationdateDDMMYY"].ToString().Trim()), "ddMMyy");
                            }

                            if (Convert.ToInt32(dsOut.Tables[0].Rows[0]["IdIssuedDateDDMMYY"].ToString().Trim()) > 0)
                            {
                                try
                                {
                                    msgResponse.Data.IdIssuedDate = DateTimeLib.GetDateTime(Convert.ToInt32(dsOut.Tables[0].Rows[0]["IdIssuedDateDDMMYY"].ToString().Trim()), "ddMMyy").ToString("yyyy-MM-dd");
                                }
                                catch { }
                            }

                            msgResponse.Data.RiskCode = dsOut.Tables[0].Rows[0]["KodeUsaha"].ToString().Trim();
                            msgResponse.Data.RiskDescription = dsOut.Tables[0].Rows[0]["Keterangan"].ToString().Trim();

                            local.APIMessage<DataSet> msgCRS = new local.APIMessage<DataSet>();
                            msgCRS = this.SubGetParameterMasterCRS(paramModel.CustomerNumber);

                            if (msgCRS.IsSuccess && msgCRS.Data != null && msgCRS.Data.Tables.Count > 0 && msgCRS.Data.Tables[0].Rows.Count > 0)
                            {
                                msgResponse.Data.CRSFlag = msgCRS.Data.Tables[0].Rows[0]["CRS"].ToString().Trim();
                                if (msgCRS.Data.Tables[0].Rows[0]["DateCRS"].ToString() == "0")
                                {
                                    msgResponse.Data.CRSDate = "";
                                }
                                else
                                {
                                    msgResponse.Data.CRSDate = DateTimeLib.GetDateTime(msgCRS.Data.Tables[0].Rows[0]["DateCRS"].ToString(), "yyyyMMdd").ToString("yyyy-MM-dd");
                                }
                            }
                            else
                            {
                                msgResponse.Data.CRSFlag = "";
                                msgResponse.Data.CRSDate = "";
                            }

                            msgResponse.Data.NIK = paramModel.NIK;
                            msgResponse.Data.Branch = paramModel.Branch;
                            msgResponse.Data.Module = paramModel.Module;
                            msgResponse.Data.GUID = paramModel.GUID;
                            msgResponse.Data.MoreIndicator = paramModel.MoreIndicator;
                            #endregion

                            msgResponse.IsSuccess = true;
                            msgResponse.Description = "";
                        }
                        else
                        {
                            msgResponse.Data = null;
                            msgResponse.IsSuccess = false;
                            msgResponse.Description = "ConvertRsSubDetailToXML Failed!";
                        }
                    }
                    catch (Exception ex)
                    {
                        msgResponse.Data = null;
                        msgResponse.IsSuccess = false;
                        //msgResponse.Description = ex.Message + Environment.NewLine + ex.StackTrace.ToString();
                        msgResponse.Description = API_LOGENVELOPModel.CATCH_DESC_EXCEPTION;
                        doLogging(API_LOGENVELOPModel.ERROR, "Catch Exception : " + GetMethodName(), "", msgResponse, ex, paramModel.GUID, paramModel.NIK, paramModel.Module, paramModel.Branch);

                    }
                }
                else
                {
                    msgResponse.Data = null;
                    msgResponse.IsSuccess = false;
                    msgResponse.Description = strErr;
                }
            }
            catch (Exception ex)
            {
                msgResponse.Data = null;
                msgResponse.IsSuccess = false;
                //msgResponse.Description = ex.Message + Environment.NewLine + ex.StackTrace.ToString();
                msgResponse.Description = API_LOGENVELOPModel.CATCH_DESC_EXCEPTION;
                doLogging(API_LOGENVELOPModel.ERROR, "Catch Exception : " + GetMethodName(), "", msgResponse, ex, paramModel.GUID, paramModel.NIK, paramModel.Module, paramModel.Branch);

            }
            return msgResponse;
        }

        public bool SubCheckBirthDateSeventeen(DateTime BirthDate, string CustomerType)
        {
            doLogging(API_LOGENVELOPModel.INFO, "Executing : " + GetMethodName(), "", BirthDate, null);

            bool blnResult = true;

            try
            {
                if (CustomerType == "A" && BirthDate.AddYears(17).Date > DateTime.Now.Date)
                {
                    blnResult = false;
                }
            }
            catch
            {
                blnResult = true;
            }

            return blnResult;
        }

        public local.APIMessage<List<ScreeningListModel>> SubCheckScreeningList(ScreeningListModel model)
        {
            doLogging(API_LOGENVELOPModel.INFO, "Executing : " + GetMethodName(), "", null, null, model.GUID, model.NIK, model.Module, model.Branch);

            local.APIMessage<List<ScreeningListModel>> msgResponse = new local.APIMessage<List<ScreeningListModel>>();            
            local.APIMessage<DataSet> msgResponseListType = new local.APIMessage<DataSet>();            

            try
            {
                bool blnReturn = false;
                DataSet dsOut = new DataSet();
                string DOB = "";

                if (!String.IsNullOrEmpty(model.BirthDate))
                {
                    DOB = DateTimeLib.GetJulian(Convert.ToDateTime(model.BirthDate)).ToString();
                }

                SqlParameter[] dbParam = new SqlParameter[4];
                dbParam[0] = new SqlParameter("@pcName", SqlDbType.VarChar);
                dbParam[0].Value = model.CustomerFullName;
                dbParam[0].Direction = ParameterDirection.Input;

                dbParam[1] = new SqlParameter("@pcDOB", SqlDbType.VarChar);
                dbParam[1].Value = DOB;
                dbParam[1].Direction = ParameterDirection.Input;

                dbParam[2] = new SqlParameter("@pcNational", SqlDbType.VarChar);
                dbParam[2].Value = model.Nationality;
                dbParam[2].Direction = ParameterDirection.Input;

                dbParam[3] = new SqlParameter("@pcNomorID", SqlDbType.VarChar);
                dbParam[3].Value = model.IDNumber;
                dbParam[3].Direction = ParameterDirection.Input;

                using (clsDBServiceSQLServer _clsDBServiceSQLServer = new clsDBServiceSQLServer())
                {
                    blnReturn = _clsDBServiceSQLServer.ExecStoredProcedure(_strCIFParameterConnectionString_SQL_CIF, "ListSearchScreening", ref dbParam, _nTimeOut, out dsOut);
                }                

                if (blnReturn)
                {
                    ScreeningListModel itemScreening = null;
                    List<ScreeningListModel> ListItemScreening = new List<ScreeningListModel>();
                    if (dsOut != null && dsOut.Tables.Count > 0 && dsOut.Tables[0].Rows.Count > 0)
                    {
                        itemScreening = new ScreeningListModel();
                        itemScreening.ListType = new List<string>();
                        foreach (DataRow row in dsOut.Tables[0].Rows)
                        {                            
                            itemScreening.CustomerFullName = row[0].ToString().Trim();
                            itemScreening.Alias = row[1].ToString().Trim();
                            itemScreening.IDType = row[2].ToString().Trim();
                            itemScreening.IDNumber = row[3].ToString().Trim();
                            itemScreening.BirthDate = row[4].ToString().Trim();
                            itemScreening.Nationality = row[5].ToString().Trim();                            
                            itemScreening.ListType.Add(row[6].ToString().Trim());

                            msgResponseListType = new local.APIMessage<DataSet>();
                            msgResponseListType = this.SubGetListTypeScreeningList(row[6].ToString().Trim());

                            if (msgResponseListType.IsSuccess)
                            {
                                if (msgResponseListType.Data != null && msgResponseListType.Data.Tables.Count > 0 && msgResponseListType.Data.Tables[0].Rows.Count > 0)
                                {                                    
                                    if (msgResponseListType.Data.Tables[0].Rows[0]["RejectGroup"].ToString().Trim().ToLower().Equals("dpn"))
                                    {
                                        itemScreening.IsDPN = "Y";
                                    }
                                    else if (msgResponseListType.Data.Tables[0].Rows[0]["RejectGroup"].ToString().Trim().ToLower().Equals("blacklist"))
                                    {
                                        itemScreening.IsBlacklist = "Y";
                                    }
                                    else if (msgResponseListType.Data.Tables[0].Rows[0]["RejectGroup"].ToString().Trim().ToLower().Equals("highrisk"))
                                    {
                                        itemScreening.IsHighrisk = "Y";
                                    }
                                    else
                                    {
                                        itemScreening.IsBlacklist = "Y";
                                    }
                                }
                                else
                                {
                                    itemScreening.IsBlacklist = "Y";
                                }
                            }
                            else
                            {
                                itemScreening.IsBlacklist = "Y";
                            }

                            itemScreening.Comment = row[7].ToString().Trim();
                            itemScreening.Reason = row[7].ToString().Trim();
                            itemScreening.IsNeedOvveride = true;

                            itemScreening.Branch = model.Branch;
                            itemScreening.NIK = model.NIK;
                            itemScreening.GUID = model.GUID;
                            itemScreening.Module = model.Module;
                            itemScreening.MoreIndicator = model.MoreIndicator;                            
                        }
                        if (itemScreening.IsNeedOvveride)
                            ListItemScreening.Add(itemScreening);
                        else
                            ListItemScreening = null;
                    }

                    msgResponse.IsSuccess = true;
                    msgResponse.Description = "";
                    msgResponse.Data = ListItemScreening;
                }
                else
                {
                    msgResponse.IsSuccess = false;
                    msgResponse.Description = "Unhandle Error On Check Screening List";
                    msgResponse.Data = null;
                }

            }
            catch (Exception ex)
            {
                msgResponse.IsSuccess = false;
                //msgResponse.Description = ex.Message;
                msgResponse.Data = null;
                msgResponse.Description = API_LOGENVELOPModel.CATCH_DESC_EXCEPTION;
                doLogging(API_LOGENVELOPModel.ERROR, "Catch Exception : " + GetMethodName(), "", null, ex, model.GUID, model.NIK, model.Module, model.Branch);
            }

            return msgResponse;
        }

        public local.APIMessage<DataSet> SubCheckScreeningNegara(string strKodeNegara, out string strError)
        {
            doLogging(API_LOGENVELOPModel.INFO, "Executing : " + GetMethodName(), "", strKodeNegara, null);

            DataSet dsOut = null;
            local.APIMessage<DataSet> msgResponse = new local.APIMessage<DataSet>();            
            
            string sqlCommand = "select JHCCOC as KodeNegara, JHCNAM as NamaNegara, Warning, Reject, JHCH from ProCIFP4NegaraHRCM_TR where JHCCOC = @cCodeNegara";
            strError = "";

            SqlParameter[] dbParam = new SqlParameter[1];
            dbParam[0] = new SqlParameter("@cCodeNegara", SqlDbType.Char, 3);
            dbParam[0].Value = strKodeNegara.PadLeft(3,'0');
            dbParam[0].Direction = System.Data.ParameterDirection.Input;

            bool blnResult;
            using (clsDBServiceSQLServer _clsDBServiceSQLServer = new clsDBServiceSQLServer())
            {
                blnResult = _clsDBServiceSQLServer.QueryCommand(_strCIFParameterConnectionString_CFMAST, sqlCommand, ref dbParam, _nTimeOut, out dsOut, out strError);
            }

            if (blnResult)
            {
                msgResponse.IsSuccess = true;
                msgResponse.Description = strError;
                msgResponse.Data = new DataSet();
                msgResponse.Data = dsOut.Copy();
            }
            else
            {
                msgResponse.IsSuccess = true;
                msgResponse.Description = strError;
                msgResponse.Data = null;
                doLogging(API_LOGENVELOPModel.ERROR, "Error Exception : " + GetMethodName(), "", msgResponse, null);
                msgResponse.Description = API_LOGENVELOPModel.CATCH_DESC_EXCEPTION;
            }            

            return msgResponse;
        }

        #endregion

        #region Approval
        public local.APIMessage<DataSet> SubCheckApproval(string paramCIF, string paramTable, string paramGUID, string paramChangeType)
        {
            string[] var = { paramCIF, paramTable, paramGUID, paramChangeType };
            doLogging(API_LOGENVELOPModel.INFO, "Executing : " + GetMethodName(), "", var);

            local.APIMessage<DataSet> msgResponse = new local.APIMessage<DataSet>();
            try
            {
                DataSet dsOut = new DataSet();
                string strError = "";

                string sqlCommand = "SELECT * FROM [DataChangeMaster_TM] WHERE CIFNumber=@paramCIF and Status ='PENDING';";
                SqlParameter[] dbParam = new SqlParameter[3];
              
                dbParam[0] = new SqlParameter("@paramCIF", SqlDbType.VarChar);
                dbParam[0].Value = paramCIF;
                dbParam[0].Direction = System.Data.ParameterDirection.Input;

                dbParam[1] = new SqlParameter("@paramTable", SqlDbType.VarChar);
                dbParam[1].Value = paramTable;
                dbParam[1].Direction = System.Data.ParameterDirection.Input;

                dbParam[2] = new SqlParameter("@paramChangeType", SqlDbType.VarChar);
                dbParam[2].Value = paramChangeType;
                dbParam[2].Direction = System.Data.ParameterDirection.Input;

                using (clsDBServiceSQLServer _clsDBServiceSQLServer = new clsDBServiceSQLServer())
                {
                    msgResponse.IsSuccess = _clsDBServiceSQLServer.QueryCommand(_strCIFParameterConnectionString_CFMAST, sqlCommand, ref dbParam, _nTimeOut, out dsOut, out strError);
                    msgResponse.Data = dsOut;
                    msgResponse.Description = strError;

                    if (strError.Length > 0)
                    {
                        doLogging(API_LOGENVELOPModel.ERROR, "Error Exception : " + GetMethodName(), "", msgResponse, null);
                        msgResponse.Description = API_LOGENVELOPModel.CATCH_DESC_EXCEPTION;
                    }
                }
            }
            catch (Exception ex)
            {
                msgResponse.IsSuccess = false;
                msgResponse.Data = new DataSet();
                //msgResponse.Description = ex.Message;
                msgResponse.Description = API_LOGENVELOPModel.CATCH_DESC_EXCEPTION;
                doLogging(API_LOGENVELOPModel.ERROR, "Catch Exception : " + GetMethodName(), "", null, ex);

            }

            return msgResponse;
        }

        public local.APIMessageDetail<GeneralCIFModel> SubSaveApprovalMaintenanceCIF(ApprovalMasterModel modelMasterApp, List<ApprovalDetailModel> modelDetailApp, bool isAutoApprove = false)
        {
            doLogging(API_LOGENVELOPModel.INFO, "Executing : " + GetMethodName(), "", null, null, modelMasterApp.GUID, modelMasterApp.StaffNIK,"", modelMasterApp.Branch);

            local.APIMessageDetail<GeneralCIFModel> msgResponse = new local.APIMessageDetail<GeneralCIFModel>();
            try
            {
                StringBuilder batchInsert = new StringBuilder();
                batchInsert.AppendLine("BEGIN TRY");
                batchInsert.AppendLine("BEGIN TRANSACTION;");
                batchInsert.AppendLine("DECLARE @ChangeId bigint;");
                batchInsert.AppendLine("INSERT INTO [dbo].[DataChangeMaster_TM] ([Branch],[CIFNumber],[CIFName],[TableName],[Status],[ChangeType],[ChangeDate],[StaffNIK],[GUID], JsonDataNew, JsonDataOld, SubSystem)");
                batchInsert.AppendLine("VALUES (@Branch, @CIFNumber ,@CIFName ,@TableName ,@Status ,@ChangeType ,GETDATE() , @StaffNIK, @GUID, @JsonDataNew, @JsonDataOld, @SubSystem);");
                batchInsert.AppendLine("SET @ChangeId = @@identity; SELECT @ChangeId;");

                List<Dictionary<string, string>> listOfDataValue = new List<Dictionary<string, string>>();
                Dictionary<string, string> dataValueMaster = new Dictionary<string, string>();
                Dictionary<string, string> dataValue = new Dictionary<string, string>();

                dataValueMaster.Add("@Branch", modelMasterApp.Branch);
                dataValueMaster.Add("@CIFNumber", modelMasterApp.CIFNumber);
                dataValueMaster.Add("@CIFName", modelMasterApp.CIFName);
                dataValueMaster.Add("@TableName", modelMasterApp.TableName);
                dataValueMaster.Add("@Status", modelMasterApp.Status);
                dataValueMaster.Add("@ChangeType", modelMasterApp.ChangeType);
                dataValueMaster.Add("@StaffNIK", modelMasterApp.StaffNIK);
                dataValueMaster.Add("@GUID", modelMasterApp.GUID);
                dataValueMaster.Add("@JsonDataNew", modelMasterApp.JsonDataNew);
                dataValueMaster.Add("@JsonDataOld", modelMasterApp.JsonDataOld);
                dataValueMaster.Add("@SubSystem", modelMasterApp.SubSystem);

                foreach (ApprovalDetailModel item in modelDetailApp)
                {
                    dataValue = new Dictionary<string, string>();
                    dataValue.Add("@TableName", item.TableName);
                    dataValue.Add("@ColumnName", item.ColumnName);
                    dataValue.Add("@Sequence", item.Sequence.ToString());
                    dataValue.Add("@Descripsi", item.Deskripsi);
                    dataValue.Add("@OldValue", item.OldValue);
                    dataValue.Add("@NewValue", item.NewValue);
                    dataValue.Add("@OldDescription", item.OldDescription);
                    dataValue.Add("@NewDescription", item.NewDescription);
                    dataValue.Add("@WSValue", item.WSValue);
                    listOfDataValue.Add(dataValue);
                }

                batchInsert.Replace("@Branch", "'" + dataValueMaster["@Branch"].Replace("'", "''") + "'");
                batchInsert.Replace("@CIFNumber", "'" + dataValueMaster["@CIFNumber"].Replace("'", "''") + "'");
                batchInsert.Replace("@CIFName", "'" + dataValueMaster["@CIFName"].Replace("'", "''") + "'");
                batchInsert.Replace("@TableName", "'" + dataValueMaster["@TableName"].Replace("'", "''") + "'");
                batchInsert.Replace("@Status", "'" + dataValueMaster["@Status"].Replace("'", "''") + "'");
                batchInsert.Replace("@ChangeType", "'" + dataValueMaster["@ChangeType"].Replace("'", "''") + "'");
                batchInsert.Replace("@StaffNIK", "'" + dataValueMaster["@StaffNIK"].Replace("'", "''") + "'");
                batchInsert.Replace("@GUID", "'" + dataValueMaster["@GUID"].Replace("'", "''") + "'");
                batchInsert.Replace("@JsonDataNew", "'" + dataValueMaster["@JsonDataNew"].Replace("'", "''") + "'");
                batchInsert.Replace("@JsonDataOld", "'" + dataValueMaster["@JsonDataOld"].Replace("'", "''") + "'");
                batchInsert.Replace("@SubSystem", "'" + dataValueMaster["@SubSystem"].Replace("'", "''") + "'");

                foreach (Dictionary<string, string> data in listOfDataValue)
                {
                    batchInsert.AppendLine("INSERT INTO [dbo].[DataChangeDetail_TM] ([ChangeId],[TableName],[ColumnName],[Sequence],[Deskripsi],[OldValue],[NewValue],[OldDescription],[NewDescription],[WSValue])");
                    batchInsert.AppendLine("VALUES(@ChangeId, @TableName, @ColumnName, @Sequence, @Descripsi, @OldValue, @NewValue, @OldDescription, @NewDescription, @WSValue);");

                    batchInsert.Replace("@TableName", "'" + data["@TableName"].Replace("'", "''") + "'");
                    batchInsert.Replace("@ColumnName", "'" + data["@ColumnName"].Replace("'", "''") + "'");
                    batchInsert.Replace("@Sequence", "'" + data["@Sequence"].Replace("'", "''") + "'");
                    batchInsert.Replace("@Descripsi", "'" + data["@Descripsi"].Replace("'", "''") + "'");
                    batchInsert.Replace("@OldValue", "'" + data["@OldValue"].Replace("'", "''") + "'");
                    batchInsert.Replace("@NewValue", "'" + data["@NewValue"].Replace("'", "''") + "'");
                    batchInsert.Replace("@OldDescription", "'" + data["@OldDescription"].Replace("'", "''") + "'");
                    batchInsert.Replace("@NewDescription", "'" + data["@NewDescription"].Replace("'", "''") + "'");
                    batchInsert.Replace("@WSValue", "'" + data["@WSValue"].Replace("'", "''") + "'");
                }

                if (isAutoApprove)
                {
                    batchInsert.AppendLine("INSERT INTO [dbo].[DataChangeDetail_TH] ([ChangeId],[TableName],[ColumnName],[Sequence],[Deskripsi],[OldValue],[NewValue],[OldDescription],[NewDescription],[WSValue])");
                    batchInsert.AppendLine("SELECT [ChangeId],[TableName],[ColumnName],[Sequence],[Deskripsi],[OldValue],[NewValue],[OldDescription],[NewDescription],[WSValue] FROM [dbo].[DataChangeDetail_TM] where ChangeId=@ChangeId;");

                    batchInsert.AppendLine("INSERT INTO [dbo].[DataChangeMaster_TH] ([ChangeId],[GUID],[SubSystem],[Branch],[CIFNumber],[CIFName],[TableName],[Status],[ChangeType],[ChangeDate],[StaffNIK],[SpvNIK],[ApprovedDate],[HostDate],[Description],[CIFTranId],[JsonDataNew],[JsonDataOld])");
                    batchInsert.AppendLine("SELECT [ChangeId],[GUID],[SubSystem],[Branch],[CIFNumber],[CIFName],[TableName],'APPROVE',[ChangeType],[ChangeDate],[StaffNIK],[StaffNIK],getdate(),getdate(),'DATA AUTO APPROVE VIA API CIF',[CIFTranId],[JsonDataNew],[JsonDataOld] FROM [dbo].[DataChangeMaster_TM] where ChangeId=@ChangeId;");

                    batchInsert.AppendLine("DELETE FROM [dbo].[DataChangeMaster_TM] where ChangeId=@ChangeId;");
                    batchInsert.AppendLine("DELETE FROM [dbo].[DataChangeDetail_TM] where ChangeId=@ChangeId;");
                }

                batchInsert.AppendLine("COMMIT TRAN; ");
                batchInsert.AppendLine("END TRY ");
                batchInsert.AppendLine("BEGIN CATCH ");
                batchInsert.AppendLine("IF @@TRANCOUNT > 0");
                batchInsert.AppendLine("BEGIN");
                batchInsert.AppendLine("ROLLBACK TRAN; ");
                batchInsert.AppendLine("END; ");
                batchInsert.AppendLine("declare @errorMsg varchar(500);");
                batchInsert.AppendLine("set @errorMsg = ERROR_MESSAGE();");
                batchInsert.AppendLine("raiserror (@errorMsg, 16, 1); ");
                batchInsert.AppendLine("END CATCH ");


                string sqlCommand = "";
                string strError = "";
                DataSet dsOut = new DataSet();
                sqlCommand = batchInsert.ToString();

                using (clsDBServiceSQLServer _clsDBServiceSQLServer = new clsDBServiceSQLServer())
                {
                    msgResponse.IsSuccess = _clsDBServiceSQLServer.QueryCommand(_strCIFParameterConnectionString_CFMAST, sqlCommand, _nTimeOut, out dsOut, out strError);
                    msgResponse.Data = null;
                    msgResponse.Description = strError;

                    if (strError.Length > 0)
                    {
                        doLogging(API_LOGENVELOPModel.ERROR, "Error Exception : " + GetMethodName(), "", msgResponse, null);
                        msgResponse.Description = API_LOGENVELOPModel.CATCH_DESC_EXCEPTION;
                    }
                }

                if (msgResponse.IsSuccess && !isAutoApprove)
                {
                    //create class kafka
                    KAFKAModel requestModel = new KAFKAModel();
                    requestModel.DateIssued = DateTime.Now.Date.ToString("yyyy-MM-dd HH:mm:ss");
                    requestModel.User = modelMasterApp.StaffNIK;
                    requestModel.Subsystem = modelMasterApp.SubSystem;
                    requestModel.Zone = "A";
                    requestModel.TrxGUID = modelMasterApp.GUID;
                    requestModel.TrxName = "Perubahan Data Pribadi Nasabah";
                    requestModel.TrxType = "0";
                    requestModel.UserMessage = "";
                    modelMasterApp.ChangeId = dsOut.Tables[0].Rows[0][0].ToString();
                    requestModel.JSONData.masterModels.Add(modelMasterApp);

                    foreach(ApprovalDetailModel item in modelDetailApp)
                    {
                        item.ChangeId = long.Parse(dsOut.Tables[0].Rows[0][0].ToString());
                        requestModel.JSONData.detailModels.Add(item);
                    }

                    string jsonKAFKA = JsonConvert.SerializeObject(requestModel);

                    local.APIMessage<bool> msgResult = new local.APIMessage<bool>();

                    msgResult = this.PUTMessageToRequestKAFKA(jsonKAFKA);

                    if (msgResult.IsSuccess)
                    {
                        msgResponse.IsSuccess = true;
                        msgResponse.Data = null;
                        msgResponse.Description = "";
                    }
                    else
                    {
                        //kalo gagal delete changed Id nya
                    }
                }
            }
            catch (Exception ex)
            {
                msgResponse.IsSuccess = false;
                msgResponse.Data = null;
                //msgResponse.Description = ex.Message;
                msgResponse.Description = API_LOGENVELOPModel.CATCH_DESC_EXCEPTION;
                doLogging(API_LOGENVELOPModel.ERROR, "Catch Exception : " + GetMethodName(), "", null, ex);

            }

            return msgResponse;
        }

        public local.APIMessage<ApprovalMasterModel> SubGetDataChangeMasterTM(string paramChangeId)
        {
            doLogging(API_LOGENVELOPModel.INFO, "Executing : " + GetMethodName(), "", paramChangeId);

            local.APIMessage<ApprovalMasterModel> msgResponse = new local.APIMessage<ApprovalMasterModel>();
            try
            {
                DataSet dsOut = new DataSet();
                string strError = "";

                string sqlCommand = "SELECT isnull([ChangeId],0) as [ChangeId],isnull([GUID],'') as [GUID],isnull([SubSystem],'') as [SubSystem],isnull([Branch],'') as [Branch],isnull([CIFNumber],0) as [CIFNumber],isnull([CIFName],'') as [CIFName],isnull([TableName],'') as [TableName],isnull([Status],'') as [Status],isnull([ChangeType],'') as [ChangeType],isnull([ChangeDate],'') as [ChangeDate],isnull([StaffNIK],'') as [StaffNIK],isnull([SpvNIK],'') as [SpvNIK],isnull([ApprovedDate],'') as [ApprovedDate],isnull([HostDate],'') as [HostDate],isnull([Description],'') as [Description],isnull([CIFTranId],'') as [CIFTranId],isnull([JsonDataNew],'') as [JsonDataNew],isnull([JsonDataOld],'') as [JsonDataOld]  FROM [dbo].[DataChangeMaster_TM] WHERE ChangeId=@paramValue";
                SqlParameter[] dbParam = new SqlParameter[1];

                dbParam[0] = new SqlParameter("@paramValue", SqlDbType.VarChar);
                dbParam[0].Value = paramChangeId;
                dbParam[0].Direction = System.Data.ParameterDirection.Input;

                using (clsDBServiceSQLServer _clsDBServiceSQLServer = new clsDBServiceSQLServer())
                {
                    msgResponse.IsSuccess = _clsDBServiceSQLServer.QueryCommand(_strCIFParameterConnectionString_CFMAST, sqlCommand, ref dbParam, _nTimeOut, out dsOut, out strError);
                }

                if (msgResponse.IsSuccess && dsOut != null && dsOut.Tables.Count >0 && dsOut.Tables[0].Rows.Count > 0)
                {
                    msgResponse.Data = new ApprovalMasterModel();
                    msgResponse.Data.ApprovedDate = dsOut.Tables[0].Rows[0]["ApprovedDate"].ToString();
                    msgResponse.Data.Branch = dsOut.Tables[0].Rows[0]["Branch"].ToString();
                    msgResponse.Data.ChangeId = dsOut.Tables[0].Rows[0]["ChangeId"].ToString();
                    msgResponse.Data.ChangeType = dsOut.Tables[0].Rows[0]["ChangeType"].ToString();
                    msgResponse.Data.CIFName = dsOut.Tables[0].Rows[0]["CIFName"].ToString();
                    msgResponse.Data.CIFNumber = dsOut.Tables[0].Rows[0]["CIFNumber"].ToString();
                    msgResponse.Data.Description = dsOut.Tables[0].Rows[0]["Description"].ToString();
                    msgResponse.Data.GUID = dsOut.Tables[0].Rows[0]["GUID"].ToString();
                    msgResponse.Data.HostDate = dsOut.Tables[0].Rows[0]["HostDate"].ToString();
                    msgResponse.Data.JsonDataNew = dsOut.Tables[0].Rows[0]["JsonDataNew"].ToString();
                    msgResponse.Data.JsonDataOld = dsOut.Tables[0].Rows[0]["JsonDataOld"].ToString();
                    msgResponse.Data.SpvNIK = dsOut.Tables[0].Rows[0]["SpvNIK"].ToString();
                    msgResponse.Data.StaffNIK = dsOut.Tables[0].Rows[0]["StaffNIK"].ToString();
                    msgResponse.Data.Status = dsOut.Tables[0].Rows[0]["Status"].ToString();
                    msgResponse.Data.SubSystem = dsOut.Tables[0].Rows[0]["SubSystem"].ToString();
                    msgResponse.Data.TableName = dsOut.Tables[0].Rows[0]["TableName"].ToString();
                    msgResponse.Description = "";
                }
                else
                {
                    msgResponse.IsSuccess = false;
                    msgResponse.Data = null;
                    msgResponse.Description = strError;
                    if (strError.Length > 0)
                    {
                        doLogging(API_LOGENVELOPModel.ERROR, "Error Exception : " + GetMethodName(), "", msgResponse, null);
                        msgResponse.Description = API_LOGENVELOPModel.CATCH_DESC_EXCEPTION;
                    }
                }
            }
            catch (Exception ex)
            {
                msgResponse.IsSuccess = false;
                msgResponse.Data = null;
                //msgResponse.Description = ex.Message;
                msgResponse.Description = API_LOGENVELOPModel.CATCH_DESC_EXCEPTION;
                doLogging(API_LOGENVELOPModel.ERROR, "Catch Exception : " + GetMethodName(), "", null, ex);

            }

            return msgResponse;
        }

        public local.APIMessage<ApprovalMasterModel> SubUpdateDataChangeMasterTM(ApprovalMasterModel inModel)
        {
            doLogging(API_LOGENVELOPModel.INFO, "Executing : " + GetMethodName(), "", null, null, inModel.GUID, inModel.StaffNIK, "", inModel.Branch);

            local.APIMessage<ApprovalMasterModel> msgResponse = new local.APIMessage<ApprovalMasterModel>();
            try
            {
                DataSet dsOut = new DataSet();
                string strError = "";

                string sqlCommand = "UPDATE [dbo].[DataChangeMaster_TM] SET [SpvNIK] = @SpvNIK,[ApprovedDate] =@ApproveDate,HostDate = case when @HostDate = '-' then null else @HostDate end,[Status] = @Status,[Description] = @Description";
                sqlCommand = sqlCommand + " " + "WHERE [ChangeId] = @ChangeId";

                SqlParameter[] dbParam = new SqlParameter[6];
                dbParam[0] = new SqlParameter("@ChangeId", SqlDbType.VarChar);
                dbParam[0].Value = inModel.ChangeId;
                dbParam[0].Direction = System.Data.ParameterDirection.Input;

                dbParam[1] = new SqlParameter("@SpvNIK", SqlDbType.VarChar);
                dbParam[1].Value = inModel.SpvNIK;
                dbParam[1].Direction = System.Data.ParameterDirection.Input;

                dbParam[2] = new SqlParameter("@Status", SqlDbType.VarChar);
                dbParam[2].Value = inModel.Status;
                dbParam[2].Direction = System.Data.ParameterDirection.Input;

                dbParam[3] = new SqlParameter("@ApproveDate", SqlDbType.VarChar);
                dbParam[3].Value = inModel.ApprovedDate;
                dbParam[3].Direction = System.Data.ParameterDirection.Input;

                dbParam[4] = new SqlParameter("@HostDate", SqlDbType.VarChar);
                dbParam[4].Value = inModel.HostDate;
                dbParam[4].Direction = System.Data.ParameterDirection.Input;

                dbParam[5] = new SqlParameter("@Description", SqlDbType.VarChar);
                dbParam[5].Value = inModel.Description;
                dbParam[5].Direction = System.Data.ParameterDirection.Input;

                using (clsDBServiceSQLServer _clsDBServiceSQLServer = new clsDBServiceSQLServer())
                {
                    msgResponse.IsSuccess = _clsDBServiceSQLServer.QueryCommand(_strCIFParameterConnectionString_CFMAST, sqlCommand, ref dbParam, _nTimeOut, out strError);
                    msgResponse.Data = inModel;
                    msgResponse.Description = strError;


                    if (strError.Length > 0)
                    {
                        doLogging(API_LOGENVELOPModel.ERROR, "Error Exception : " + GetMethodName(), "", msgResponse, null);
                        msgResponse.Description = API_LOGENVELOPModel.CATCH_DESC_EXCEPTION;
                    }
                }
            }
            catch (Exception ex)
            {
                msgResponse.IsSuccess = false;
                msgResponse.Data = inModel;
                //msgResponse.Description = ex.Message;
                msgResponse.Description = API_LOGENVELOPModel.CATCH_DESC_EXCEPTION;
                doLogging(API_LOGENVELOPModel.ERROR, "Catch Exception : " + GetMethodName(), "", null, ex);

            }

            return msgResponse;
        }
        #endregion

        #region KAFKA
        public local.APIMessage<bool> PUTMessageToRequestKAFKA(string Message)
        {
            doLogging(API_LOGENVELOPModel.INFO, "Executing : " + GetMethodName(), "", Message);

            string strError = "";
            bool bResult = false;
            local.APIMessage<bool> result = new local.APIMessage<bool>();
            ProducerConfig config = new ProducerConfig { BootstrapServers = _strKAFKA_Server };

            using (var producer = new ProducerBuilder<Null, string>(config).Build())
            {
                try
                {
                    var dr = producer.ProduceAsync(_strKAFKA_Request_Topic, new Message<Null, string> { Value = Message }).Result;                    
                    bResult = true;
                }
                catch (Exception ex)
                {
                    //strError = ex.Message + " : " + ex.StackTrace;
                    strError = API_LOGENVELOPModel.CATCH_DESC_EXCEPTION;
                    doLogging(API_LOGENVELOPModel.ERROR, "Catch Exception : " + GetMethodName(), "", null, ex);

                }
            }

            result.IsSuccess = bResult;
            result.Description = strError;

            doLogging(API_LOGENVELOPModel.INFO, "End Executing : " + GetMethodName(), "", result);

            return result;
        }

        public local.APIMessage<bool> PUTMessageToProcessKAFKA(string Message)
        {
            doLogging(API_LOGENVELOPModel.INFO, "Executing : " + GetMethodName(), "", Message);

            string strError = "";
            bool bResult = false;
            local.APIMessage<bool> result = new local.APIMessage<bool>();
            ProducerConfig config = new ProducerConfig { BootstrapServers = _strKAFKA_Server };

            using (var producer = new ProducerBuilder<Null, string>(config).Build())
            {
                try
                {
                    var dr = producer.ProduceAsync(_strKAFKA_Process_Topic, new Message<Null, string> { Value = Message }).Result;
                    bResult = true;
                }
                catch (Exception ex)
                {
                    strError = API_LOGENVELOPModel.CATCH_DESC_EXCEPTION;
                    doLogging(API_LOGENVELOPModel.ERROR, "Catch Exception : " + GetMethodName(), "", null, ex);
                }
            }

            result.IsSuccess = bResult;
            result.Description = strError;
            doLogging(API_LOGENVELOPModel.INFO, "End Executing : " + GetMethodName(), "", result);

            return result;
        }
        #endregion
        
        #region Override
        public local.APIMessage<DataSet> SubCheckOverride(string paramGUID)
        {
            doLogging(API_LOGENVELOPModel.INFO, "Executing : " + GetMethodName(), "", paramGUID, null);


            local.APIMessage<DataSet> msgResponse = new local.APIMessage<DataSet>();
            try
            {
                DataSet dsOut = new DataSet();
                string strError = "";

                string sqlCommand = "SELECT * FROM [OverrideConfirmation_TM] WHERE [GUID]=@GUID";
                SqlParameter[] dbParam = new SqlParameter[1];
                dbParam[0] = new SqlParameter("@GUID", SqlDbType.VarChar);
                dbParam[0].Value = paramGUID;
                dbParam[0].Direction = System.Data.ParameterDirection.Input;

                using (clsDBServiceSQLServer _clsDBServiceSQLServer = new clsDBServiceSQLServer())
                {
                    msgResponse.IsSuccess = _clsDBServiceSQLServer.QueryCommand(_strCIFParameterConnectionString_CFMAST, sqlCommand, ref dbParam, _nTimeOut, out dsOut, out strError);
                    msgResponse.Data = dsOut;
                    msgResponse.Description = strError;

                    if (strError.Length > 0)
                    {
                        doLogging(API_LOGENVELOPModel.ERROR, "Error Exception : " + GetMethodName(), "", msgResponse, null);
                        msgResponse.Description = API_LOGENVELOPModel.CATCH_DESC_EXCEPTION;
                    }
                }
            }
            catch (Exception ex)
            {
                msgResponse.IsSuccess = false;
                msgResponse.Data = new DataSet();
                //msgResponse.Description = ex.Message;
                msgResponse.Description = API_LOGENVELOPModel.CATCH_DESC_EXCEPTION;
                doLogging(API_LOGENVELOPModel.ERROR, "Catch Exception : " + GetMethodName(), "", null, ex);
            }

            return msgResponse;
        }
        public local.APIMessage<object> SubSaveOverride(string paramGUID, string paramNIK, object paramObj)
        {
            doLogging(API_LOGENVELOPModel.INFO, "Executing : " + GetMethodName(), "", paramGUID, null);

            local.APIMessage<object> msgResponse = new local.APIMessage<object>();
            try
            {
                DataSet dsOut = new DataSet();
                string strError = "";

                string sqlCommand = "INSERT INTO [dbo].[OverrideConfirmation_TM] ([GUID],[RequestDate],[UserNIK],[JsonData],[Status])";
                sqlCommand = sqlCommand + " " + "VALUES (@Guid,getdate(),@UserNIK,@JsonData,'PENDING')";

                SqlParameter[] dbParam = new SqlParameter[3];
                dbParam[0] = new SqlParameter("@Guid", SqlDbType.VarChar);
                dbParam[0].Value = paramGUID;
                dbParam[0].Direction = System.Data.ParameterDirection.Input;

                dbParam[1] = new SqlParameter("@UserNIK", SqlDbType.VarChar);
                dbParam[1].Value = paramNIK;
                dbParam[1].Direction = System.Data.ParameterDirection.Input;

                string jsonData = JsonConvert.SerializeObject(paramObj);

                dbParam[2] = new SqlParameter("@JsonData", SqlDbType.VarChar);
                dbParam[2].Value = jsonData;
                dbParam[2].Direction = System.Data.ParameterDirection.Input;

                using (clsDBServiceSQLServer _clsDBServiceSQLServer = new clsDBServiceSQLServer())
                {
                    msgResponse.IsSuccess = _clsDBServiceSQLServer.QueryCommand(_strCIFParameterConnectionString_CFMAST, sqlCommand, ref dbParam, _nTimeOut, out dsOut, out strError);
                    msgResponse.Data = paramObj;
                    msgResponse.Description = strError;


                    if (strError.Length > 0)
                    {
                        doLogging(API_LOGENVELOPModel.ERROR, "Error Exception : " + GetMethodName(), "", msgResponse, null);
                        msgResponse.Description = API_LOGENVELOPModel.CATCH_DESC_EXCEPTION;
                    }
                }
            }
            catch (Exception ex)
            {
                msgResponse.IsSuccess = false;
                msgResponse.Data = paramObj;
                //msgResponse.Description = ex.Message;

                msgResponse.Description = API_LOGENVELOPModel.CATCH_DESC_EXCEPTION;
                doLogging(API_LOGENVELOPModel.ERROR, "Catch Exception : " + GetMethodName(), "", null, ex);
            }

            return msgResponse;
        }
        #endregion

        #region SQL_CIF
        private local.APIMessage<GeneralCIFModel> ProcessToProCIFChangeMaster(GeneralCIFModel createCIFModel, GeneralCIFModel modelOld, bool isAutoApprove = false)
        {
            doLogging(API_LOGENVELOPModel.INFO, "Executing : " + GetMethodName(), "", null, null, createCIFModel.GUID, createCIFModel.NIK, createCIFModel.Module, createCIFModel.Branch);

            local.APIMessage<GeneralCIFModel> msgResponse = new local.APIMessage<GeneralCIFModel>();
            StringBuilder strRqDetail = new StringBuilder();//pembentukan RqDetail + nanti assign value dari client
            clsAPIService _service = this;

            try
            {
                #region Compare

                if (modelOld == null)
                {
                    throw new Exception("Inquiry Detail Model Data Old not found!");
                }

                local.APIMessage<ApprovalMasterModel> masterModel = new local.APIMessage<ApprovalMasterModel>();
                List<ApprovalDetailModel> listApprovalDetailModel = new List<ApprovalDetailModel>();

                masterModel = this.ProcessCompareOldAndNewModel(createCIFModel, modelOld, ref listApprovalDetailModel);

                #endregion

                if (masterModel.IsSuccess && masterModel.Data != null && listApprovalDetailModel != null && listApprovalDetailModel.Count > 0)
                {
                    local.APIMessageDetail<GeneralCIFModel> msgResponseChangeMasterProCIF = new local.APIMessageDetail<GeneralCIFModel>();
                    msgResponseChangeMasterProCIF = _service.SaveToProCIFChangeMaster(masterModel.Data, listApprovalDetailModel);

                    if (msgResponseChangeMasterProCIF.IsSuccess)
                    {
                        msgResponseChangeMasterProCIF.IsSuccess = true;
                        msgResponseChangeMasterProCIF.IsNeedApproval = false;
                        msgResponseChangeMasterProCIF.Description = "Data CIF Maintenance ProCIF AUTO Approval Supervisor!" + "(" + createCIFModel.Module + ")";


                        //25102019, Miftahul, begin
                        this.SubUpdateProCIF(createCIFModel);
                        //25102019, Miftahul, end

                        this.ProcessSyncWithREKSA(createCIFModel);
                    }

                    if (isAutoApprove)
                    {
                        local.APIMessageDetail<GeneralCIFModel> msgResponseChangeMasterAPICIF = new local.APIMessageDetail<GeneralCIFModel>();
                        msgResponseChangeMasterAPICIF = _service.SubSaveApprovalMaintenanceCIF(masterModel.Data, listApprovalDetailModel, isAutoApprove);

                        if (msgResponseChangeMasterAPICIF.IsSuccess)
                        {
                            msgResponseChangeMasterAPICIF.IsSuccess = true;
                            msgResponseChangeMasterAPICIF.IsNeedApproval = false;
                            msgResponseChangeMasterAPICIF.Description = "Data CIF Maintenance API CIF AUTO Approval Supervisor!" + "(" + createCIFModel.Module + ")";
                        }
                    }                    
                }            
            }
            catch (Exception ex)
            {
                msgResponse.Data = null;
                msgResponse.IsSuccess = false;
                //msgResponse.Description = ex.Message + Environment.NewLine + ex.StackTrace.ToString();

                msgResponse.Description = API_LOGENVELOPModel.CATCH_DESC_EXCEPTION;
                doLogging(API_LOGENVELOPModel.ERROR, "Catch Exception : " + GetMethodName(), "", null, ex);
            }
            return msgResponse;
        }

        private local.APIMessageDetail<GeneralCIFModel> SaveToProCIFChangeMaster(ApprovalMasterModel modelMasterApp, List<ApprovalDetailModel> modelDetailApp)
        {
            doLogging(API_LOGENVELOPModel.INFO, "Executing : " + GetMethodName(), "", null, null, modelMasterApp.GUID, modelMasterApp.StaffNIK, "", modelMasterApp.Branch);

            local.APIMessageDetail<GeneralCIFModel> msgResponse = new local.APIMessageDetail<GeneralCIFModel>();
            try
            {
                StringBuilder batchInsert = new StringBuilder();
                batchInsert.AppendLine("BEGIN TRY");
                batchInsert.AppendLine("BEGIN TRANSACTION;");
                batchInsert.AppendLine("DECLARE @ChangeId bigint;");
                batchInsert.AppendLine("INSERT INTO [dbo].[ProCIFChangeMaster_TM] " +
                    "([Branch],[CIFNumber],[CIFName],[TableName],[Status],[ChangeType],[ProcessId],[ChangeDate],[TellerId],[CheckerSuid],[ApprovedDate],[HostDate],[Description],[CIFTranId])");
                batchInsert.AppendLine("VALUES (@Branch, @CIFNumber ,@CIFName ,@TableName ,@Status ,@ChangeType ,NULL, GETDATE(), @StaffNIK, @StaffNIK, GETDATE(), GETDATE(),'', NULL);");
                batchInsert.AppendLine("SET @ChangeId = @@identity; SELECT @ChangeId;");

                List<Dictionary<string, string>> listOfDataValue = new List<Dictionary<string, string>>();
                Dictionary<string, string> dataValueMaster = new Dictionary<string, string>();
                Dictionary<string, string> dataValue = new Dictionary<string, string>();

                dataValueMaster.Add("@Branch", modelMasterApp.Branch);
                dataValueMaster.Add("@CIFNumber", modelMasterApp.CIFNumber);
                dataValueMaster.Add("@CIFName", modelMasterApp.CIFName);
                dataValueMaster.Add("@TableName", modelMasterApp.TableName);
                dataValueMaster.Add("@Status", "2"); //SUDAH DI APPROVE
                dataValueMaster.Add("@ChangeType", "U");
                dataValueMaster.Add("@StaffNIK", "77777"); //DISET DEFAULT DULU UNTUK BY PASS VALIDASI DI ProCIFOtorisasiMtncData
                dataValueMaster.Add("@GUID", modelMasterApp.GUID);
                dataValueMaster.Add("@JsonDataNew", modelMasterApp.JsonDataNew);
                dataValueMaster.Add("@JsonDataOld", modelMasterApp.JsonDataOld);
                dataValueMaster.Add("@SubSystem", modelMasterApp.SubSystem);

                foreach (ApprovalDetailModel item in modelDetailApp)
                {
                    dataValue = new Dictionary<string, string>();
                    dataValue.Add("@TableName", item.TableName);
                    dataValue.Add("@ColumnName", item.ColumnName);
                    dataValue.Add("@Sequence", item.Sequence.ToString());
                    dataValue.Add("@Descripsi", item.Deskripsi);
                    dataValue.Add("@OldValue", item.OldValue);
                    dataValue.Add("@NewValue", item.NewValue);
                    dataValue.Add("@OldDescription", item.OldDescription);
                    dataValue.Add("@NewDescription", item.NewDescription);
                    dataValue.Add("@WSValue", item.WSValue);
                    listOfDataValue.Add(dataValue);
                }

                batchInsert.Replace("@Branch", "'" + dataValueMaster["@Branch"].Replace("'", "''") + "'");
                batchInsert.Replace("@CIFNumber", "'" + dataValueMaster["@CIFNumber"].Replace("'", "''") + "'");
                batchInsert.Replace("@CIFName", "'" + dataValueMaster["@CIFName"].Replace("'", "''") + "'");
                batchInsert.Replace("@TableName", "'" + dataValueMaster["@TableName"].Replace("'", "''") + "'");
                batchInsert.Replace("@Status", "'" + dataValueMaster["@Status"].Replace("'", "''") + "'");
                batchInsert.Replace("@ChangeType", "'" + dataValueMaster["@ChangeType"].Replace("'", "''") + "'");
                batchInsert.Replace("@StaffNIK", "'" + dataValueMaster["@StaffNIK"].Replace("'", "''") + "'");
                batchInsert.Replace("@GUID", "'" + dataValueMaster["@GUID"].Replace("'", "''") + "'");
                batchInsert.Replace("@JsonDataNew", "'" + dataValueMaster["@JsonDataNew"].Replace("'", "''") + "'");
                batchInsert.Replace("@JsonDataOld", "'" + dataValueMaster["@JsonDataOld"].Replace("'", "''") + "'");
                batchInsert.Replace("@SubSystem", "'" + dataValueMaster["@SubSystem"].Replace("'", "''") + "'");

                foreach (Dictionary<string, string> data in listOfDataValue)
                {
                    batchInsert.AppendLine("INSERT INTO [dbo].[ProCIFChangeDetail_TM] " +
                        "([ChangeId],[TableName],[ColumnName],[Sequence],[Descripsi],[OldValue],[NewValue],[OldDescription],[NewDescription],[WSValue])");
                    batchInsert.AppendLine("VALUES(@ChangeId, @TableName, @ColumnName, @Sequence, @Descripsi, @OldValue, @NewValue, @OldDescription, @NewDescription, @WSValue);");

                    batchInsert.Replace("@TableName", "'" + data["@TableName"].Replace("'", "''") + "'");
                    batchInsert.Replace("@ColumnName", "'" + data["@ColumnName"].Replace("'", "''") + "'");
                    batchInsert.Replace("@Sequence", "'" + data["@Sequence"].Replace("'", "''") + "'");
                    batchInsert.Replace("@Descripsi", "'" + data["@Descripsi"].Replace("'", "''") + "'");
                    batchInsert.Replace("@OldValue", "'" + data["@OldValue"].Replace("'", "''") + "'");
                    batchInsert.Replace("@NewValue", "'" + data["@NewValue"].Replace("'", "''") + "'");
                    batchInsert.Replace("@OldDescription", "'" + data["@OldDescription"].Replace("'", "''") + "'");
                    batchInsert.Replace("@NewDescription", "'" + data["@NewDescription"].Replace("'", "''") + "'");
                    batchInsert.Replace("@WSValue", "'" + data["@WSValue"].Replace("'", "''") + "'");
                }

                batchInsert.AppendLine("COMMIT TRAN; ");
                batchInsert.AppendLine("END TRY ");
                batchInsert.AppendLine("BEGIN CATCH ");
                batchInsert.AppendLine("IF @@TRANCOUNT > 0");
                batchInsert.AppendLine("BEGIN");
                batchInsert.AppendLine("ROLLBACK TRAN; ");
                batchInsert.AppendLine("END; ");
                batchInsert.AppendLine("declare @errorMsg varchar(500);");
                batchInsert.AppendLine("set @errorMsg = ERROR_MESSAGE();");
                batchInsert.AppendLine("raiserror (@errorMsg, 16, 1); ");
                batchInsert.AppendLine("END CATCH ");
                
                string sqlCommand = "";
                string strError = "";
                DataSet dsOut = new DataSet();
                sqlCommand = batchInsert.ToString();

                using (clsDBServiceSQLServer _clsDBServiceSQLServer = new clsDBServiceSQLServer())
                {
                    msgResponse.IsSuccess = _clsDBServiceSQLServer.QueryCommand(_strCIFParameterConnectionString_SQL_CIF, sqlCommand, _nTimeOut, out dsOut, out strError);
                    msgResponse.Data = null;
                    msgResponse.Description = strError;

                    if (strError.Length > 0)
                    {
                        doLogging(API_LOGENVELOPModel.ERROR, "Error Exception : " + GetMethodName(), "", msgResponse, null);
                        msgResponse.Description = API_LOGENVELOPModel.CATCH_DESC_EXCEPTION;
                    }
                }

                if (msgResponse.IsSuccess)
                {
                    msgResponse.IsSuccess = true;
                    msgResponse.Data = null;
                    msgResponse.Description = "";
                }
                else
                {
                    msgResponse.IsSuccess = false;
                    msgResponse.Data = null;
                    msgResponse.Description = "";
                }
            }
            catch (Exception ex)
            {
                msgResponse.IsSuccess = false;
                msgResponse.Data = null;
                //msgResponse.Description = ex.Message;

                msgResponse.Description = API_LOGENVELOPModel.CATCH_DESC_EXCEPTION;
                doLogging(API_LOGENVELOPModel.ERROR, "Catch Exception : " + GetMethodName(), "", null, ex);
            }

            return msgResponse;
        }
        
        public local.APIMessageDetail<GeneralCIFModel> SubSaveProCIFMaster_TM(GeneralCIFModel generalCIFModel)
        {
            doLogging(API_LOGENVELOPModel.INFO, "Executing : " + GetMethodName(), "", null, null, generalCIFModel.GUID, generalCIFModel.NIK, generalCIFModel.Module, generalCIFModel.Branch);

            local.APIMessageDetail<GeneralCIFModel> msgResponse = new local.APIMessageDetail<GeneralCIFModel>();
            try
            {
                StringBuilder batchInsert = new StringBuilder();
                batchInsert.AppendLine("BEGIN TRY");
                batchInsert.AppendLine("BEGIN TRANSACTION;");
                batchInsert.AppendLine("declare @nUserClassId int;");
                batchInsert.AppendLine("select @nUserClassId = classification_id from user_table_classification_v where nik = @pcTellerId and module = 'Pro CIF';");
                batchInsert.AppendLine("INSERT INTO ProCIFMaster_TM");
                batchInsert.AppendLine("([CFCIF#],[CFBRNN],[Status],[UserSuid],[InputDate], [ClassId],[BatchID],[DocumentID], CreateByBranch, NIKRM)");
                batchInsert.AppendLine("select @pcCIF, @pcTransactionBranch, 0, @pcTellerId, @dTransactionDate ,@nUserClassId,@pcBatchID,@pcDocumentID ,@pcCreateByBranch ,@pnNIKRM ");
                batchInsert.AppendLine("COMMIT TRAN; ");
                batchInsert.AppendLine("END TRY ");
                batchInsert.AppendLine("BEGIN CATCH ");
                batchInsert.AppendLine("IF @@TRANCOUNT > 0");
                batchInsert.AppendLine("BEGIN");
                batchInsert.AppendLine("ROLLBACK TRAN; ");
                batchInsert.AppendLine("END; ");
                batchInsert.AppendLine("declare @errorMsg varchar(500);");
                batchInsert.AppendLine("set @errorMsg = ERROR_MESSAGE();");
                batchInsert.AppendLine("raiserror (@errorMsg, 16, 1); ");
                batchInsert.AppendLine("END CATCH ");

                SqlParameter[] dbParam = new SqlParameter[8];

                dbParam[0] = new SqlParameter("@pcCIF", SqlDbType.VarChar);
                dbParam[0].Value = Convert.ToInt64(generalCIFModel.CustomerNumber);
                dbParam[0].Direction = System.Data.ParameterDirection.Input;

                dbParam[1] = new SqlParameter("@pcTransactionBranch", SqlDbType.VarChar);
                dbParam[1].Value = generalCIFModel.BranchNumber;
                dbParam[1].Direction = System.Data.ParameterDirection.Input;

                dbParam[2] = new SqlParameter("@pcTellerId", SqlDbType.VarChar);
                dbParam[2].Value = generalCIFModel.NIK;
                dbParam[2].Direction = System.Data.ParameterDirection.Input;

                dbParam[3] = new SqlParameter("@dTransactionDate", SqlDbType.VarChar);
                dbParam[3].Value = generalCIFModel.TransactionDate.ToString("yyyy/MM/dd");
                dbParam[3].Direction = System.Data.ParameterDirection.Input;

                dbParam[4] = new SqlParameter("@pcBatchID", SqlDbType.VarChar);
                dbParam[4].Value = "";
                dbParam[4].Direction = System.Data.ParameterDirection.Input;

                dbParam[5] = new SqlParameter("@pcDocumentID", SqlDbType.VarChar);
                dbParam[5].Value = "";
                dbParam[5].Direction = System.Data.ParameterDirection.Input;

                dbParam[6] = new SqlParameter("@pcCreateByBranch", SqlDbType.VarChar);
                dbParam[6].Value = generalCIFModel.BranchNumber;
                dbParam[6].Direction = System.Data.ParameterDirection.Input;

                dbParam[7] = new SqlParameter("@pnNIKRM", SqlDbType.VarChar);
                dbParam[7].Value = generalCIFModel.NIKRM;
                dbParam[7].Direction = System.Data.ParameterDirection.Input;

                string sqlCommand = "";
                string strError = "";
                DataSet dsOut = new DataSet();
                sqlCommand = batchInsert.ToString();

                using (clsDBServiceSQLServer _clsDBServiceSQLServer = new clsDBServiceSQLServer())
                {
                    msgResponse.IsSuccess = _clsDBServiceSQLServer.QueryCommand(_strCIFParameterConnectionString_SQL_CIF, sqlCommand,ref dbParam, _nTimeOut, out dsOut, out strError);
                    msgResponse.Data = null;
                    msgResponse.Description = strError;

                    if (strError.Length > 0)
                    {
                        doLogging(API_LOGENVELOPModel.ERROR, "Error Exception : " + GetMethodName(), "", msgResponse, null);
                        msgResponse.Description = API_LOGENVELOPModel.CATCH_DESC_EXCEPTION;
                    }
                }
            }
            catch (Exception ex)
            {
                msgResponse.IsSuccess = false;
                msgResponse.Data = null;
                //msgResponse.Description = ex.Message;
                msgResponse.Description = API_LOGENVELOPModel.CATCH_DESC_EXCEPTION;
                doLogging(API_LOGENVELOPModel.ERROR, "Catch Exception : " + GetMethodName(), "", null, ex);

            }

            return msgResponse;
        }

        public local.APIMessage<DataSet> InsertCRSFlag(string CifNo, string CrsFlag, string CrsDate)
        {
            doLogging(API_LOGENVELOPModel.INFO, "Executing : " + GetMethodName(), "", CifNo + "|" + CrsFlag + "|" + CrsDate);

            local.APIMessage<DataSet> msgResponse = new local.APIMessage<DataSet>();
            DataSet dsOut = new DataSet();
            string strError = "";

            StringBuilder batchInsert = new StringBuilder();
            batchInsert.AppendLine("BEGIN TRY");
            batchInsert.AppendLine("BEGIN TRANSACTION;");
            batchInsert.AppendLine("if isnull(@CRSFlag, '') != ''");
            batchInsert.AppendLine("begin");
            batchInsert.AppendLine("declare @pnCRSDate varchar(10)");
            batchInsert.AppendLine("set @pnCRSDate = convert(varchar, getdate(), 112)");
            batchInsert.AppendLine("if exists (select top 1 1 from CIFCRS_TM where CIFNo = @CifNo)");
            batchInsert.AppendLine("begin");
            batchInsert.AppendLine("update CIFCRS_TM set CRS = @CRSFlag, DateCRS = @pnCRSDate where CIFNo = @CifNo");
            batchInsert.AppendLine("end");
            batchInsert.AppendLine("else");
            batchInsert.AppendLine("begin");
            batchInsert.AppendLine("insert into CIFCRS_TM (CIFNo, CRS, DateCRS)");
            batchInsert.AppendLine("select @CifNo, @CRSFlag, @pnCRSDate");
            batchInsert.AppendLine("end");
            batchInsert.AppendLine("end");
            batchInsert.AppendLine("COMMIT TRAN; ");
            batchInsert.AppendLine("END TRY ");
            batchInsert.AppendLine("BEGIN CATCH ");
            batchInsert.AppendLine("IF @@TRANCOUNT > 0");
            batchInsert.AppendLine("BEGIN");
            batchInsert.AppendLine("ROLLBACK TRAN; ");
            batchInsert.AppendLine("END; ");
            batchInsert.AppendLine("declare @errorMsg varchar(500);");
            batchInsert.AppendLine("set @errorMsg = ERROR_MESSAGE();");
            batchInsert.AppendLine("raiserror (@errorMsg, 16, 1); ");
            batchInsert.AppendLine("END CATCH ");

            SqlParameter[] dbParam = new SqlParameter[2];
            dbParam[0] = new SqlParameter("@CifNo", SqlDbType.VarChar);
            dbParam[0].Value = CifNo;
            dbParam[0].Direction = System.Data.ParameterDirection.Input;

            dbParam[1] = new SqlParameter("@CRSFlag", SqlDbType.VarChar);
            dbParam[1].Value = CrsFlag;
            dbParam[1].Direction = System.Data.ParameterDirection.Input;

            using (clsDBServiceSQLServer _clsDBServiceSQLServer = new clsDBServiceSQLServer())
            {
                msgResponse.IsSuccess = _clsDBServiceSQLServer.QueryCommand(_strCIFParameterConnectionString_SQL_CIF, batchInsert.ToString(), ref dbParam, _nTimeOut, out dsOut, out strError);
                msgResponse.Data = dsOut;
                msgResponse.Description = strError;

                if (strError.Length > 0)
                {
                    doLogging(API_LOGENVELOPModel.ERROR, "Error Exception : " + GetMethodName(), "", msgResponse, null);
                    msgResponse.Description = API_LOGENVELOPModel.CATCH_DESC_EXCEPTION;
                }
            }

            return msgResponse;
        }

        public local.APIMessage<DataSet> InsertProCIFRiskProfile_TM(string CifNo)
        {
            doLogging(API_LOGENVELOPModel.INFO, "Executing : " + GetMethodName(), "", CifNo);

            local.APIMessage<DataSet> msgResponse = new local.APIMessage<DataSet>();
            DataSet dsOut = new DataSet();
            string strError = "";

            StringBuilder batchInsert = new StringBuilder();
            batchInsert.AppendLine("BEGIN TRY");
            batchInsert.AppendLine("BEGIN TRANSACTION;");
            batchInsert.AppendLine("begin");
            batchInsert.AppendLine("if exists (select top 1 1 from dbo.ProCIFRiskProfile_TM where CIFNumber = @cCIF)");
            batchInsert.AppendLine("begin");
            batchInsert.AppendLine("update dbo.ProCIFRiskProfile_TM set LastUpdated = getdate(), ExpiredDate = dateadd(year,3,getdate()) where CIFNumber = @cCIF");
            batchInsert.AppendLine("end");
            batchInsert.AppendLine("else");
            batchInsert.AppendLine("begin");
            batchInsert.AppendLine("insert into dbo.ProCIFRiskProfile_TM(CIFNumber,LastUpdated,ExpiredDate)");
            batchInsert.AppendLine("select @cCIF, getdate(), dateadd(year,3,getdate())");
            batchInsert.AppendLine("end");
            batchInsert.AppendLine("end");
            batchInsert.AppendLine("COMMIT TRAN; ");
            batchInsert.AppendLine("END TRY ");
            batchInsert.AppendLine("BEGIN CATCH ");
            batchInsert.AppendLine("IF @@TRANCOUNT > 0");
            batchInsert.AppendLine("BEGIN");
            batchInsert.AppendLine("ROLLBACK TRAN; ");
            batchInsert.AppendLine("END; ");
            batchInsert.AppendLine("declare @errorMsg varchar(500);");
            batchInsert.AppendLine("set @errorMsg = ERROR_MESSAGE();");
            batchInsert.AppendLine("raiserror (@errorMsg, 16, 1); ");
            batchInsert.AppendLine("END CATCH ");

            SqlParameter[] dbParam = new SqlParameter[1];
            dbParam[0] = new SqlParameter("@cCIF", SqlDbType.VarChar);
            dbParam[0].Value = CifNo;
            dbParam[0].Direction = System.Data.ParameterDirection.Input;

            using (clsDBServiceSQLServer _clsDBServiceSQLServer = new clsDBServiceSQLServer())
            {
                msgResponse.IsSuccess = _clsDBServiceSQLServer.QueryCommand(_strCIFParameterConnectionString_SQL_CIF, batchInsert.ToString(), ref dbParam, _nTimeOut, out dsOut, out strError);
                msgResponse.Data = dsOut;
                msgResponse.Description = strError;

                if (strError.Length > 0)
                {
                    doLogging(API_LOGENVELOPModel.ERROR, "Error Exception : " + GetMethodName(), "", msgResponse, null);
                    msgResponse.Description = API_LOGENVELOPModel.CATCH_DESC_EXCEPTION;
                }
            }

            return msgResponse;
        }
        //25102019, Miftahul, begin
        private local.APIMessage<GeneralCIFModel> SubUpdateProCIF(GeneralCIFModel model)
        {
            doLogging(API_LOGENVELOPModel.INFO, "Executing : " + GetMethodName(), "", model,null,model.GUID,model.NIK,model.Module,model.Branch);

            local.APIMessage<GeneralCIFModel> msgResponse = new local.APIMessage<GeneralCIFModel>();
            try
            {
                bool Indikator = false;
                if (model.EmployeeIndicator == "N")
                {
                    Indikator = false;
                }
                else
                {
                    Indikator = true;
                }
                if (string.IsNullOrEmpty(model.EmployeeNIK))
                {
                    model.EmployeeNIK = "0";
                }

                try
                {
                    model.DateIDExpiry = (Convert.ToDateTime(model.DateIDExpiry)).ToString("ddMMyyyy");
                }
                catch
                {
                    model.DateIDExpiry = "";
                }
                

                DataSet dsOut = new DataSet();
                string strErr = "", sqlCommand = "";

                sqlCommand = @" UPDATE [dbo].[CFMAST] SET [CFNA1] = @CFNA1, [CFRESD] = @CFRESD, [CFCOUN] = @CFCOUN, [CFCITZ] = @CFCITZ,
                                [CFRACE] = @CFRACE, [CFSSNO] = @CFSSNO, [CFSSCD] = @CFSSCD, [CFBIR6] = @CFBIR6, [CFSEX] = @CFSEX,
                                [CFCLAS] = @CFCLAS, [CFWHCD] = @CFWHCD, [CFUIC2] = @CFUIC2, [CFUIC3] = @CFUIC3, [CFUIC7] = @CFUIC7,
                                [CFUIC8] = @CFUIC8, [CFIEX6] = @CFIEX6, [CFYIDP] = @CFYIDP, [CFYBIP] = @CFYBIP, [CFMOTN] = @CFMOTN,
                                [CFISD6] = @CFISD6, [CFPLTT] = @CFPLTT, [EmployeeFlag] = @EmployeeFlag, [EmployeeNIK] = @EmployeeNIK, [EmployeeName] = @EmployeeName,
                                [FlagFatca] = @FlagFatca, [KodeUsaha] = @KodeUsaha, [Keterangan] = @Keterangan, [HighRiskUser] = @HighRiskUser, 
                                [KeteranganKodeUsaha] = @KeteranganKodeUsaha WHERE [CFCIF#] = @CFCIF#
                                ";

                SqlParameter[] dbParam = new SqlParameter[30];
                dbParam[0] = new SqlParameter("@CFCIF#", SqlDbType.BigInt);
                dbParam[0].Value = model.CustomerNumber;
                dbParam[0].Direction = System.Data.ParameterDirection.Input;

                dbParam[1] = new SqlParameter("@CFNA1", SqlDbType.VarChar);
                dbParam[1].Value = model.CustomerName1;
                dbParam[1].Direction = System.Data.ParameterDirection.Input;

                dbParam[2] = new SqlParameter("@CFRESD", SqlDbType.VarChar);
                dbParam[2].Value = model.ResidentCode;
                dbParam[2].Direction = System.Data.ParameterDirection.Input;

                dbParam[3] = new SqlParameter("@CFCOUN", SqlDbType.VarChar);
                dbParam[3].Value = model.County;
                dbParam[3].Direction = System.Data.ParameterDirection.Input;

                dbParam[4] = new SqlParameter("@CFCITZ", SqlDbType.VarChar);
                dbParam[4].Value = model.CountryOfCitizenship;
                dbParam[4].Direction = System.Data.ParameterDirection.Input;

                dbParam[5] = new SqlParameter("@CFRACE", SqlDbType.VarChar);
                dbParam[5].Value = model.Religion;
                dbParam[5].Direction = System.Data.ParameterDirection.Input;

                dbParam[6] = new SqlParameter("@CFSSNO", SqlDbType.VarChar);
                dbParam[6].Value = model.IDNumber;
                dbParam[6].Direction = System.Data.ParameterDirection.Input;

                dbParam[7] = new SqlParameter("@CFSSCD", SqlDbType.VarChar);
                dbParam[7].Value = model.IDTypeCode;
                dbParam[7].Direction = System.Data.ParameterDirection.Input;

                dbParam[8] = new SqlParameter("@CFBIR6", SqlDbType.VarChar);
                dbParam[8].Value = model.BirthDate.ToString("yyyyMMdd");
                dbParam[8].Direction = System.Data.ParameterDirection.Input;

                dbParam[9] = new SqlParameter("@CFSEX", SqlDbType.VarChar);
                dbParam[9].Value = model.Sexcode;
                dbParam[9].Direction = System.Data.ParameterDirection.Input;

                dbParam[10] = new SqlParameter("@CFCLAS", SqlDbType.VarChar);
                dbParam[10].Value = model.CustomerTypeCode;
                dbParam[10].Direction = System.Data.ParameterDirection.Input;

                dbParam[11] = new SqlParameter("@CFWHCD", SqlDbType.VarChar);
                dbParam[11].Value = model.TaxCode;
                dbParam[11].Direction = System.Data.ParameterDirection.Input;

                dbParam[12] = new SqlParameter("@CFUIC2", SqlDbType.VarChar);
                dbParam[12].Value = model.StatusPernikahan;
                dbParam[12].Direction = System.Data.ParameterDirection.Input;

                dbParam[13] = new SqlParameter("@CFUIC3", SqlDbType.VarChar);
                dbParam[13].Value = model.LevelPendidikan;
                dbParam[13].Direction = System.Data.ParameterDirection.Input;

                dbParam[14] = new SqlParameter("@CFUIC7", SqlDbType.VarChar);
                dbParam[14].Value = model.RatarataTransaksi;
                dbParam[14].Direction = System.Data.ParameterDirection.Input;

                dbParam[15] = new SqlParameter("@CFUIC8", SqlDbType.VarChar);
                dbParam[15].Value = model.RiskProfile;
                dbParam[15].Direction = System.Data.ParameterDirection.Input;

                dbParam[16] = new SqlParameter("@CFIEX6", SqlDbType.VarChar);
                dbParam[16].Value = model.DateIDExpiry;
                dbParam[16].Direction = System.Data.ParameterDirection.Input;

                dbParam[17] = new SqlParameter("@CFYIDP", SqlDbType.VarChar);
                dbParam[17].Value = model.IDIssuePlace;
                dbParam[17].Direction = System.Data.ParameterDirection.Input;

                dbParam[18] = new SqlParameter("@CFYBIP", SqlDbType.VarChar);
                dbParam[18].Value = model.BirthPlace;
                dbParam[18].Direction = System.Data.ParameterDirection.Input;

                dbParam[19] = new SqlParameter("@CFMOTN", SqlDbType.VarChar);
                dbParam[19].Value = model.MotherMaidenName;
                dbParam[19].Direction = System.Data.ParameterDirection.Input;

                dbParam[20] = new SqlParameter("@CFISD6", SqlDbType.VarChar);
                dbParam[20].Value = model.IdIssuedDateDMY;
                dbParam[20].Direction = System.Data.ParameterDirection.Input;

                dbParam[21] = new SqlParameter("@CFPLTT", SqlDbType.VarChar);
                dbParam[21].Value = model.KodeLaporanTransaksiTunai;
                dbParam[21].Direction = System.Data.ParameterDirection.Input;

                dbParam[22] = new SqlParameter("@EmployeeFlag", SqlDbType.Bit);
                dbParam[22].Value = Indikator;
                dbParam[22].Direction = System.Data.ParameterDirection.Input;

                dbParam[23] = new SqlParameter("@EmployeeNIK", SqlDbType.Int);
                dbParam[23].Value = int.Parse(model.EmployeeNIK);
                dbParam[23].Direction = System.Data.ParameterDirection.Input;

                dbParam[24] = new SqlParameter("@EmployeeName", SqlDbType.VarChar);
                dbParam[24].Value = model.EmployeeName;
                dbParam[24].Direction = System.Data.ParameterDirection.Input;

                dbParam[25] = new SqlParameter("@FlagFatca", SqlDbType.VarChar);
                dbParam[25].Value = model.FatcaFlag;
                dbParam[25].Direction = System.Data.ParameterDirection.Input;

                dbParam[26] = new SqlParameter("@KodeUsaha", SqlDbType.VarChar);
                dbParam[26].Value = model.RiskCode;
                dbParam[26].Direction = System.Data.ParameterDirection.Input;

                dbParam[27] = new SqlParameter("@KeteranganKodeUsaha", SqlDbType.VarChar);
                dbParam[27].Value = "";
                dbParam[27].Direction = System.Data.ParameterDirection.Input;

                dbParam[28] = new SqlParameter("@Keterangan", SqlDbType.VarChar);
                dbParam[28].Value = model.RiskDescription;
                dbParam[28].Direction = System.Data.ParameterDirection.Input;

                dbParam[29] = new SqlParameter("@HighRiskUser", SqlDbType.VarChar);
                dbParam[29].Value = model.HighriskUser;
                dbParam[29].Direction = System.Data.ParameterDirection.Input;

                using (clsDBServiceSQLServer _clsDBServiceSQLServer = new clsDBServiceSQLServer())
                {
                    msgResponse.IsSuccess = _clsDBServiceSQLServer.QueryCommand(_strCIFParameterConnectionString_SQL_CIF, sqlCommand, ref dbParam, _nTimeOut, out dsOut, out strErr);
                    msgResponse.Data = model;
                    if (string.IsNullOrEmpty(strErr))
                    {
                        msgResponse.Description = strErr;
                    }
                    else
                    {
                        msgResponse.Description = strErr;
                        doLogging(API_LOGENVELOPModel.ERROR, "Error Exception : " + this.GetMethodName(), "", msgResponse, null, model.GUID, model.NIK, "", model.Branch);
                        msgResponse.Description = API_LOGENVELOPModel.CATCH_DESC_EXCEPTION;
                    }
                }
            }
            catch (Exception ex)
            {
                msgResponse.IsSuccess = false;
                msgResponse.Data = model;
                msgResponse.Description = API_LOGENVELOPModel.CATCH_DESC_EXCEPTION;
                doLogging(API_LOGENVELOPModel.ERROR, "Catch Exception : " + this.GetMethodName(), "", null, ex, model.GUID);
            }
            return msgResponse;
        }
        //25102019, Miftahul, end
        #endregion

        #region CompareOldNew
        public local.APIMessage<ApprovalMasterModel> ProcessCompareOldAndNewModel(
            GeneralCIFModel createCIFModel, GeneralCIFModel modelOld, ref List<ApprovalDetailModel> outListApprovalDetail)
        {
            local.APIMessage<ApprovalMasterModel> msgResponse = new local.APIMessage<ApprovalMasterModel>();
            StringBuilder strRqDetail = new StringBuilder();//pembentukan RqDetail + nanti assign value dari client
            clsAPIService _service = this;
            outListApprovalDetail = new List<ApprovalDetailModel>();

            try
            {
                #region Compare

                if (modelOld == null)
                {
                    throw new Exception("Inquiry Detail Model Data Old not found!");
                }
                doLogging(API_LOGENVELOPModel.INFO, "Executing : " + GetMethodName(), "", modelOld, null, createCIFModel.GUID, createCIFModel.NIK, createCIFModel.Module, createCIFModel.Branch);

                ApprovalDetailModel approvalModel = new ApprovalDetailModel();               

                int sequence = 0;
                string tableName = "CFMAST";
                local.APIMessage<DataSet> dsOut = new local.APIMessage<DataSet>();
                string tmp;
                tmp = "";
                DataRow[] dataRow = null;


                EKTPDukCaPilModel _eKTPDukCaPilModel;
                _eKTPDukCaPilModel = null;
                if (modelOld.CustomerTypeCode == "A" && (modelOld.IDTypeCode.Equals("KTP") || modelOld.IDTypeCode.Equals("KTPS")))
                {
                    _eKTPDukCaPilModel = this.SubGetDataEKTP(modelOld.IDNumber).Data;
                }

                if (!modelOld.CustomerTypeCode.Equals(createCIFModel.CustomerTypeCode))
                {
                    sequence = sequence + 1;
                    approvalModel.ChangeId = 0; approvalModel.ColumnName = "CFCLAS"; approvalModel.Deskripsi = "CustomerTypeCode";
                    approvalModel.Sequence = sequence; approvalModel.TableName = tableName; approvalModel.WSValue = "";
                    approvalModel.NewDescription = "-";
                    approvalModel.OldDescription = "-";

                    tmp = "";
                    dsOut = new local.APIMessage<DataSet>();
                    dsOut = _service.SubGetParameterCustomerType("");

                    if (dsOut.IsSuccess == true && dsOut.Data.Tables.Count > 0 && dsOut.Data.Tables[0].Rows.Count > 0)
                    {
                        dataRow = dsOut.Data.Tables[0].Select("JHCCOD='" + createCIFModel.CustomerTypeCode.Trim() + "'");
                        if (dataRow.Length > 0)
                        {
                            tmp = dataRow[0]["JHCDES"].ToString().Trim();
                            approvalModel.NewDescription = tmp;
                        }

                        dataRow = dsOut.Data.Tables[0].Select("JHCCOD='" + modelOld.CustomerTypeCode.Trim() + "'");
                        if (dataRow.Length > 0)
                        {
                            tmp = dataRow[0]["JHCDES"].ToString().Trim();
                            approvalModel.OldDescription = tmp;
                        }
                    }

                    approvalModel.NewValue = createCIFModel.CustomerTypeCode;
                    approvalModel.OldValue = modelOld.CustomerTypeCode;
                    outListApprovalDetail.Add(approvalModel);
                }

                if (!modelOld.IDTypeCode.Equals(createCIFModel.IDTypeCode))
                {
                    sequence = sequence + 1;
                    approvalModel.ChangeId = 0; approvalModel.ColumnName = "CFSSCD"; approvalModel.Deskripsi = "Type Identitas";
                    approvalModel.Sequence = sequence; approvalModel.TableName = tableName; approvalModel.WSValue = "";
                    approvalModel.NewDescription = "-";
                    approvalModel.OldDescription = "-";

                    tmp = "";
                    dsOut = new local.APIMessage<DataSet>();
                    dsOut = _service.SubGetParameterIDType("");

                    if (dsOut.IsSuccess == true && dsOut.Data.Tables.Count > 0 && dsOut.Data.Tables[0].Rows.Count > 0)
                    {
                        dataRow = dsOut.Data.Tables[0].Select("CFIDCD='" + createCIFModel.IDTypeCode.Trim() + "'");
                        if (dataRow.Length > 0)
                        {
                            tmp = dataRow[0]["CFIDSC"].ToString().Trim();
                            approvalModel.NewDescription = tmp;
                        }

                        dataRow = dsOut.Data.Tables[0].Select("CFIDCD='" + modelOld.IDTypeCode.Trim() + "'");
                        if (dataRow.Length > 0)
                        {
                            tmp = dataRow[0]["CFIDSC"].ToString().Trim();
                            approvalModel.OldDescription = tmp;
                        }
                    }

                    approvalModel.NewValue = createCIFModel.IDTypeCode;
                    approvalModel.OldValue = modelOld.IDTypeCode;
                    outListApprovalDetail.Add(approvalModel);
                }


                if (!modelOld.IDNumber.Equals(createCIFModel.IDNumber))
                {
                    approvalModel = new ApprovalDetailModel();
                    sequence = sequence + 1;
                    approvalModel.ChangeId = 0; approvalModel.ColumnName = "CFSSNO"; approvalModel.Deskripsi = "Nomor Identitas";
                    approvalModel.Sequence = sequence; approvalModel.TableName = tableName;approvalModel.WSValue = "";

                    if (_eKTPDukCaPilModel != null)
                        approvalModel.WSValue = _eKTPDukCaPilModel.NomorEKTP.ToString();

                    approvalModel.NewDescription = "-";
                    approvalModel.NewValue = createCIFModel.IDNumber;
                    approvalModel.OldDescription = "-";
                    approvalModel.OldValue = modelOld.IDNumber;
                    outListApprovalDetail.Add(approvalModel);
                }

                if (!modelOld.DateIDExpiry.Equals(createCIFModel.DateIDExpiry))
                {
                    approvalModel = new ApprovalDetailModel();
                    sequence = sequence + 1;
                    approvalModel.ChangeId = 0; approvalModel.ColumnName = "CFIEX6"; approvalModel.Deskripsi = "Tanggal Expire Identitas";
                    approvalModel.Sequence = sequence; approvalModel.TableName = tableName; approvalModel.WSValue = "";

                    approvalModel.NewDescription = "-";
                    approvalModel.NewValue = createCIFModel.DateIDExpiry;
                    approvalModel.OldDescription = "-";
                    approvalModel.OldValue = modelOld.DateIDExpiry;
                    outListApprovalDetail.Add(approvalModel);
                }

                if (!modelOld.IDIssuePlace.Equals(createCIFModel.IDIssuePlace))
                {
                    approvalModel = new ApprovalDetailModel();
                    sequence = sequence + 1;
                    approvalModel.ChangeId = 0; approvalModel.ColumnName = "CFYIDP"; approvalModel.Deskripsi = "Tempat Dikeluarkan Identitas";
                    approvalModel.Sequence = sequence; approvalModel.TableName = tableName; approvalModel.WSValue = "";

                    if (_eKTPDukCaPilModel != null)
                        approvalModel.WSValue = _eKTPDukCaPilModel.KabName.ToString();

                    approvalModel.NewDescription = "-";
                    approvalModel.NewValue = createCIFModel.IDIssuePlace;
                    approvalModel.OldDescription = "-";
                    approvalModel.OldValue = modelOld.IDIssuePlace;
                    outListApprovalDetail.Add(approvalModel);
                }

                if (!modelOld.BirthDate.Equals(createCIFModel.BirthDate))
                {
                    approvalModel = new ApprovalDetailModel();
                    sequence = sequence + 1;
                    approvalModel.ChangeId = 0; approvalModel.ColumnName = "CFBIR6"; approvalModel.Deskripsi = "Tanggal Lahir";
                    approvalModel.Sequence = sequence; approvalModel.TableName = tableName; approvalModel.WSValue = "";

                    if (_eKTPDukCaPilModel != null)
                        approvalModel.WSValue = _eKTPDukCaPilModel.TanggalLahir.ToString();

                    approvalModel.NewDescription = "-";
                    approvalModel.NewValue = createCIFModel.BirthDate.ToString("dd-MM-yyyy");
                    approvalModel.OldDescription = "-";
                    approvalModel.OldValue = modelOld.BirthDate.ToString("dd-MM-yyyy");
                    outListApprovalDetail.Add(approvalModel);
                }

                if (!modelOld.BirthPlace.Equals(createCIFModel.BirthPlace))
                {
                    approvalModel = new ApprovalDetailModel();
                    sequence = sequence + 1;
                    approvalModel.ChangeId = 0; approvalModel.ColumnName = "CFYBIP"; approvalModel.Deskripsi = "Tempat Lahir";
                    approvalModel.Sequence = sequence; approvalModel.TableName = tableName; approvalModel.WSValue = "";

                    if (_eKTPDukCaPilModel != null)
                        approvalModel.WSValue = _eKTPDukCaPilModel.TempatLahir.ToString();

                    approvalModel.NewDescription = "-";
                    approvalModel.NewValue = createCIFModel.BirthPlace;
                    approvalModel.OldDescription = "-";
                    approvalModel.OldValue = modelOld.BirthPlace;
                    outListApprovalDetail.Add(approvalModel);
                }

                if (!modelOld.Sexcode.Equals(createCIFModel.Sexcode))
                {
                    approvalModel = new ApprovalDetailModel();
                    sequence = sequence + 1;
                    approvalModel.ChangeId = 0; approvalModel.ColumnName = "CFSEX"; approvalModel.Deskripsi = "Jenis Kelamin";
                    approvalModel.Sequence = sequence; approvalModel.TableName = tableName; approvalModel.WSValue = "";

                    if (_eKTPDukCaPilModel != null)
                        approvalModel.WSValue = _eKTPDukCaPilModel.JenisKelamin.ToString();

                    approvalModel.NewDescription = "-";
                    approvalModel.OldDescription = "-";

                    if (createCIFModel.Sexcode.Trim() == "F")
                    {
                        approvalModel.NewDescription = "Female";
                    }
                    else
                    {
                        approvalModel.NewDescription = "Male";
                    }

                    if (modelOld.Sexcode.Trim() == "F")
                    {
                        approvalModel.OldDescription = "Female";
                    }
                    else
                    {
                        approvalModel.OldDescription = "Male";
                    }

                    approvalModel.NewValue = createCIFModel.Sexcode;
                    approvalModel.OldValue = modelOld.Sexcode;
                    outListApprovalDetail.Add(approvalModel);
                }

                if (!modelOld.Religion.Equals(createCIFModel.Religion))
                {
                    approvalModel = new ApprovalDetailModel();
                    sequence = sequence + 1;
                    approvalModel.ChangeId = 0; approvalModel.ColumnName = "CFRACE"; approvalModel.Deskripsi = "Agama";
                    approvalModel.Sequence = sequence; approvalModel.TableName = tableName; approvalModel.WSValue = "";

                    if (_eKTPDukCaPilModel != null)
                        approvalModel.WSValue = _eKTPDukCaPilModel.Agama.ToString();

                    approvalModel.NewDescription = "-";
                    approvalModel.OldDescription = "-";
                    tmp = "";
                    dsOut = new local.APIMessage<DataSet>();
                    dsOut = _service.SubGetParameterReligion("");

                    if (dsOut.IsSuccess == true && dsOut.Data.Tables.Count > 0 && dsOut.Data.Tables[0].Rows.Count > 0)
                    {
                        dataRow = dsOut.Data.Tables[0].Select("KodeAgama='" + createCIFModel.Religion.Trim() + "'");
                        if (dataRow.Length > 0)
                        {
                            tmp = dataRow[0]["DeskripsiAgama"].ToString().Trim();
                            approvalModel.NewDescription = tmp;
                        }

                        dataRow = dsOut.Data.Tables[0].Select("KodeAgama='" + modelOld.Religion.Trim() + "'");
                        if (dataRow.Length > 0)
                        {
                            tmp = dataRow[0]["DeskripsiAgama"].ToString().Trim();
                            approvalModel.OldDescription = tmp;
                        }
                    }

                    approvalModel.NewValue = createCIFModel.Religion;
                    approvalModel.OldValue = modelOld.Religion;
                    outListApprovalDetail.Add(approvalModel);
                }

                if (!modelOld.CountryOfCitizenship.Equals(createCIFModel.CountryOfCitizenship))
                {
                    approvalModel = new ApprovalDetailModel();
                    sequence = sequence + 1;
                    approvalModel.ChangeId = 0; approvalModel.ColumnName = "CFCITZ"; approvalModel.Deskripsi = "Kewarganegaraan";
                    approvalModel.Sequence = sequence; approvalModel.TableName = tableName; approvalModel.WSValue = "";

                    approvalModel.NewDescription = "-";
                    approvalModel.OldDescription = "-";
                    tmp = "";
                    dsOut = new local.APIMessage<DataSet>();
                    dsOut = _service.SubGetParameterMasterCountry("");

                    if (dsOut.IsSuccess == true && dsOut.Data.Tables.Count > 0 && dsOut.Data.Tables[0].Rows.Count > 0)
                    {
                        dataRow = dsOut.Data.Tables[0].Select("JHCCOC='" + createCIFModel.CountryOfCitizenship.Trim() + "'");
                        if (dataRow.Length > 0)
                        {
                            tmp = dataRow[0]["JHCNAM"].ToString().Trim();
                            approvalModel.NewDescription = tmp;
                        }

                        dataRow = dsOut.Data.Tables[0].Select("JHCCOC='" + modelOld.CountryOfCitizenship.Trim() + "'");
                        if (dataRow.Length > 0)
                        {
                            tmp = dataRow[0]["JHCNAM"].ToString().Trim();
                            approvalModel.OldDescription = tmp;
                        }
                    }

                    approvalModel.NewValue = createCIFModel.CountryOfCitizenship;
                    approvalModel.OldValue = modelOld.CountryOfCitizenship;
                    outListApprovalDetail.Add(approvalModel);
                }

                if (!modelOld.CountryOfResidence.Equals(createCIFModel.CountryOfResidence))
                {
                    approvalModel = new ApprovalDetailModel();
                    sequence = sequence + 1;
                    approvalModel.ChangeId = 0; approvalModel.ColumnName = "CFCOUN"; approvalModel.Deskripsi = "Negara Domisili";
                    approvalModel.Sequence = sequence; approvalModel.TableName = tableName; approvalModel.WSValue = "";

                    approvalModel.NewDescription = "-";
                    approvalModel.OldDescription = "-";
                    tmp = "";
                    dsOut = new local.APIMessage<DataSet>();
                    dsOut = _service.SubGetParameterMasterCountry("");

                    if (dsOut.IsSuccess == true && dsOut.Data.Tables.Count > 0 && dsOut.Data.Tables[0].Rows.Count > 0)
                    {
                        dataRow = dsOut.Data.Tables[0].Select("JHCCOC='" + createCIFModel.CountryOfResidence.Trim() + "'");
                        if (dataRow.Length > 0)
                        {
                            tmp = dataRow[0]["JHCNAM"].ToString().Trim();
                            approvalModel.NewDescription = tmp;
                        }

                        dataRow = dsOut.Data.Tables[0].Select("JHCCOC='" + modelOld.CountryOfResidence.Trim() + "'");
                        if (dataRow.Length > 0)
                        {
                            tmp = dataRow[0]["JHCNAM"].ToString().Trim();
                            approvalModel.OldDescription = tmp;
                        }
                    }

                    approvalModel.NewValue = createCIFModel.CountryOfResidence;
                    approvalModel.OldValue = modelOld.CountryOfResidence;
                    outListApprovalDetail.Add(approvalModel);
                }

                if (!modelOld.ResidentCode.Equals(createCIFModel.ResidentCode))
                {
                    approvalModel = new ApprovalDetailModel();
                    sequence = sequence + 1;
                    approvalModel.ChangeId = 0; approvalModel.ColumnName = "CFRESD"; approvalModel.Deskripsi = "Status Kependudukan";
                    approvalModel.Sequence = sequence; approvalModel.TableName = tableName; approvalModel.WSValue = "";

                    approvalModel.NewDescription = "-";
                    approvalModel.NewValue = createCIFModel.ResidentCode;
                    approvalModel.OldDescription = "-";
                    approvalModel.OldValue = modelOld.ResidentCode;
                    outListApprovalDetail.Add(approvalModel);
                }

                if (!modelOld.StatusPernikahan.Equals(createCIFModel.StatusPernikahan))
                {
                    approvalModel = new ApprovalDetailModel();
                    sequence = sequence + 1;
                    approvalModel.ChangeId = 0; approvalModel.ColumnName = "CFUIC2"; approvalModel.Deskripsi = "Status Pernikahan";
                    approvalModel.Sequence = sequence; approvalModel.TableName = tableName; approvalModel.WSValue = "";

                    if (_eKTPDukCaPilModel != null)
                        approvalModel.WSValue = _eKTPDukCaPilModel.StatusKawin.ToString();

                    approvalModel.NewDescription = "-";
                    approvalModel.OldDescription = "-";
                    tmp = "";
                    dsOut = new local.APIMessage<DataSet>();
                    dsOut = _service.SubGetParameterMasterCFPAR3("", "");

                    if (dsOut.IsSuccess == true && dsOut.Data.Tables.Count > 0 && dsOut.Data.Tables[0].Rows.Count > 0)
                    {
                        dataRow = dsOut.Data.Tables[0].Select("CP3UCD='" + createCIFModel.StatusPernikahan.Trim() + "' and CP3UIC=2");
                        if (dataRow.Length > 0)
                        {
                            tmp = dataRow[0]["CP3DSC"].ToString().Trim();
                            approvalModel.NewDescription = tmp;
                        }

                        dataRow = dsOut.Data.Tables[0].Select("CP3UCD='" + modelOld.StatusPernikahan.Trim() + "'  and CP3UIC=2");
                        if (dataRow.Length > 0)
                        {
                            tmp = dataRow[0]["CP3DSC"].ToString().Trim();
                            approvalModel.OldDescription = tmp;
                        }
                    }

                    approvalModel.NewValue = createCIFModel.StatusPernikahan;
                    approvalModel.OldValue = modelOld.StatusPernikahan;
                    outListApprovalDetail.Add(approvalModel);
                }

                if (!modelOld.LevelPendidikan.Equals(createCIFModel.LevelPendidikan))
                {
                    approvalModel = new ApprovalDetailModel();
                    sequence = sequence + 1;
                    approvalModel.ChangeId = 0; approvalModel.ColumnName = "CFUIC3"; approvalModel.Deskripsi = "Level Pendidikan";
                    approvalModel.Sequence = sequence; approvalModel.TableName = tableName; approvalModel.WSValue = "";

                    if (_eKTPDukCaPilModel != null)
                        approvalModel.WSValue = _eKTPDukCaPilModel.PendidikanAkhir.ToString();

                    approvalModel.NewDescription = "-";
                    approvalModel.OldDescription = "-";
                    tmp = "";
                    dsOut = new local.APIMessage<DataSet>();
                    dsOut = _service.SubGetParameterMasterCFPAR3("", "");

                    if (dsOut.IsSuccess == true && dsOut.Data.Tables.Count > 0 && dsOut.Data.Tables[0].Rows.Count > 0)
                    {
                        dataRow = dsOut.Data.Tables[0].Select("CP3UCD='" + createCIFModel.LevelPendidikan.Trim() + "' and CP3UIC=3");
                        if (dataRow.Length > 0)
                        {
                            tmp = dataRow[0]["CP3DSC"].ToString().Trim();
                            approvalModel.NewDescription = tmp;
                        }

                        dataRow = dsOut.Data.Tables[0].Select("CP3UCD='" + modelOld.LevelPendidikan.Trim() + "' and CP3UIC=3");
                        if (dataRow.Length > 0)
                        {
                            tmp = dataRow[0]["CP3DSC"].ToString().Trim();
                            approvalModel.OldDescription = tmp;
                        }
                    }

                    approvalModel.NewValue = createCIFModel.LevelPendidikan;
                    approvalModel.OldValue = modelOld.LevelPendidikan;
                    outListApprovalDetail.Add(approvalModel);
                }

                if (!modelOld.RatarataTransaksi.Equals(createCIFModel.RatarataTransaksi))
                {
                    approvalModel = new ApprovalDetailModel();
                    sequence = sequence + 1;
                    approvalModel.ChangeId = 0; approvalModel.ColumnName = "CFUIC7"; approvalModel.Deskripsi = "Rata-rata Transaksi/bln";
                    approvalModel.Sequence = sequence; approvalModel.TableName = tableName; approvalModel.WSValue = "";

                    approvalModel.NewDescription = "-";
                    approvalModel.OldDescription = "-";
                    tmp = "";
                    dsOut = new local.APIMessage<DataSet>();
                    dsOut = _service.SubGetParameterMasterCFPAR3("", "");

                    if (dsOut.IsSuccess == true && dsOut.Data.Tables.Count > 0 && dsOut.Data.Tables[0].Rows.Count > 0)
                    {
                        dataRow = dsOut.Data.Tables[0].Select("CP3UCD='" + createCIFModel.RatarataTransaksi.Trim() + "' and CP3UIC=7");
                        if (dataRow.Length > 0)
                        {
                            tmp = dataRow[0]["CP3DSC"].ToString().Trim();
                            approvalModel.NewDescription = tmp;
                        }

                        dataRow = dsOut.Data.Tables[0].Select("CP3UCD='" + modelOld.RatarataTransaksi.Trim() + "' and CP3UIC=7");
                        if (dataRow.Length > 0)
                        {
                            tmp = dataRow[0]["CP3DSC"].ToString().Trim();
                            approvalModel.OldDescription = tmp;
                        }
                    }

                    approvalModel.NewValue = createCIFModel.RatarataTransaksi;
                    approvalModel.OldValue = modelOld.RatarataTransaksi;
                    outListApprovalDetail.Add(approvalModel);
                }

                if (!modelOld.RiskProfile.Equals(createCIFModel.RiskProfile))
                {
                    approvalModel = new ApprovalDetailModel();
                    sequence = sequence + 1;
                    approvalModel.ChangeId = 0; approvalModel.ColumnName = "CFUIC8"; approvalModel.Deskripsi = "Risk Profile";
                    approvalModel.Sequence = sequence; approvalModel.TableName = tableName; approvalModel.WSValue = "";

                    approvalModel.NewDescription = "-";
                    approvalModel.OldDescription = "-";
                    tmp = "";
                    dsOut = new local.APIMessage<DataSet>();
                    dsOut = _service.SubGetParameterMasterCFPAR3("", "");

                    if (dsOut.IsSuccess == true && dsOut.Data.Tables.Count > 0 && dsOut.Data.Tables[0].Rows.Count > 0)
                    {
                        dataRow = dsOut.Data.Tables[0].Select("CP3UCD='" + createCIFModel.RiskProfile.Trim() + "' and CP3UIC=8");
                        if (dataRow.Length > 0)
                        {
                            tmp = dataRow[0]["CP3DSC"].ToString().Trim();
                            approvalModel.NewDescription = tmp;
                        }

                        dataRow = dsOut.Data.Tables[0].Select("CP3UCD='" + modelOld.RiskProfile.Trim() + "' and CP3UIC=8");
                        if (dataRow.Length > 0)
                        {
                            tmp = dataRow[0]["CP3DSC"].ToString().Trim();
                            approvalModel.OldDescription = tmp;
                        }
                    }

                    approvalModel.NewValue = createCIFModel.RiskProfile;
                    approvalModel.OldValue = modelOld.RiskProfile;
                    outListApprovalDetail.Add(approvalModel);
                }

                if (!modelOld.MotherMaidenName.Equals(createCIFModel.MotherMaidenName))
                {
                    approvalModel = new ApprovalDetailModel();
                    sequence = sequence + 1;
                    approvalModel.ChangeId = 0; approvalModel.ColumnName = "CFMOTN"; approvalModel.Deskripsi = "Nama Ibu Kandung";
                    approvalModel.Sequence = sequence; approvalModel.TableName = tableName; approvalModel.WSValue = "";

                    if (_eKTPDukCaPilModel != null)
                        approvalModel.WSValue = _eKTPDukCaPilModel.NamaLengkapIbu.ToString();

                    approvalModel.NewDescription = "-";
                    approvalModel.NewValue = createCIFModel.MotherMaidenName;
                    approvalModel.OldDescription = "-";
                    approvalModel.OldValue = modelOld.MotherMaidenName;
                    outListApprovalDetail.Add(approvalModel);
                }

                if (!modelOld.IdIssuedDate.Equals(createCIFModel.IdIssuedDate))
                {
                    approvalModel = new ApprovalDetailModel();
                    sequence = sequence + 1;
                    approvalModel.ChangeId = 0; approvalModel.ColumnName = "CFISD6"; approvalModel.Deskripsi = "Tanggal Dikeluarkan Identitas";
                    approvalModel.Sequence = sequence; approvalModel.TableName = tableName; approvalModel.WSValue = "";

                    approvalModel.NewDescription = "-";
                    approvalModel.NewValue = DateTimeLib.GetDateTime(createCIFModel.IdIssuedDate, "yyyy-MM-dd").ToString("dd-MM-yyyy");
                    approvalModel.OldDescription = "-";
                    approvalModel.OldValue = DateTimeLib.GetDateTime(modelOld.IdIssuedDate, "yyyy-MM-dd").ToString("dd-MM-yyyy");
                    outListApprovalDetail.Add(approvalModel);
                }

                if (!modelOld.KodeLaporanTransaksiTunai.Equals(createCIFModel.KodeLaporanTransaksiTunai))
                {
                    approvalModel = new ApprovalDetailModel();
                    sequence = sequence + 1;
                    approvalModel.ChangeId = 0; approvalModel.ColumnName = "CFPLTT"; approvalModel.Deskripsi = "Kode Laporan Transaksi Tunai";
                    approvalModel.Sequence = sequence; approvalModel.TableName = tableName; approvalModel.WSValue = "";

                    approvalModel.NewDescription = "-";
                    approvalModel.OldDescription = "-";
                    tmp = "";
                    dsOut = new local.APIMessage<DataSet>();
                    dsOut = _service.SubGetParameterMasterLapTrxTunai("");

                    if (dsOut.IsSuccess == true && dsOut.Data.Tables.Count > 0 && dsOut.Data.Tables[0].Rows.Count > 0)
                    {
                        dataRow = dsOut.Data.Tables[0].Select("CFTTCD='" + createCIFModel.KodeLaporanTransaksiTunai.Trim() + "'");
                        if (dataRow.Length > 0)
                        {
                            tmp = dataRow[0]["CFTTDS"].ToString().Trim();
                            approvalModel.NewDescription = tmp;
                        }

                        dataRow = dsOut.Data.Tables[0].Select("CFTTCD='" + modelOld.KodeLaporanTransaksiTunai.Trim() + "'");
                        if (dataRow.Length > 0)
                        {
                            tmp = dataRow[0]["CFTTDS"].ToString().Trim();
                            approvalModel.OldDescription = tmp;
                        }
                    }

                    approvalModel.NewValue = createCIFModel.KodeLaporanTransaksiTunai;
                    approvalModel.OldValue = modelOld.KodeLaporanTransaksiTunai;
                    outListApprovalDetail.Add(approvalModel);
                }

                if (!modelOld.TaxCode.Equals(createCIFModel.TaxCode))
                {
                    approvalModel = new ApprovalDetailModel();
                    sequence = sequence + 1;
                    approvalModel.ChangeId = 0; approvalModel.ColumnName = "CFWHCD"; approvalModel.Deskripsi = "Status Pajak User";
                    approvalModel.Sequence = sequence; approvalModel.TableName = tableName; approvalModel.WSValue = "";

                    approvalModel.NewDescription = "-";
                    approvalModel.OldDescription = "-";
                    tmp = "";
                    dsOut = new local.APIMessage<DataSet>();
                    dsOut = _service.SubGetParameterMasterPajak("");

                    if (dsOut.IsSuccess == true && dsOut.Data.Tables.Count > 0 && dsOut.Data.Tables[0].Rows.Count > 0)
                    {
                        dataRow = dsOut.Data.Tables[0].Select("KDPajak='" + createCIFModel.TaxCode.Trim() + "'");
                        if (dataRow.Length > 0)
                        {
                            tmp = dataRow[0]["DeskripsiPajak"].ToString().Trim();
                            approvalModel.NewDescription = tmp;
                        }

                        dataRow = dsOut.Data.Tables[0].Select("KDPajak='" + modelOld.TaxCode.Trim() + "'");
                        if (dataRow.Length > 0)
                        {
                            tmp = dataRow[0]["DeskripsiPajak"].ToString().Trim();
                            approvalModel.OldDescription = tmp;
                        }
                    }

                    approvalModel.NewValue = createCIFModel.TaxCode;
                    approvalModel.OldValue = modelOld.TaxCode;
                    outListApprovalDetail.Add(approvalModel);
                }

                if (!modelOld.EmployeeNIK.Equals(createCIFModel.EmployeeNIK))
                {
                    approvalModel = new ApprovalDetailModel();
                    sequence = sequence + 1;
                    approvalModel.ChangeId = 0; approvalModel.ColumnName = "EmployeeNIK"; approvalModel.Deskripsi = "NIK Nasabah";
                    approvalModel.Sequence = sequence; approvalModel.TableName = tableName; approvalModel.WSValue = "";

                    approvalModel.NewDescription = "-";
                    approvalModel.NewValue = createCIFModel.EmployeeNIK;
                    approvalModel.OldDescription = "-";
                    approvalModel.OldValue = modelOld.EmployeeNIK;
                    outListApprovalDetail.Add(approvalModel);
                }

                if (!modelOld.EmployeeName.Equals(createCIFModel.EmployeeNIK))
                {
                    approvalModel = new ApprovalDetailModel();
                    sequence = sequence + 1;
                    approvalModel.ChangeId = 0; approvalModel.ColumnName = "EmployeeName"; approvalModel.Deskripsi = "Nama Karyawan";
                    approvalModel.Sequence = sequence; approvalModel.TableName = tableName; approvalModel.WSValue = "";

                    if (_eKTPDukCaPilModel != null)
                        approvalModel.WSValue = _eKTPDukCaPilModel.NamaLengkap.ToString();

                    approvalModel.NewDescription = "-";
                    approvalModel.NewValue = createCIFModel.EmployeeName;
                    approvalModel.OldDescription = "-";
                    approvalModel.OldValue = modelOld.EmployeeName;
                    outListApprovalDetail.Add(approvalModel);
                }

                if (!modelOld.EmployeeIndicator.Equals(createCIFModel.EmployeeIndicator))
                {
                    approvalModel = new ApprovalDetailModel();
                    sequence = sequence + 1;
                    approvalModel.ChangeId = 0; approvalModel.ColumnName = "EmployeeFlag"; approvalModel.Deskripsi = "Status Karyawan";
                    approvalModel.Sequence = sequence; approvalModel.TableName = tableName; approvalModel.WSValue = "";

                    approvalModel.NewDescription = "-";
                    approvalModel.NewValue = createCIFModel.EmployeeIndicator;
                    approvalModel.OldDescription = "-";
                    approvalModel.OldValue = modelOld.EmployeeIndicator;
                    outListApprovalDetail.Add(approvalModel);
                }

                if (!modelOld.FatcaFlag.Equals(createCIFModel.FatcaFlag))
                {
                    approvalModel = new ApprovalDetailModel();
                    sequence = sequence + 1;
                    approvalModel.ChangeId = 0; approvalModel.ColumnName = "FatcaFlag"; approvalModel.Deskripsi = "Fatca Flag";
                    approvalModel.Sequence = sequence; approvalModel.TableName = tableName; approvalModel.WSValue = "";

                    approvalModel.NewDescription = "-";
                    approvalModel.NewValue = createCIFModel.FatcaFlag;
                    approvalModel.OldDescription = "-";
                    approvalModel.OldValue = modelOld.FatcaFlag;
                    outListApprovalDetail.Add(approvalModel);
                }

                if (!modelOld.FatcaDate.Equals(createCIFModel.FatcaDate))
                {
                    approvalModel = new ApprovalDetailModel();
                    sequence = sequence + 1;
                    approvalModel.ChangeId = 0; approvalModel.ColumnName = "DateFatca"; approvalModel.Deskripsi = "Tanggal Fatca";
                    approvalModel.Sequence = sequence; approvalModel.TableName = tableName; approvalModel.WSValue = "";

                    approvalModel.NewDescription = "-";
                    approvalModel.NewValue = createCIFModel.FatcaDate;
                    approvalModel.OldDescription = "-";
                    approvalModel.OldValue = modelOld.FatcaDate;
                    outListApprovalDetail.Add(approvalModel);
                }

                if (!modelOld.RiskCode.Equals(createCIFModel.RiskCode))
                {
                    approvalModel = new ApprovalDetailModel();
                    sequence = sequence + 1;
                    approvalModel.ChangeId = 0; approvalModel.ColumnName = "KodeUsaha"; approvalModel.Deskripsi = "Kode Identifikasi Resiko Nasabah";
                    approvalModel.Sequence = sequence; approvalModel.TableName = tableName; approvalModel.WSValue = "";

                    approvalModel.NewDescription = "-";
                    approvalModel.NewValue = createCIFModel.RiskCode;
                    approvalModel.OldDescription = "-";
                    approvalModel.OldValue = modelOld.RiskCode;
                    outListApprovalDetail.Add(approvalModel);
                }

                if (!modelOld.RiskDescription.Equals(createCIFModel.RiskDescription))
                {
                    approvalModel = new ApprovalDetailModel();
                    sequence = sequence + 1;
                    approvalModel.ChangeId = 0; approvalModel.ColumnName = "Keterangan"; approvalModel.Deskripsi = "Keterangan Identifikasi Resiko Nasabah";
                    approvalModel.Sequence = sequence; approvalModel.TableName = tableName; approvalModel.WSValue = "";

                    approvalModel.NewDescription = "-";
                    approvalModel.NewValue = createCIFModel.RiskDescription;
                    approvalModel.OldDescription = "-";
                    approvalModel.OldValue = modelOld.RiskDescription;
                    outListApprovalDetail.Add(approvalModel);
                }

                if (!modelOld.HighriskUser.Equals(createCIFModel.HighriskUser))
                {
                    approvalModel = new ApprovalDetailModel();
                    sequence = sequence + 1;
                    approvalModel.ChangeId = 0; approvalModel.ColumnName = "HighRiskUser"; approvalModel.Deskripsi = "HighRisk User";
                    approvalModel.Sequence = sequence; approvalModel.TableName = tableName; approvalModel.WSValue = "";

                    approvalModel.NewDescription = "-";
                    approvalModel.NewValue = createCIFModel.HighriskUser;
                    approvalModel.OldDescription = "-";
                    approvalModel.OldValue = modelOld.HighriskUser;
                    outListApprovalDetail.Add(approvalModel);
                }

                if (!modelOld.CRSFlag.Equals(createCIFModel.CRSFlag))
                {
                    approvalModel = new ApprovalDetailModel();
                    sequence = sequence + 1;
                    approvalModel.ChangeId = 0; approvalModel.ColumnName = "CRS"; approvalModel.Deskripsi = "CRS Flag";
                    approvalModel.Sequence = sequence; approvalModel.TableName = "CIFCRS_TM"; approvalModel.WSValue = "";

                    approvalModel.NewDescription = "-";
                    approvalModel.NewValue = createCIFModel.CRSFlag;
                    approvalModel.OldDescription = "-";
                    approvalModel.OldValue = modelOld.CRSFlag;
                    outListApprovalDetail.Add(approvalModel);
                }

                if (!String.IsNullOrEmpty(createCIFModel.CRSDate))
                {
                    if (!modelOld.CRSDate.Equals(Convert.ToDateTime(createCIFModel.CRSDate).ToString("yyyy-MM-dd")))
                    {
                        approvalModel = new ApprovalDetailModel();
                        sequence = sequence + 1;
                        approvalModel.ChangeId = 0; approvalModel.ColumnName = "DateCRS"; approvalModel.Deskripsi = "Tanggal CRS";
                        approvalModel.Sequence = sequence; approvalModel.TableName = "CIFCRS_TM"; approvalModel.WSValue = "";

                        approvalModel.NewDescription = "-";
                        approvalModel.NewValue = Convert.ToDateTime(createCIFModel.CRSDate).ToString("yyyy-MM-dd");
                        approvalModel.OldDescription = "-";
                        approvalModel.OldValue = modelOld.CRSDate;
                        outListApprovalDetail.Add(approvalModel);
                    }
                }
                #endregion

                if (outListApprovalDetail.Count > 0)
                {
                    doLogging(API_LOGENVELOPModel.INFO, "Data Check ListApprovalDetail : " + GetMethodName(), "", outListApprovalDetail, null, createCIFModel.GUID, createCIFModel.NIK, createCIFModel.Module, createCIFModel.Branch);

                    ApprovalMasterModel masterModel = new ApprovalMasterModel();
                    masterModel.Branch = createCIFModel.Branch;
                    masterModel.ChangeType = "U";
                    masterModel.CIFName = createCIFModel.CustomerName1;
                    masterModel.CIFNumber = createCIFModel.CustomerNumber;
                    masterModel.GUID = createCIFModel.GUID;

                    string jsonNew = JsonConvert.SerializeObject(createCIFModel);
                    string jsonOld = JsonConvert.SerializeObject(modelOld);

                    masterModel.JsonDataNew = jsonNew;
                    masterModel.JsonDataOld = jsonOld;
                    masterModel.StaffNIK = createCIFModel.NIK;
                    masterModel.Status = "2";
                    masterModel.SubSystem = createCIFModel.Module;
                    masterModel.TableName = tableName;

                    msgResponse.Data = masterModel;
                    msgResponse.IsSuccess = true;
                    msgResponse.Description = "";
                }
            }
            catch (Exception ex)
            {
                msgResponse.Data = null;
                msgResponse.IsSuccess = false;
                //msgResponse.Description = ex.Message + Environment.NewLine + ex.StackTrace.ToString();

                msgResponse.Description = API_LOGENVELOPModel.CATCH_DESC_EXCEPTION;
                doLogging(API_LOGENVELOPModel.ERROR, "Catch Exception : " + GetMethodName(), "", null, ex);
            }

            return msgResponse;
        }
        
        private local.APIMessage<bool> ProcessSyncWithREKSA(GeneralCIFModel model)
        {
            local.APIMessage<bool> msgResponse = new local.APIMessage<bool>();           

            try
            {
                doLogging(API_LOGENVELOPModel.INFO, "Executing : " + GetMethodName(), "", null, null, model.GUID, model.NIK, model.Module, model.Branch);

                StringBuilder xml = new StringBuilder();
                xml.AppendLine("<Root>");
                xml.AppendLine("<DATA>");
                xml.AppendLine("<Type>CFMAST</Type>");
                xml.AppendLine("<CFCIF>" + model.CustomerNumber.ToString() + "</CFCIF>");
                xml.AppendLine("<CFRACE>" + model.Religion.ToString() + "</CFRACE>");
                xml.AppendLine("<CFSSNO>" + model.IDNumber.ToString() + "</CFSSNO>");
                xml.AppendLine("<CFSSCD>" + model.IDTypeCode.ToString() + "</CFSSCD>");
                xml.AppendLine("<CFBIR6>" + model.BirthDate.ToString("yyyyMMdd") + "</CFBIR6>");
                xml.AppendLine("<CFSEX>" + model.Sexcode.ToString() + "</CFSEX>");
                xml.AppendLine("</DATA>");
                xml.AppendLine("</Root>");
                             
                string strErr = "";

                StringBuilder batchInsert = new StringBuilder();
                batchInsert.AppendLine("BEGIN TRY");
                batchInsert.AppendLine("DECLARE @nOK int");
                batchInsert.AppendLine("exec @nOK = SQL_REKSA2.dbo.ReksaUpdateCIFData 0,1,@pxmlData,0");
                batchInsert.AppendLine("if @nOK != 0 or @@error != 0");
                batchInsert.AppendLine("begin");
                batchInsert.AppendLine("raiserror ('Gagal Sinkronisasi Data Pribadi ke Pro Reksa', 16, 1); ");
                batchInsert.AppendLine("end");
                batchInsert.AppendLine("END TRY ");
                batchInsert.AppendLine("BEGIN CATCH ");
                batchInsert.AppendLine("IF @@TRANCOUNT > 0");
                batchInsert.AppendLine("BEGIN");
                batchInsert.AppendLine("ROLLBACK TRAN; ");
                batchInsert.AppendLine("END; ");
                batchInsert.AppendLine("declare @errorMsg varchar(500);");
                batchInsert.AppendLine("set @errorMsg = ERROR_MESSAGE();");
                batchInsert.AppendLine("raiserror (@errorMsg, 16, 1); ");
                batchInsert.AppendLine("END CATCH");

                SqlParameter[] dbParam = new SqlParameter[1];
                dbParam[0] = new SqlParameter("@pxmlData", SqlDbType.VarChar);
                dbParam[0].Value = xml.ToString();
                dbParam[0].Direction = System.Data.ParameterDirection.Input;

                using (clsDBServiceSQLServer _clsDBServiceSQLServer = new clsDBServiceSQLServer())
                {
                    msgResponse.IsSuccess = _clsDBServiceSQLServer.QueryCommand(_strCIFParameterConnectionString_SQL_CIF, batchInsert.ToString(), ref dbParam, _nTimeOut, out strErr);
                    msgResponse.Data = true;
                    if (string.IsNullOrEmpty(strErr))
                    {
                        msgResponse.Description = strErr;
                    }
                    else
                    {
                        msgResponse.Description = strErr;
                        msgResponse.Data = false;
                        doLogging(API_LOGENVELOPModel.ERROR, "Error Exception : " + this.GetMethodName(), "", msgResponse, null, model.GUID, model.NIK, "", model.Branch);
                        msgResponse.Description = API_LOGENVELOPModel.CATCH_DESC_EXCEPTION;
                    }
                }
            }
            catch(Exception ex)
            {
                msgResponse.Data = false;
                msgResponse.IsSuccess = false;
                msgResponse.Description = API_LOGENVELOPModel.CATCH_DESC_EXCEPTION;
                doLogging(API_LOGENVELOPModel.ERROR, "Catch Exception : " + GetMethodName(), "", null, ex, model.GUID, model.NIK, model.Module, model.Branch);
            }

            return msgResponse;
        }
        #endregion
        
        #region Parameter
        public local.APIMessage<DataSet> SubCheckIDTypeWithCustomerType(string CustomerTypeCode, string IDType)
        {
            doLogging(API_LOGENVELOPModel.INFO, "Executing : " + GetMethodName(), "", CustomerTypeCode + "|" + IDType);

            local.APIMessage<DataSet> msgResponse = new local.APIMessage<DataSet>();
            DataSet dsOut = new DataSet();
            string strError = "";

            string sqlCommand = "select * from CFIDDF_v a join CFYED2_v b on a.CFIDCD = b.CYIDTY where b.CYECLS = @pcFieldValue1 and CFIDCD = @pcFieldValue2";
            SqlParameter[] dbParam = new SqlParameter[2];
            dbParam[0] = new SqlParameter("@pcFieldValue1", SqlDbType.VarChar);
            dbParam[0].Value = CustomerTypeCode;
            dbParam[0].Direction = System.Data.ParameterDirection.Input;

            dbParam[1] = new SqlParameter("@pcFieldValue2", SqlDbType.VarChar);
            dbParam[1].Value = IDType;
            dbParam[1].Direction = System.Data.ParameterDirection.Input;


            using (clsDBServiceSQLServer _clsDBServiceSQLServer = new clsDBServiceSQLServer())
            {
                msgResponse.IsSuccess = _clsDBServiceSQLServer.QueryCommand(_strCIFParameterConnectionString_CFMAST, sqlCommand, ref dbParam, _nTimeOut, out dsOut, out strError);
                msgResponse.Data = dsOut;
                msgResponse.Description = strError;

                if (strError.Length > 0)
                {
                    doLogging(API_LOGENVELOPModel.ERROR, "Error Exception : " + GetMethodName(), "", msgResponse, null);
                    msgResponse.Description = API_LOGENVELOPModel.CATCH_DESC_EXCEPTION;
                }
            }

            return msgResponse;
        }

        public local.APIMessage<EKTPDukCaPilModel> SubGetDataEKTP(string NomorEKTP)
        {
            doLogging(API_LOGENVELOPModel.INFO, "Executing : " + GetMethodName(), "", NomorEKTP);

            local.APIMessage<EKTPDukCaPilModel> msgResponse = new local.APIMessage<EKTPDukCaPilModel>();
            DataSet dsOut = new DataSet();
            string strError = "";
            string sqlCommand = "";

            long.TryParse(NomorEKTP, out long tmp);

            if (String.IsNullOrEmpty(NomorEKTP.Trim()))
            {
                sqlCommand = "SELECT * FROM ProCIFEKTPWSData where Nik = 0";
            }
            else if (tmp <= 0)
            {
                sqlCommand = "SELECT * FROM ProCIFEKTPWSData where Nik = 0";
            }
            else
            {
                sqlCommand = "SELECT * FROM ProCIFEKTPWSData WHERE Nik = @NomorEKTP";
            }

            SqlParameter[] dbParam = new SqlParameter[1];
            dbParam[0] = new SqlParameter("@NomorEKTP", SqlDbType.VarChar);
            dbParam[0].Value = NomorEKTP;
            dbParam[0].Direction = System.Data.ParameterDirection.Input;

            using (clsDBServiceSQLServer _clsDBServiceSQLServer = new clsDBServiceSQLServer())
            {
                msgResponse.IsSuccess = _clsDBServiceSQLServer.QueryCommand(_strCIFParameterConnectionString_CFMAST, sqlCommand, ref dbParam, _nTimeOut, out dsOut, out strError);                
                msgResponse.Description = strError;

                if (dsOut != null && dsOut.Tables.Count > 0 && dsOut.Tables[0].Rows.Count > 0)
                {
                    msgResponse.Data = new EKTPDukCaPilModel();
                    msgResponse.Data.Agama = dsOut.Tables[0].Rows[0]["Agama"].ToString();
                    msgResponse.Data.Alamat = dsOut.Tables[0].Rows[0]["Alamat"].ToString();
                    msgResponse.Data.Dusun = dsOut.Tables[0].Rows[0]["Dusun"].ToString();
                    msgResponse.Data.EKTPStatus = dsOut.Tables[0].Rows[0]["EKTPStatus"].ToString();
                    msgResponse.Data.GolonganDarah = dsOut.Tables[0].Rows[0]["GolonganDarah"].ToString();
                    msgResponse.Data.JenisKelamin = dsOut.Tables[0].Rows[0]["JenisKelamin"].ToString();
                    msgResponse.Data.JenisPekerjaan = dsOut.Tables[0].Rows[0]["JenisPekerjaan"].ToString();
                    msgResponse.Data.KabName = dsOut.Tables[0].Rows[0]["KabName"].ToString();
                    msgResponse.Data.KecamatanName = dsOut.Tables[0].Rows[0]["KecamatanName"].ToString();
                    msgResponse.Data.KelurahanName = dsOut.Tables[0].Rows[0]["KelurahanName"].ToString();
                    msgResponse.Data.KodePos = Int32.Parse(dsOut.Tables[0].Rows[0]["KodePos"].ToString());
                    msgResponse.Data.NamaLengkap = dsOut.Tables[0].Rows[0]["NamaLengkap"].ToString();
                    msgResponse.Data.NamaLengkapAyah = dsOut.Tables[0].Rows[0]["NamaLengkapAyah"].ToString();
                    msgResponse.Data.NamaLengkapIbu = dsOut.Tables[0].Rows[0]["NamaLengkapIbu"].ToString();
                    msgResponse.Data.NomorEKTP = Int64.Parse(dsOut.Tables[0].Rows[0]["Nik"].ToString());
                    msgResponse.Data.NoKabupaten = Int32.Parse(dsOut.Tables[0].Rows[0]["NoKabupaten"].ToString());
                    msgResponse.Data.NoKecamatan = Int32.Parse(dsOut.Tables[0].Rows[0]["NoKecamatan"].ToString());
                    msgResponse.Data.NoKelurahan = Int32.Parse(dsOut.Tables[0].Rows[0]["NoKelurahan"].ToString());
                    msgResponse.Data.NomorKK = Int64.Parse(dsOut.Tables[0].Rows[0]["NoKK"].ToString());
                    msgResponse.Data.NoProvinsi = Int32.Parse(dsOut.Tables[0].Rows[0]["NoProvinsi"].ToString());
                    msgResponse.Data.NoRT = Int32.Parse(dsOut.Tables[0].Rows[0]["NoRT"].ToString());
                    msgResponse.Data.NoRW = Int32.Parse(dsOut.Tables[0].Rows[0]["NoRW"].ToString());
                    msgResponse.Data.PendidikanAkhir = dsOut.Tables[0].Rows[0]["PendidikanAkhir"].ToString();
                    msgResponse.Data.PenyandangCacat = dsOut.Tables[0].Rows[0]["PenyandangCacat"].ToString();
                    msgResponse.Data.ProvinsiName = dsOut.Tables[0].Rows[0]["ProvinsiName"].ToString();
                    msgResponse.Data.RequestDate = Convert.ToDateTime(dsOut.Tables[0].Rows[0]["RequestDate"].ToString());
                    msgResponse.Data.RequesterNIK = dsOut.Tables[0].Rows[0]["RequesterNIK"].ToString();
                    msgResponse.Data.StatusHubKel = dsOut.Tables[0].Rows[0]["StatusHubKel"].ToString();
                    msgResponse.Data.StatusKawin = dsOut.Tables[0].Rows[0]["StatusKawin"].ToString();
                    msgResponse.Data.TanggalLahir = dsOut.Tables[0].Rows[0]["TanggalLahir"].ToString();
                    msgResponse.Data.TempatLahir = dsOut.Tables[0].Rows[0]["TempatLahir"].ToString();
                }
                else
                {
                    msgResponse.Data = null;

                    if (strError.Length > 0)
                    {
                        doLogging(API_LOGENVELOPModel.ERROR, "Error Exception : " + GetMethodName(), "", msgResponse, null);
                        msgResponse.Description = API_LOGENVELOPModel.CATCH_DESC_EXCEPTION;
                    }
                }
            }

            return msgResponse;
        }

        public local.APIMessage<DataSet> SubGetParameterIDType(string IDType)
        {
            doLogging(API_LOGENVELOPModel.INFO, "Executing : " + GetMethodName(), "", IDType);

            local.APIMessage<DataSet> msgResponse = new local.APIMessage<DataSet>();
            DataSet dsOut = new DataSet();
            string strError = "";
            string sqlCommand = "";

            if (String.IsNullOrEmpty(IDType.Trim()))
            {
                sqlCommand = "SELECT * FROM CFIDDF_v";
            }
            else
            {
                sqlCommand = "SELECT * FROM CFIDDF_v WHERE CFIDCD = @pcIDTypeCode";
            }

            SqlParameter[] dbParam = new SqlParameter[1];
            dbParam[0] = new SqlParameter("@pcIDTypeCode", SqlDbType.VarChar);
            dbParam[0].Value = IDType;
            dbParam[0].Direction = System.Data.ParameterDirection.Input;

            using (clsDBServiceSQLServer _clsDBServiceSQLServer = new clsDBServiceSQLServer())
            {
                msgResponse.IsSuccess = _clsDBServiceSQLServer.QueryCommand(_strCIFParameterConnectionString_CFMAST, sqlCommand, ref dbParam, _nTimeOut, out dsOut, out strError);
                msgResponse.Data = dsOut;
                msgResponse.Description = strError;

                if (strError.Length > 0)
                {
                    doLogging(API_LOGENVELOPModel.ERROR, "Error Exception : " + GetMethodName(), "", msgResponse, null);
                    msgResponse.Description = API_LOGENVELOPModel.CATCH_DESC_EXCEPTION;
                }
            }

            return msgResponse;
        }

        public local.APIMessage<DataSet> SubGetParameterMasterCountry(string paramValue)
        {
            doLogging(API_LOGENVELOPModel.INFO, "Executing : " + GetMethodName(), "", paramValue);

            local.APIMessage<DataSet> msgResponse = new local.APIMessage<DataSet>();
            DataSet dsOut = new DataSet();
            string strError = "";
            string sqlCommand = "";

            if (String.IsNullOrEmpty(paramValue.Trim()))
            {
                sqlCommand = "SELECT * FROM JHCOUN_v where JHCOD = 'CT'  ";
            }
            else
            {
                sqlCommand = "SELECT * FROM JHCOUN_v WHERE JHCCOC = @paramValue and JHCOD = 'CT' ";
            }

            SqlParameter[] dbParam = new SqlParameter[1];
            dbParam[0] = new SqlParameter("@paramValue", SqlDbType.VarChar);
            dbParam[0].Value = paramValue;
            dbParam[0].Direction = System.Data.ParameterDirection.Input;

            using (clsDBServiceSQLServer _clsDBServiceSQLServer = new clsDBServiceSQLServer())
            {
                msgResponse.IsSuccess = _clsDBServiceSQLServer.QueryCommand(_strCIFParameterConnectionString_CFMAST, sqlCommand, ref dbParam, _nTimeOut, out dsOut, out strError);
                msgResponse.Data = dsOut;
                msgResponse.Description = strError;

                if (strError.Length > 0)
                {
                    doLogging(API_LOGENVELOPModel.ERROR, "Error Exception : " + GetMethodName(), "", msgResponse, null);
                    msgResponse.Description = API_LOGENVELOPModel.CATCH_DESC_EXCEPTION;
                }
            }

            return msgResponse;
        }

        public local.APIMessage<DataSet> SubGetParameterMasterCFPAR3(string paramValue1, string paramValue2)
        {
            doLogging(API_LOGENVELOPModel.INFO, "Executing : " + GetMethodName(), "", paramValue1 + "|" + paramValue2);

            local.APIMessage<DataSet> msgResponse = new local.APIMessage<DataSet>();
            DataSet dsOut = new DataSet();
            string strError = "";
            string sqlCommand = "";

            if (String.IsNullOrEmpty(paramValue1.Trim()))
            {
                sqlCommand = "SELECT * FROM CFPAR3_v";
            }
            else
            {
                sqlCommand = "SELECT * FROM CFPAR3_v WHERE CP3UCD = @paramValue1 and CP3UIC = @paramValue2";
            }

            SqlParameter[] dbParam = new SqlParameter[2];
            dbParam[0] = new SqlParameter("@paramValue1", SqlDbType.VarChar);
            dbParam[0].Value = paramValue1;
            dbParam[0].Direction = System.Data.ParameterDirection.Input;

            dbParam[1] = new SqlParameter("@paramValue2", SqlDbType.VarChar);
            dbParam[1].Value = paramValue2;
            dbParam[1].Direction = System.Data.ParameterDirection.Input;

            using (clsDBServiceSQLServer _clsDBServiceSQLServer = new clsDBServiceSQLServer())
            {
                msgResponse.IsSuccess = _clsDBServiceSQLServer.QueryCommand(_strCIFParameterConnectionString_CFMAST, sqlCommand, ref dbParam, _nTimeOut, out dsOut, out strError);
                msgResponse.Data = dsOut;
                msgResponse.Description = strError;

                if (strError.Length > 0)
                {
                    doLogging(API_LOGENVELOPModel.ERROR, "Error Exception : " + GetMethodName(), "", msgResponse, null);
                    msgResponse.Description = API_LOGENVELOPModel.CATCH_DESC_EXCEPTION;
                }
            }

            return msgResponse;
        }

        public local.APIMessage<DataSet> SubGetParameterMasterLapTrxTunai(string paramValue1)
        {
            doLogging(API_LOGENVELOPModel.INFO, "Executing : " + GetMethodName(), "", paramValue1);

            local.APIMessage<DataSet> msgResponse = new local.APIMessage<DataSet>();
            DataSet dsOut = new DataSet();
            string strError = "";
            string sqlCommand = "";

            if (String.IsNullOrEmpty(paramValue1.Trim()))
            {
                sqlCommand = "SELECT * FROM CFPPTT_v";
            }
            else
            {
                sqlCommand = "SELECT * FROM CFPPTT_v WHERE CFTTCD = @paramValue1";
            }

            SqlParameter[] dbParam = new SqlParameter[1];
            dbParam[0] = new SqlParameter("@paramValue1", SqlDbType.VarChar);
            dbParam[0].Value = paramValue1;
            dbParam[0].Direction = System.Data.ParameterDirection.Input;

            using (clsDBServiceSQLServer _clsDBServiceSQLServer = new clsDBServiceSQLServer())
            {
                msgResponse.IsSuccess = _clsDBServiceSQLServer.QueryCommand(_strCIFParameterConnectionString_CFMAST, sqlCommand, ref dbParam, _nTimeOut, out dsOut, out strError);
                msgResponse.Data = dsOut;
                msgResponse.Description = strError;

                if (strError.Length > 0)
                {
                    doLogging(API_LOGENVELOPModel.ERROR, "Error Exception : " + GetMethodName(), "", msgResponse, null);
                    msgResponse.Description = API_LOGENVELOPModel.CATCH_DESC_EXCEPTION;
                }
            }

            return msgResponse;
        }

        public local.APIMessage<DataSet> SubGetParameterMasterNamaKaryawan(string paramValue)
        {
            doLogging(API_LOGENVELOPModel.INFO, "Executing : " + GetMethodName(), "", paramValue);

            local.APIMessage<DataSet> msgResponse = new local.APIMessage<DataSet>();
            DataSet dsOut = new DataSet();
            string strError = "";
            string sqlCommand = "";

            if (String.IsNullOrEmpty(paramValue.Trim()))
            {
                sqlCommand = "SELECT * FROM CFEMEM_v";
            }
            else
            {
                sqlCommand = "SELECT * FROM CFEMEM_v WHERE CFEMEK = @paramValue";
            }

            SqlParameter[] dbParam = new SqlParameter[1];
            dbParam[0] = new SqlParameter("@paramValue", SqlDbType.VarChar);
            dbParam[0].Value = paramValue;
            dbParam[0].Direction = System.Data.ParameterDirection.Input;

            using (clsDBServiceSQLServer _clsDBServiceSQLServer = new clsDBServiceSQLServer())
            {
                msgResponse.IsSuccess = _clsDBServiceSQLServer.QueryCommand(_strCIFParameterConnectionString_CFMAST, sqlCommand, ref dbParam, _nTimeOut, out dsOut, out strError);
                msgResponse.Data = dsOut;
                msgResponse.Description = strError;

                if (strError.Length > 0)
                {
                    doLogging(API_LOGENVELOPModel.ERROR, "Error Exception : " + GetMethodName(), "", msgResponse, null);
                    msgResponse.Description = API_LOGENVELOPModel.CATCH_DESC_EXCEPTION;
                }
            }

            return msgResponse;
        }

        public local.APIMessage<DataSet> SubGetParameterMasterCRS(string paramValue)
        {
            doLogging(API_LOGENVELOPModel.INFO, "Executing : " + GetMethodName(), "", paramValue);

            local.APIMessage<DataSet> msgResponse = new local.APIMessage<DataSet>();
            DataSet dsOut = new DataSet();
            string strError = "";
            string sqlCommand = "";

            if (String.IsNullOrEmpty(paramValue.Trim()))
            {
                sqlCommand = "select isnull(CRS,'N') as CRS, isnull(DateCRS,'0') as DateCRS from CIFCRS_TM";
            }
            else
            {
                sqlCommand = "select isnull(CRS,'N') as CRS, isnull(DateCRS,'0') as DateCRS from CIFCRS_TM where CIFNo = @paramValue";
            }

            SqlParameter[] dbParam = new SqlParameter[1];
            dbParam[0] = new SqlParameter("@paramValue", SqlDbType.VarChar);
            dbParam[0].Value = paramValue;
            dbParam[0].Direction = System.Data.ParameterDirection.Input;

            using (clsDBServiceSQLServer _clsDBServiceSQLServer = new clsDBServiceSQLServer())
            {
                msgResponse.IsSuccess = _clsDBServiceSQLServer.QueryCommand(_strCIFParameterConnectionString_SQL_CIF, sqlCommand, ref dbParam, _nTimeOut, out dsOut, out strError);
                msgResponse.Data = dsOut;
                msgResponse.Description = strError;

                if (strError.Length > 0)
                {
                    doLogging(API_LOGENVELOPModel.ERROR, "Error Exception : " + GetMethodName(), "", msgResponse, null);
                    msgResponse.Description = API_LOGENVELOPModel.CATCH_DESC_EXCEPTION;
                }
            }

            return msgResponse;
        }

        public local.APIMessage<DataSet> SubGetTolakanIbuKandung()
        {
            doLogging(API_LOGENVELOPModel.INFO, "Executing : " + GetMethodName(), "", "");

            local.APIMessage<DataSet> msgResponse = new local.APIMessage<DataSet>();
            DataSet dsOut = new DataSet();
            string strError = "";

            string sqlCommand = "select * from dbo.ProCIFParam_TR where Category = 'TolakanNamaGadisIbuKandung'";
            using (clsDBServiceSQLServer _clsDBServiceSQLServer = new clsDBServiceSQLServer())
            {
                msgResponse.IsSuccess = _clsDBServiceSQLServer.QueryCommand(_strCIFParameterConnectionString_CFMAST, sqlCommand, _nTimeOut, out dsOut, out strError);
                msgResponse.Data = dsOut;
                msgResponse.Description = strError;

                if (strError.Length > 0)
                {
                    doLogging(API_LOGENVELOPModel.ERROR, "Error Exception : " + GetMethodName(), "", msgResponse, null);
                    msgResponse.Description = API_LOGENVELOPModel.CATCH_DESC_EXCEPTION;
                }
            }

            return msgResponse;
        }

        public local.APIMessage<DataSet> SubGetParameterMasterPajak(string paramValue)
        {
            doLogging(API_LOGENVELOPModel.INFO, "Executing : " + GetMethodName(), "", paramValue);

            local.APIMessage<DataSet> msgResponse = new local.APIMessage<DataSet>();
            DataSet dsOut = new DataSet();
            string strError = "";
            string sqlCommand = "";

            if (String.IsNullOrEmpty(paramValue.Trim()))
            {
                sqlCommand = "select distinct JHWCOD as [KDPajak], JHWDES as [DeskripsiPajak] from JHWITH_v where JHWCUR = 'IDR' and isnull(JHWDES,'')!= '' and JHWCOD != 1 order by [KDPajak] desc";
            }
            else
            {
                sqlCommand = "select distinct JHWCOD as [KDPajak], JHWDES as [DeskripsiPajak] from JHWITH_v where JHWCUR = 'IDR' and isnull(JHWDES,'')!= '' and JHWCOD != 1 and JHWCOD=@paramValue order by [KDPajak] desc";
            }

            SqlParameter[] dbParam = new SqlParameter[1];
            dbParam[0] = new SqlParameter("@paramValue", SqlDbType.VarChar);
            dbParam[0].Value = paramValue;
            dbParam[0].Direction = System.Data.ParameterDirection.Input;

            using (clsDBServiceSQLServer _clsDBServiceSQLServer = new clsDBServiceSQLServer())
            {
                msgResponse.IsSuccess = _clsDBServiceSQLServer.QueryCommand(_strCIFParameterConnectionString_CFMAST, sqlCommand, ref dbParam, _nTimeOut, out dsOut, out strError);
                msgResponse.Data = dsOut;
                msgResponse.Description = strError;

                if (strError.Length > 0)
                {
                    doLogging(API_LOGENVELOPModel.ERROR, "Error Exception : " + GetMethodName(), "", msgResponse, null);
                    msgResponse.Description = API_LOGENVELOPModel.CATCH_DESC_EXCEPTION;
                }
            }

            return msgResponse;
        }

        public local.APIMessage<DataSet> GetCurrentDate()
        {
            doLogging(API_LOGENVELOPModel.INFO, "Executing : " + GetMethodName(), "", "");

            local.APIMessage<DataSet> msgResponse = new local.APIMessage<DataSet>();
            try
            {
                DataSet dsOut = new DataSet();


                using (clsDBServiceSQLServer _clsDBServiceSQLServer = new clsDBServiceSQLServer())
                {
                    if (_clsDBServiceSQLServer.ExecStoredProcedure(_strCIFParameterConnectionString_SQL_CIF, "ProCIFP3GetCurrentDate", _nTimeOut, out dsOut))
                    {
                        if (dsOut != null && dsOut.Tables.Count > 0 && dsOut.Tables[0].Rows.Count > 0)
                        {
                            msgResponse.Data = dsOut;
                            msgResponse.IsSuccess = true;
                            msgResponse.Description = "";
                        }
                        else
                        {
                            //return new TimeSpan(0);
                            msgResponse.Data = new DataSet();
                            msgResponse.IsSuccess = false;
                            msgResponse.Description = "No Data";
                        }
                    }
                    else
                    {
                        msgResponse.Data = new DataSet();
                        msgResponse.IsSuccess = false;
                        msgResponse.Description = "Gagal Inquiry";
                    }
                } 
            }
            catch (Exception ex)
            {
                msgResponse.Data = new DataSet();
                msgResponse.IsSuccess = false;
                //msgResponse.Description = ex.Message;

                msgResponse.Description = API_LOGENVELOPModel.CATCH_DESC_EXCEPTION;
                doLogging(API_LOGENVELOPModel.ERROR, "Catch Exception : " + GetMethodName(), "", null, ex);
            }

            return msgResponse;
        }

        public local.APIMessage<DataSet> SubGetListTypeScreeningList(string paramValue)
        {
            doLogging(API_LOGENVELOPModel.INFO, "Executing : " + GetMethodName(), "", paramValue);

            local.APIMessage<DataSet> msgResponse = new local.APIMessage<DataSet>();
            DataSet dsOut = new DataSet();
            string strError = "";
            string sqlCommand = "";

            if (String.IsNullOrEmpty(paramValue.Trim()))
            {
                sqlCommand = "select distinct RejectGroup,ListType,Comment from CIFAMLGroupData_TR";
            }
            else
            {
                sqlCommand = "select distinct RejectGroup,ListType,Comment from CIFAMLGroupData_TR where lower(ListType) = lower(@listtype)";
            }

            SqlParameter[] dbParam = new SqlParameter[1];
            dbParam[0] = new SqlParameter("@listtype", SqlDbType.VarChar);
            dbParam[0].Value = paramValue;
            dbParam[0].Direction = System.Data.ParameterDirection.Input;

            using (clsDBServiceSQLServer _clsDBServiceSQLServer = new clsDBServiceSQLServer())
            {
                msgResponse.IsSuccess = _clsDBServiceSQLServer.QueryCommand(_strCIFParameterConnectionString_CFMAST, sqlCommand, ref dbParam, _nTimeOut, out dsOut, out strError);
                msgResponse.Data = dsOut;
                msgResponse.Description = strError;

                if (strError.Length > 0)
                {
                    doLogging(API_LOGENVELOPModel.ERROR, "Error Exception : " + GetMethodName(), "", msgResponse, null);
                    msgResponse.Description = API_LOGENVELOPModel.CATCH_DESC_EXCEPTION;
                }
            }

            return msgResponse;
        }

        public local.APIMessage<DataSet> SubCheckDateIssuedID(string IDType)
        {
            doLogging(API_LOGENVELOPModel.INFO, "Executing : " + GetMethodName(), "", IDType);

            local.APIMessage<DataSet> msgResponse = new local.APIMessage<DataSet>();
            DataSet dsOut = new DataSet();
            string strError = "";

            string sqlCommand = "select * from CFIDDF_v where CFIDIX = 'Y' and CFIDCD = @pcFieldValue1";
            SqlParameter[] dbParam = new SqlParameter[1];
            dbParam[0] = new SqlParameter("@pcFieldValue1", SqlDbType.VarChar);
            dbParam[0].Value = IDType;
            dbParam[0].Direction = System.Data.ParameterDirection.Input;
            
            using (clsDBServiceSQLServer _clsDBServiceSQLServer = new clsDBServiceSQLServer())
            {
                msgResponse.IsSuccess = _clsDBServiceSQLServer.QueryCommand(_strCIFParameterConnectionString_CFMAST, sqlCommand, ref dbParam, _nTimeOut, out dsOut, out strError);
                msgResponse.Data = dsOut;
                msgResponse.Description = strError;

                if (strError.Length > 0)
                {
                    doLogging(API_LOGENVELOPModel.ERROR, "Error Exception : " + GetMethodName(), "", msgResponse, null);
                    msgResponse.Description = API_LOGENVELOPModel.CATCH_DESC_EXCEPTION;
                }
            }

            return msgResponse;
        }

        public local.APIMessage<DataSet> SubCheckDateExpiredID(string IDType)
        {
            doLogging(API_LOGENVELOPModel.INFO, "Executing : " + GetMethodName(), "", IDType);

            local.APIMessage<DataSet> msgResponse = new local.APIMessage<DataSet>();
            DataSet dsOut = new DataSet();
            string strError = "";

            string sqlCommand = "select * from CFIDDF_v where CFIDDX = 'Y' and CFIDCD = @pcFieldValue1";
            SqlParameter[] dbParam = new SqlParameter[1];
            dbParam[0] = new SqlParameter("@pcFieldValue1", SqlDbType.VarChar);
            dbParam[0].Value = IDType;
            dbParam[0].Direction = System.Data.ParameterDirection.Input;

            using (clsDBServiceSQLServer _clsDBServiceSQLServer = new clsDBServiceSQLServer())
            {
                msgResponse.IsSuccess = _clsDBServiceSQLServer.QueryCommand(_strCIFParameterConnectionString_CFMAST, sqlCommand, ref dbParam, _nTimeOut, out dsOut, out strError);
                msgResponse.Data = dsOut;
                msgResponse.Description = strError;

                if (strError.Length > 0)
                {
                    doLogging(API_LOGENVELOPModel.ERROR, "Error Exception : " + GetMethodName(), "", msgResponse, null);
                    msgResponse.Description = API_LOGENVELOPModel.CATCH_DESC_EXCEPTION;
                }
            }

            return msgResponse;
        }
        
        public local.APIMessage<DataSet> SubGetParameterCustomerType(string CustType)
        {
            doLogging(API_LOGENVELOPModel.INFO, "Executing : " + GetMethodName(), "", CustType);

            local.APIMessage<DataSet> msgResponse = new local.APIMessage<DataSet>();
            DataSet dsOut = new DataSet();
            string strError = "";
            string sqlCommand = "";

            if (String.IsNullOrEmpty(CustType.Trim()))
            {
                sqlCommand = "SELECT * FROM JHCLAS_v";
            }
            else
            {
                sqlCommand = "SELECT * FROM JHCLAS_v WHERE JHCCOD = @CustType";
            }

            SqlParameter[] dbParam = new SqlParameter[1];
            dbParam[0] = new SqlParameter("@CustType", SqlDbType.VarChar);
            dbParam[0].Value = CustType;
            dbParam[0].Direction = System.Data.ParameterDirection.Input;

            using (clsDBServiceSQLServer _clsDBServiceSQLServer = new clsDBServiceSQLServer())
            {
                msgResponse.IsSuccess = _clsDBServiceSQLServer.QueryCommand(_strCIFParameterConnectionString_CFMAST, sqlCommand, ref dbParam, _nTimeOut, out dsOut, out strError);
                msgResponse.Data = dsOut;
                msgResponse.Description = strError;

                if (strError.Length > 0)
                {
                    doLogging(API_LOGENVELOPModel.ERROR, "Error Exception : " + GetMethodName(), "", msgResponse, null);
                    msgResponse.Description = API_LOGENVELOPModel.CATCH_DESC_EXCEPTION;
                }
            }

            return msgResponse;
        }

        public local.APIMessage<DataSet> SubGetParameterBranchNumber(string param1)
        {
            doLogging(API_LOGENVELOPModel.INFO, "Executing : " + GetMethodName(), "", param1);

            local.APIMessage<DataSet> msgResponse = new local.APIMessage<DataSet>();
            DataSet dsOut = new DataSet();
            string strError = "";
            string sqlCommand = "";

            if (String.IsNullOrEmpty(param1.Trim()))
            {
                sqlCommand = "select JDBR, JDCBR, JDNAME from JHDATA_v";
            }
            else
            {
                sqlCommand = "select JDBR, JDCBR, JDNAME from JHDATA_v where JDBR=@param1";
            }

            SqlParameter[] dbParam = new SqlParameter[1];
            dbParam[0] = new SqlParameter("@param1", SqlDbType.VarChar);
            dbParam[0].Value = param1.PadLeft(5,'0');
            dbParam[0].Direction = System.Data.ParameterDirection.Input;

            using (clsDBServiceSQLServer _clsDBServiceSQLServer = new clsDBServiceSQLServer())
            {
                msgResponse.IsSuccess = _clsDBServiceSQLServer.QueryCommand(_strCIFParameterConnectionString_CFMAST, sqlCommand, ref dbParam, _nTimeOut, out dsOut, out strError);
                msgResponse.Data = dsOut;
                msgResponse.Description = strError;

                if (strError.Length > 0)
                {
                    doLogging(API_LOGENVELOPModel.ERROR, "Error Exception : " + GetMethodName(), "", msgResponse, null);
                    msgResponse.Description = API_LOGENVELOPModel.CATCH_DESC_EXCEPTION;
                }
            }

            return msgResponse;
        }

        public local.APIMessage<DataSet> SubGetParameterGelarSebelum(string param1)
        {
            doLogging(API_LOGENVELOPModel.INFO, "Executing : " + GetMethodName(), "", param1);

            local.APIMessage<DataSet> msgResponse = new local.APIMessage<DataSet>();
            DataSet dsOut = new DataSet();
            string strError = "";
            string sqlCommand = "";

            if (String.IsNullOrEmpty(param1.Trim()))
            {
                sqlCommand = "select '' as [Gelar] union all select  CFTIBN as [Gelar] from   dbo.CFTITB_v a left join ProCIFGelarPerusahaan_TM b on a.CFTIBN = b.Gelar where  b.Gelar is null order by [Gelar]";
            }
            else
            {
                sqlCommand = "select '' as [Gelar] union all select  CFTIBN as [Gelar] from   dbo.CFTITB_v a left join ProCIFGelarPerusahaan_TM b on a.CFTIBN = b.Gelar where  a.CFTIBN = @param1 and b.Gelar is null order by [Gelar]";
            }

            SqlParameter[] dbParam = new SqlParameter[1];
            dbParam[0] = new SqlParameter("@param1", SqlDbType.VarChar);
            dbParam[0].Value = param1;
            dbParam[0].Direction = System.Data.ParameterDirection.Input;

            using (clsDBServiceSQLServer _clsDBServiceSQLServer = new clsDBServiceSQLServer())
            {
                msgResponse.IsSuccess = _clsDBServiceSQLServer.QueryCommand(_strCIFParameterConnectionString_CFMAST, sqlCommand, ref dbParam, _nTimeOut, out dsOut, out strError);
                msgResponse.Data = dsOut;
                msgResponse.Description = strError;

                if (strError.Length > 0)
                {
                    doLogging(API_LOGENVELOPModel.ERROR, "Error Exception : " + GetMethodName(), "", msgResponse, null);
                    msgResponse.Description = API_LOGENVELOPModel.CATCH_DESC_EXCEPTION;
                }
            }

            return msgResponse;
        }

        public local.APIMessage<DataSet> SubGetParameterGelarSesudah(string param1)
        {
            doLogging(API_LOGENVELOPModel.INFO, "Executing : " + GetMethodName(), "", param1);

            local.APIMessage<DataSet> msgResponse = new local.APIMessage<DataSet>();
            DataSet dsOut = new DataSet();
            string strError = "";
            string sqlCommand = "";

            if (String.IsNullOrEmpty(param1.Trim()))
            {
                sqlCommand = "select '' as [Gelar] union all select  CFTIAN as [Gelar] from   dbo.CFTITA_v a where  CFTIAN  not in ('TBK','LTD') order by[Gelar]";
            }
            else
            {
                sqlCommand = "select '' as [Gelar] union all select  CFTIAN as [Gelar] from   dbo.CFTITA_v a where  CFTIAN  not in ('TBK','LTD') and CFTIAN = @param1 order by[Gelar]";
            }

            SqlParameter[] dbParam = new SqlParameter[1];
            dbParam[0] = new SqlParameter("@param1", SqlDbType.VarChar);
            dbParam[0].Value = param1;
            dbParam[0].Direction = System.Data.ParameterDirection.Input;

            using (clsDBServiceSQLServer _clsDBServiceSQLServer = new clsDBServiceSQLServer())
            {
                msgResponse.IsSuccess = _clsDBServiceSQLServer.QueryCommand(_strCIFParameterConnectionString_CFMAST, sqlCommand, ref dbParam, _nTimeOut, out dsOut, out strError);
                msgResponse.Data = dsOut;
                msgResponse.Description = strError;

                if (strError.Length > 0)
                {
                    doLogging(API_LOGENVELOPModel.ERROR, "Error Exception : " + GetMethodName(), "", msgResponse, null);
                    msgResponse.Description = API_LOGENVELOPModel.CATCH_DESC_EXCEPTION;
                }
            }

            return msgResponse;
        }

        public local.APIMessage<DataSet> SubGetParameterUserDefine(string custType, string type)
        {
            doLogging(API_LOGENVELOPModel.INFO, "Executing : " + GetMethodName(), "", type);

            local.APIMessage<DataSet> msgResponse = new local.APIMessage<DataSet>();
            DataSet dsOut = new DataSet();
            string strError = "";
            StringBuilder sqlCommand = new StringBuilder();

            sqlCommand.AppendLine("select  '' as [Kode], '' as [Deskripsi] union all");
            sqlCommand.AppendLine("select  CP3UCD as [Kode], CP3DSC as [Deskripsi]");
            sqlCommand.AppendLine("from      CFPAR3_v");
            sqlCommand.AppendLine("where     CP3UIC=@nType");
            sqlCommand.AppendLine("and CP3UCD <= case when @nType = 7 and @cCustType = 'A' then 'F'");
            sqlCommand.AppendLine("when @nType = 7 and @cCustType != 'A' then 'N'");
            sqlCommand.AppendLine("else CP3UCD end");
            sqlCommand.AppendLine("and CP3UCD >= case when @nType = 7 and @cCustType = 'A' then 'A'");
            sqlCommand.AppendLine("when @nType = 7 and @cCustType != 'A' then 'F' ");
            sqlCommand.AppendLine("else CP3UCD end");
            sqlCommand.AppendLine("and CP3UCD != case when @nType = 3 then 'D' else '' end");
            sqlCommand.AppendLine("and CP3UCD != case when @nType = 7 and @cCustType != 'A' then 'G' else 'XXX' end");
            sqlCommand.AppendLine("or (CP3UCD = case when @nType = 7 and @cCustType = 'A' then 'J' else 'XXX' end and CP3UIC = 7)");
            sqlCommand.AppendLine("or CP3UCD = case when @nType = 7 and @cCustType = 'A' then 'K' else 'XXX' end");
            sqlCommand.AppendLine("or CP3UCD = case when @nType = 7 and @cCustType = 'A' then 'O' else 'XXX' end");

            SqlParameter[] dbParam = new SqlParameter[2];
            dbParam[0] = new SqlParameter("@nType", SqlDbType.VarChar);
            dbParam[0].Value = type;
            dbParam[0].Direction = System.Data.ParameterDirection.Input;

            dbParam[1] = new SqlParameter("@cCustType", SqlDbType.VarChar);
            dbParam[1].Value = custType;
            dbParam[1].Direction = System.Data.ParameterDirection.Input;

            using (clsDBServiceSQLServer _clsDBServiceSQLServer = new clsDBServiceSQLServer())
            {
                msgResponse.IsSuccess = _clsDBServiceSQLServer.QueryCommand(_strCIFParameterConnectionString_CFMAST, sqlCommand.ToString(), ref dbParam, _nTimeOut, out dsOut, out strError);
                msgResponse.Data = dsOut;
                msgResponse.Description = strError;

                if (strError.Length > 0)
                {
                    doLogging(API_LOGENVELOPModel.ERROR, "Error Exception : " + GetMethodName(), "", msgResponse, null);
                    msgResponse.Description = API_LOGENVELOPModel.CATCH_DESC_EXCEPTION;
                }
            }

            return msgResponse;
        }

        public local.APIMessage<DataSet> SubGetParameterReligion(string param1)
        {
            doLogging(API_LOGENVELOPModel.INFO, "Executing : " + GetMethodName(), "", param1);

            local.APIMessage<DataSet> msgResponse = new local.APIMessage<DataSet>();
            DataSet dsOut = new DataSet();
            string strError = "";
            string sqlCommand = "";

            if (String.IsNullOrEmpty(param1.Trim()))
            {
                sqlCommand = "select  '' as [KodeAgama], '' as [DeskripsiAgama] union all select  JHCCOC as [KodeAgama], JHCNAM as [DeskripsiAgama] from      JHCOUN_v where     JHCOD='RL' ";
            }
            else
            {
                sqlCommand = "select  '' as [KodeAgama], '' as [DeskripsiAgama] union all select  JHCCOC as [KodeAgama], JHCNAM as [DeskripsiAgama] from      JHCOUN_v where     JHCOD='RL' and JHCCOC=@param1";
            }

            SqlParameter[] dbParam = new SqlParameter[1];
            dbParam[0] = new SqlParameter("@param1", SqlDbType.VarChar);
            dbParam[0].Value = param1;
            dbParam[0].Direction = System.Data.ParameterDirection.Input;

            using (clsDBServiceSQLServer _clsDBServiceSQLServer = new clsDBServiceSQLServer())
            {
                msgResponse.IsSuccess = _clsDBServiceSQLServer.QueryCommand(_strCIFParameterConnectionString_CFMAST, sqlCommand, ref dbParam, _nTimeOut, out dsOut, out strError);
                msgResponse.Data = dsOut;
                msgResponse.Description = strError;

                if (strError.Length > 0)
                {
                    doLogging(API_LOGENVELOPModel.ERROR, "Error Exception : " + GetMethodName(), "", msgResponse, null);
                    msgResponse.Description = API_LOGENVELOPModel.CATCH_DESC_EXCEPTION;
                }
            }

            return msgResponse;
        }

        public local.APIMessage<DataSet> SubGetParameterIdentifikasiResiko(string param1)
        {
            doLogging(API_LOGENVELOPModel.INFO, "Executing : " + GetMethodName(), "", param1);

            local.APIMessage<DataSet> msgResponse = new local.APIMessage<DataSet>();
            DataSet dsOut = new DataSet();
            string strError = "";
            string sqlCommand = "";

            if (String.IsNullOrEmpty(param1.Trim()))
            {
                sqlCommand = "select a.CFJNSU as [KodeIndikatorResikoTinggi],a.CFJNSUDSC as [DeskripsiIndikatorResikoTinggi] from  CFHRPARB a";
            }
            else
            {
                sqlCommand = "select a.CFJNSU as [KodeIndikatorResikoTinggi],a.CFJNSUDSC as [DeskripsiIndikatorResikoTinggi] from  CFHRPARB a where a.CFJNSU=@param1";
            }

            SqlParameter[] dbParam = new SqlParameter[1];
            dbParam[0] = new SqlParameter("@param1", SqlDbType.VarChar);
            dbParam[0].Value = param1;
            dbParam[0].Direction = System.Data.ParameterDirection.Input;

            using (clsDBServiceSQLServer _clsDBServiceSQLServer = new clsDBServiceSQLServer())
            {
                msgResponse.IsSuccess = _clsDBServiceSQLServer.QueryCommand(_strCIFParameterConnectionString_CFMAST, sqlCommand, ref dbParam, _nTimeOut, out dsOut, out strError);
                msgResponse.Data = dsOut;
                msgResponse.Description = strError;

                if (strError.Length > 0)
                {
                    doLogging(API_LOGENVELOPModel.ERROR, "Error Exception : " + GetMethodName(), "", msgResponse, null);
                    msgResponse.Description = API_LOGENVELOPModel.CATCH_DESC_EXCEPTION;
                }
            }

            return msgResponse;
        }

        public local.APIMessageDetail<GeneralCIFModel> SubSaveCIFMaster_TM(GeneralCIFModel generalCIFModel)
        {
            doLogging(API_LOGENVELOPModel.INFO, "Executing : " + GetMethodName(), "", null, null, generalCIFModel.GUID, generalCIFModel.NIK, generalCIFModel.Module, generalCIFModel.Branch);

            local.APIMessageDetail<GeneralCIFModel> msgResponse = new local.APIMessageDetail<GeneralCIFModel>();
            try
            {
                StringBuilder batchInsert = new StringBuilder();
                batchInsert.AppendLine("BEGIN TRY");
                batchInsert.AppendLine("BEGIN TRANSACTION;");
                batchInsert.AppendLine("declare @nUserClassId int;");
                batchInsert.AppendLine("set @nUserClassId = 0;");
                batchInsert.AppendLine("--select @nUserClassId = classification_id from user_table_classification_v where nik = @pcTellerId and module = 'Pro CIF';");
                batchInsert.AppendLine("INSERT INTO CIFMaster_TM");
                batchInsert.AppendLine("([CFCIF#],[CFBRNN],[Status],[UserSuid],[InputDate], [ClassId],[BatchID],[DocumentID], CreateByBranch, NIKRM, [Module],[GUID])");
                batchInsert.AppendLine("select @pcCIF, @pcTransactionBranch, 0, @pcTellerId, @dTransactionDate ,@nUserClassId,@pcBatchID,@pcDocumentID ,@pcCreateByBranch ,@pnNIKRM, @Module, @GUID ");
                batchInsert.AppendLine("COMMIT TRAN; ");
                batchInsert.AppendLine("END TRY ");
                batchInsert.AppendLine("BEGIN CATCH ");
                batchInsert.AppendLine("IF @@TRANCOUNT > 0");
                batchInsert.AppendLine("BEGIN");
                batchInsert.AppendLine("ROLLBACK TRAN; ");
                batchInsert.AppendLine("END; ");
                batchInsert.AppendLine("declare @errorMsg varchar(500);");
                batchInsert.AppendLine("set @errorMsg = ERROR_MESSAGE();");
                batchInsert.AppendLine("raiserror (@errorMsg, 16, 1); ");
                batchInsert.AppendLine("END CATCH ");

                SqlParameter[] dbParam = new SqlParameter[10];

                dbParam[0] = new SqlParameter("@pcCIF", SqlDbType.VarChar);
                dbParam[0].Value = Convert.ToInt64(generalCIFModel.CustomerNumber);
                dbParam[0].Direction = System.Data.ParameterDirection.Input;

                dbParam[1] = new SqlParameter("@pcTransactionBranch", SqlDbType.VarChar);
                dbParam[1].Value = generalCIFModel.BranchNumber;
                dbParam[1].Direction = System.Data.ParameterDirection.Input;

                dbParam[2] = new SqlParameter("@pcTellerId", SqlDbType.VarChar);
                dbParam[2].Value = generalCIFModel.NIK;
                dbParam[2].Direction = System.Data.ParameterDirection.Input;

                dbParam[3] = new SqlParameter("@dTransactionDate", SqlDbType.VarChar);
                dbParam[3].Value = DateTime.Now.ToString("yyyy/MM/dd");
                dbParam[3].Direction = System.Data.ParameterDirection.Input;

                dbParam[4] = new SqlParameter("@pcBatchID", SqlDbType.VarChar);
                dbParam[4].Value = "";
                dbParam[4].Direction = System.Data.ParameterDirection.Input;

                dbParam[5] = new SqlParameter("@pcDocumentID", SqlDbType.VarChar);
                dbParam[5].Value = "";
                dbParam[5].Direction = System.Data.ParameterDirection.Input;

                dbParam[6] = new SqlParameter("@pcCreateByBranch", SqlDbType.VarChar);
                dbParam[6].Value = generalCIFModel.BranchNumber;
                dbParam[6].Direction = System.Data.ParameterDirection.Input;

                dbParam[7] = new SqlParameter("@pnNIKRM", SqlDbType.VarChar);
                dbParam[7].Value = generalCIFModel.NIKRM;
                dbParam[7].Direction = System.Data.ParameterDirection.Input;

                dbParam[8] = new SqlParameter("@Module", SqlDbType.VarChar);
                dbParam[8].Value = generalCIFModel.Module;
                dbParam[8].Direction = System.Data.ParameterDirection.Input;

                dbParam[9] = new SqlParameter("@GUID", SqlDbType.VarChar);
                dbParam[9].Value = generalCIFModel.GUID;
                dbParam[9].Direction = System.Data.ParameterDirection.Input;

                string sqlCommand = "";
                string strError = "";
                DataSet dsOut = new DataSet();
                sqlCommand = batchInsert.ToString();

                using (clsDBServiceSQLServer _clsDBServiceSQLServer = new clsDBServiceSQLServer())
                {
                    msgResponse.IsSuccess = _clsDBServiceSQLServer.QueryCommand(_strCIFParameterConnectionString_CFMAST, sqlCommand, ref dbParam, _nTimeOut, out dsOut, out strError);
                    msgResponse.Data = null;
                    msgResponse.Description = strError;

                    if (strError.Length > 0)
                    {
                        doLogging(API_LOGENVELOPModel.ERROR, "Error Exception : " + GetMethodName(), "", msgResponse, null);
                        msgResponse.Description = API_LOGENVELOPModel.CATCH_DESC_EXCEPTION;
                    }
                }
            }
            catch (Exception ex)
            {
                msgResponse.IsSuccess = false;
                msgResponse.Data = null;
                //msgResponse.Description = ex.Message;
                msgResponse.Description = API_LOGENVELOPModel.CATCH_DESC_EXCEPTION;
                doLogging(API_LOGENVELOPModel.ERROR, "Catch Exception : " + GetMethodName(), "", null, ex);

            }

            return msgResponse;
        }

        public local.APIMessageDetail<object> SubSaveCIFScreeningOverride_TM()
        {
            doLogging(API_LOGENVELOPModel.INFO, "Executing : " + GetMethodName(), "", null, null, "", "SYSTEM", "SYSTEM", "SYSTEM");

            local.APIMessageDetail<object> msgResponse = new local.APIMessageDetail<object>();
            try
            {
                StringBuilder batchInsert = new StringBuilder();
                batchInsert.AppendLine("BEGIN TRY");
                batchInsert.AppendLine("BEGIN TRANSACTION;");
                batchInsert.AppendLine("insert into CIFScreeningOverride_TM (CifNo, SpvNik, OverrideReason, CifNoChar)");
                batchInsert.AppendLine("select @pcCIF, @pnSpvNik, @pcOverrideReason, right('0000000000000000000' + convert(varchar, @pcCIF),19)");                
                batchInsert.AppendLine("COMMIT TRAN; ");
                batchInsert.AppendLine("END TRY ");
                batchInsert.AppendLine("BEGIN CATCH ");
                batchInsert.AppendLine("IF @@TRANCOUNT > 0");
                batchInsert.AppendLine("BEGIN");
                batchInsert.AppendLine("ROLLBACK TRAN; ");
                batchInsert.AppendLine("END; ");
                batchInsert.AppendLine("declare @errorMsg varchar(500);");
                batchInsert.AppendLine("set @errorMsg = ERROR_MESSAGE();");
                batchInsert.AppendLine("raiserror (@errorMsg, 16, 1); ");
                batchInsert.AppendLine("END CATCH ");

                SqlParameter[] dbParam = new SqlParameter[3];

                dbParam[0] = new SqlParameter("@pcCIF", SqlDbType.VarChar);
                dbParam[0].Value = Convert.ToInt64(0);
                dbParam[0].Direction = System.Data.ParameterDirection.Input;

                dbParam[1] = new SqlParameter("@pnSpvNik", SqlDbType.VarChar);
                dbParam[1].Value = 0;
                dbParam[1].Direction = System.Data.ParameterDirection.Input;

                dbParam[2] = new SqlParameter("@pcOverrideReason", SqlDbType.VarChar);
                dbParam[2].Value = "";
                dbParam[2].Direction = System.Data.ParameterDirection.Input;

                string sqlCommand = "";
                string strError = "";
                DataSet dsOut = new DataSet();
                sqlCommand = batchInsert.ToString();

                using (clsDBServiceSQLServer _clsDBServiceSQLServer = new clsDBServiceSQLServer())
                {
                    msgResponse.IsSuccess = _clsDBServiceSQLServer.QueryCommand(_strCIFParameterConnectionString_CFMAST, sqlCommand, ref dbParam, _nTimeOut, out dsOut, out strError);
                    msgResponse.Data = null;
                    msgResponse.Description = strError;

                    if (strError.Length > 0)
                    {
                        doLogging(API_LOGENVELOPModel.ERROR, "Error Exception : " + GetMethodName(), "", msgResponse, null);
                        msgResponse.Description = API_LOGENVELOPModel.CATCH_DESC_EXCEPTION;
                    }
                }


                foreach (string tmp in new List<string>())
                {
                    batchInsert = new StringBuilder();
                    batchInsert.AppendLine("BEGIN TRY");
                    batchInsert.AppendLine("BEGIN TRANSACTION;");
                    batchInsert.AppendLine("insert into CIFScreeningOverrideDetail_TM (CifNo, Nama, TanggalLahir, ListType)");
                    batchInsert.AppendLine("select distinct @pcCIF, @NAME, @DOB, @LISTTYPE ");
                    batchInsert.AppendLine("COMMIT TRAN; ");
                    batchInsert.AppendLine("END TRY ");
                    batchInsert.AppendLine("BEGIN CATCH ");
                    batchInsert.AppendLine("IF @@TRANCOUNT > 0");
                    batchInsert.AppendLine("BEGIN");
                    batchInsert.AppendLine("ROLLBACK TRAN; ");
                    batchInsert.AppendLine("END; ");
                    batchInsert.AppendLine("declare @errorMsg varchar(500);");
                    batchInsert.AppendLine("set @errorMsg = ERROR_MESSAGE();");
                    batchInsert.AppendLine("raiserror (@errorMsg, 16, 1); ");
                    batchInsert.AppendLine("END CATCH ");

                    dbParam = new SqlParameter[4];

                    dbParam[0] = new SqlParameter("@pcCIF", SqlDbType.VarChar);
                    dbParam[0].Value = Convert.ToInt64(0);
                    dbParam[0].Direction = System.Data.ParameterDirection.Input;

                    dbParam[1] = new SqlParameter("@NAME", SqlDbType.VarChar);
                    dbParam[1].Value = 0;
                    dbParam[1].Direction = System.Data.ParameterDirection.Input;

                    dbParam[2] = new SqlParameter("@DOB", SqlDbType.VarChar);
                    dbParam[2].Value = "";
                    dbParam[2].Direction = System.Data.ParameterDirection.Input;

                    dbParam[3] = new SqlParameter("@LISTTYPE", SqlDbType.VarChar);
                    dbParam[3].Value = "";
                    dbParam[3].Direction = System.Data.ParameterDirection.Input;

                    sqlCommand = "";
                    strError = "";
                    dsOut = new DataSet();
                    sqlCommand = batchInsert.ToString();

                    using (clsDBServiceSQLServer _clsDBServiceSQLServer = new clsDBServiceSQLServer())
                    {
                        msgResponse.IsSuccess = _clsDBServiceSQLServer.QueryCommand(_strCIFParameterConnectionString_CFMAST, sqlCommand, ref dbParam, _nTimeOut, out dsOut, out strError);
                        msgResponse.Data = null;
                        msgResponse.Description = strError;

                        if (strError.Length > 0)
                        {
                            doLogging(API_LOGENVELOPModel.ERROR, "Error Exception : " + GetMethodName(), "", msgResponse, null);
                            msgResponse.Description = API_LOGENVELOPModel.CATCH_DESC_EXCEPTION;

                            batchInsert = new StringBuilder();
                            batchInsert.AppendLine("BEGIN TRY");
                            batchInsert.AppendLine("BEGIN TRANSACTION;");
                            batchInsert.AppendLine("delete from CIFScreeningOverrideDetail_TM where CifNo = @pcCIF");
                            batchInsert.AppendLine("delete from CIFScreeningOverride_TM where CifNo = @pcCIF");                            
                            batchInsert.AppendLine("COMMIT TRAN; ");
                            batchInsert.AppendLine("END TRY ");
                            batchInsert.AppendLine("BEGIN CATCH ");
                            batchInsert.AppendLine("IF @@TRANCOUNT > 0");
                            batchInsert.AppendLine("BEGIN");
                            batchInsert.AppendLine("ROLLBACK TRAN; ");
                            batchInsert.AppendLine("END; ");
                            batchInsert.AppendLine("declare @errorMsg varchar(500);");
                            batchInsert.AppendLine("set @errorMsg = ERROR_MESSAGE();");
                            batchInsert.AppendLine("raiserror (@errorMsg, 16, 1); ");
                            batchInsert.AppendLine("END CATCH ");

                            dbParam = new SqlParameter[1];

                            dbParam[0] = new SqlParameter("@pcCIF", SqlDbType.VarChar);
                            dbParam[0].Value = Convert.ToInt64(0);
                            dbParam[0].Direction = System.Data.ParameterDirection.Input;

                            sqlCommand = "";
                            strError = "";
                            dsOut = new DataSet();
                            sqlCommand = batchInsert.ToString();

                            msgResponse.IsSuccess = _clsDBServiceSQLServer.QueryCommand(_strCIFParameterConnectionString_CFMAST, sqlCommand, ref dbParam, _nTimeOut, out dsOut, out strError);
                            msgResponse.Data = null;
                            msgResponse.Description = strError;

                            if (strError.Length > 0)
                            {
                                doLogging(API_LOGENVELOPModel.ERROR, "Error Exception : " + GetMethodName(), "", msgResponse, null);
                                msgResponse.Description = API_LOGENVELOPModel.CATCH_DESC_EXCEPTION;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                msgResponse.IsSuccess = false;
                msgResponse.Data = null;
                //msgResponse.Description = ex.Message;
                msgResponse.Description = API_LOGENVELOPModel.CATCH_DESC_EXCEPTION;
                doLogging(API_LOGENVELOPModel.ERROR, "Catch Exception : " + GetMethodName(), "", null, ex);

            }

            return msgResponse;
        }

        #endregion

        #region internal library
        private String XmlValidasi(String paramChar)
        {
            paramChar = paramChar + "";
            paramChar = paramChar.Replace("&", "&amp;");
            paramChar = paramChar.Replace("<", "&lt;");
            paramChar = paramChar.Replace(">", "&gt;");

            return paramChar;
        }
        private T MapSingleObject<T>(DataTable dt)
        {
            T t = Activator.CreateInstance<T>();
            if ((dt == null) || (dt.Rows.Count == 0))
            {
                t = default(T);
            }
            else
            {
                List<PropertyInfo> fields = new List<PropertyInfo>();
                fields.AddRange(typeof(T).GetProperties());

                for (int i = fields.Count - 1; i >= 0; i--)
                {
                    if (!dt.Columns.Contains(fields[i].Name))
                    {
                        fields.RemoveAt(i);
                    }
                }

                DataRow dr = dt.Rows[0];


                foreach (PropertyInfo fi in fields)
                {
                    object objectValue = System.Convert.ChangeType(dr[fi.Name], fi.PropertyType);

                    if (fi.PropertyType == typeof(System.String))
                    {
                        fi.SetValue(t, objectValue.ToString().Trim(), null);
                    }
                    else
                    {
                        fi.SetValue(t, objectValue, null);
                    }
                }
            }

            return t;
        }
        private List<T> MapListOfObject<T>(DataTable dt)
        {
            List<T> list = new List<T>();
            if ((dt == null) || (dt.Rows.Count == 0))
            {
                list = default(List<T>);
            }
            else
            {

                List<PropertyInfo> fields = new List<PropertyInfo>();
                fields.AddRange(typeof(T).GetProperties());

                for (int i = fields.Count - 1; i >= 0; i--)
                {
                    if (!dt.Columns.Contains(fields[i].Name))
                    {
                        fields.RemoveAt(i);
                    }
                }

                foreach (DataRow dr in dt.Rows)
                {
                    T t = Activator.CreateInstance<T>();
                    foreach (PropertyInfo fi in fields)
                    {
                        object objectValue = System.Convert.ChangeType(dr[fi.Name], fi.PropertyType);
                        if (fi.PropertyType == typeof(System.String))
                        {
                            fi.SetValue(t, objectValue.ToString().Trim(), null);
                        }
                        else
                        {
                            fi.SetValue(t, objectValue, null);
                        }
                    }

                    list.Add(t);
                }
            }
            return list;
        }
        private bool ConvertRsSubDetailToXML(String strXML, out DataSet dsOut)
        {
            try
            {
                DataSet dsXML = new DataSet();
                dsXML.ReadXml(new System.IO.MemoryStream(System.Text.Encoding.UTF8.GetBytes(strXML)));
                dsOut = dsXML;

                return true;
            }
            catch
            {
                dsOut = null;
                return false;
            }
        }
        #endregion

        #region Logging
        public void doLogging(string level, string msg, string path = "", object data = null, Exception ex = null, string guid = "", string nik = "", string module = "", string branch = "")
        {
            MethodBase m = MethodBase.GetCurrentMethod();
            string group = m.ReflectedType.Module.Name;
            API_LOGENVELOPModel model = new API_LOGENVELOPModel(level, path, msg, data, ex, guid, nik, branch, module, group);
            using (clsLogging _clsLog = new clsLogging(_strParameterConnectionStringLog, _nTimeOut, Convert.ToBoolean(_iconfiguration["IsDebug"])))
            {
                _clsLog.doLogging(model);
            }
        }
        public string GetMethodName()
        {
            return new StackTrace(1).GetFrame(0).GetMethod().ReflectedType.Name + "." + new StackTrace(1).GetFrame(0).GetMethod().Name;
        }
        #endregion

    }
}
