﻿using local = ProCIF.DataPribadi.API.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProCIF.Model;

namespace ProCIF.DataPribadi.API.Services
{
    public interface IService
    {
        local.APIMessage<DataSet> GetCurrentDate();
        local.APIMessage<GeneralCIFModel> QuickCreation(GeneralCIFModel model);
        local.APIMessage<GeneralCIFModel> BasicMaintenance(GeneralCIFModel model, bool isAutoApprove = false);
        local.APIMessage<GeneralCIFModel> InquiryDetail(GeneralCIFModel model);
        local.APIMessage<GeneralCIFModel> InquiryDetailV2(GeneralCIFModel model);       
        bool SubCheckBirthDateSeventeen(DateTime BirthDate,string CustomerType);
        local.APIMessage<List<ScreeningListModel>> SubCheckScreeningList(ScreeningListModel model);
        local.APIMessage<DataSet> SubCheckScreeningNegara(string strKodeNegara, out string strError);
        local.APIMessage<DataSet> SubCheckOverride(string paramGUID);        
        local.APIMessage<object> SubSaveOverride(string paramGUID, string paramNIK, object paramObj);
        local.APIMessage<DataSet> SubCheckApproval(string paramCIF, string paramTable, string paramGUID, string paramChangeType);
        local.APIMessageDetail<GeneralCIFModel> SubSaveApprovalMaintenanceCIF(ApprovalMasterModel modelMasterApp, List<ApprovalDetailModel> modelApproval, bool isAutoApprove = false);        
        local.APIMessage<DataSet> SubCheckIDTypeWithCustomerType(string CustomerTypeCode, string IDType);
        local.APIMessage<DataSet> SubGetTolakanIbuKandung();
        local.APIMessage<DataSet> SubGetParameterIDType(string IDType);
        local.APIMessage<DataSet> SubGetParameterMasterCountry(string paramValue);
        local.APIMessage<DataSet> SubGetParameterMasterCFPAR3(string paramValue1, string paramValue2);
        local.APIMessage<DataSet> SubGetParameterMasterLapTrxTunai(string paramValue);
        local.APIMessage<DataSet> SubGetParameterMasterNamaKaryawan(string paramValue);
        local.APIMessage<DataSet> SubGetParameterMasterPajak(string paramValue);
        local.APIMessage<DataSet> SubCheckDateIssuedID(string IDType);
        local.APIMessage<DataSet> SubCheckDateExpiredID(string IDType);
        local.APIMessage<DataSet> SubGetParameterCustomerType(string CustType);
        local.APIMessage<DataSet> SubGetParameterBranchNumber(string param1);
        local.APIMessage<DataSet> SubGetParameterGelarSebelum(string param1);
        local.APIMessage<DataSet> SubGetParameterGelarSesudah(string param1);
        local.APIMessage<DataSet> SubGetParameterUserDefine(string custType, string type);
        local.APIMessage<DataSet> SubGetParameterReligion(string param1);
        local.APIMessage<DataSet> SubGetParameterIdentifikasiResiko(string param1);
        local.APIMessage<bool> PUTMessageToRequestKAFKA(string Message);
        local.APIMessage<bool> PUTMessageToProcessKAFKA(string Message);
        local.APIMessage<ApprovalMasterModel> SubGetDataChangeMasterTM(string paramValue);
        local.APIMessage<ApprovalMasterModel> SubUpdateDataChangeMasterTM(ApprovalMasterModel paramModel);
        
        local.APIMessage<ApprovalMasterModel> ProcessCompareOldAndNewModel(
            GeneralCIFModel createCIFModel, GeneralCIFModel modelOld, ref List<ApprovalDetailModel> outListApprovalDetail);

        void doLogging(string level, string msg, string path = "", object data = null, Exception ex = null, string guid = "", string nik = "", string module = "", string branch = "");
        string GetMethodName();
    }
}
